-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Nov 2018 pada 05.42
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fpti`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_admin`, `email`, `api_token`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin 1', 'admin@gmail.com', '$2y$10$bJYNgF8dMx.RuBNYjhjkoOLvpXBItDMmfF.8LY05UrGCYC40xQMs2', '$2y$10$.PjJNbsQL91gASzYmv2ZduSnMv4IKGr89870JQQaV0TRoJ63RswZm', '$2y$10$wbfkg9YPh2mzsIj3XZXuHOGUrP0e058H5pLb.QsFjWJwAAipKb64a', '2018-09-04 17:00:00', '2018-09-10 21:39:29'),
(2, 'Admin 2', 'admin2@gmail.com', '$2y$10$wbfkg9YPh2mzsIj3XZXuHOGUrP0e058H5pLb.QsFjWJwAAipKb64a', '$2y$10$wbfkg9YPh2mzsIj3XZXuHOGUrP0e058H5pLb.QsFjWJwAAipKb64a', '$2y$10$wbfkg9YPh2mzsIj3XZXuHOGUrP0e058H5pLb.QsFjWJwAAipKb64a', '2018-09-04 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_club`
--

CREATE TABLE `admin_club` (
  `id_admin_club` int(11) NOT NULL,
  `id_club` int(11) NOT NULL,
  `nama_admin_club` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_admin_club` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admin_club`
--

INSERT INTO `admin_club` (`id_admin_club`, `id_club`, `nama_admin_club`, `email`, `api_token`, `password`, `foto_admin_club`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Helmi Muharromi', 'adminclub@gmail.com', '$2y$10$Ibd6CJkZHiRu5yck/dpnxOhlbl0AhIxs161Lz1kIKugM6Zokdvb6y', '$2y$10$LvIdYmRXD4irvfRCodYkT.lGsap5q9oI33lpT0Sm/DAXKt.4LUHW2', '1541338092.png', NULL, '2018-10-30 20:16:57', '2018-11-04 06:28:12'),
(2, 1, 'Nandar', 'adminclub2@gmail.com', '$2y$10$VlZzUVxxtmTrJ8IWT7V0S.gqj/GAaSst3hwxjiEnhLWyUjUAXPX9u', '$2y$10$3ImyDDw2uPOQbDtQqaNZLuJmIospgTr/SdcGdh7wRfqVSDbSEirQ6', '', NULL, '2018-10-30 20:18:16', '2018-10-30 20:18:16'),
(3, 2, 'abd', 'bonek@gmail.com', '$2y$10$rJRmab.zE3Yp1akLRvQ89ujOdLt1/NUaeskk5GD75nZuGPerwtqe6', '$2y$10$D4A/97F2cnFZ5NupAo4VSOezMO1TPNGfnaIFVUBMmOFFH04WN4/Ci', '', NULL, '2018-11-01 07:17:17', '2018-11-03 23:40:46'),
(4, 3, 'Adminclub121', 'abd@gmail.com', '$2y$10$sMSGIGyio/GI7UKm42TBfudUGe2zVwRBTGuAU9zCr7BMWDJVzcqte', '$2y$10$ZcaF46uzftwG1tizUijCieQ3HFI/heOiH8L0ngYudSPr/N1rMM91K', '1541640118.jpg', NULL, '2018-11-07 18:21:58', '2018-11-08 18:12:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `api_token` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `anggota_nik` varchar(25) NOT NULL,
  `anggota_foto` text NOT NULL,
  `anggota_nama` varchar(255) NOT NULL,
  `anggota_tempat_lahir` varchar(255) NOT NULL,
  `anggota_tanggal_lahir` date NOT NULL,
  `anggota_jenis_kelamin` enum('L','P') NOT NULL,
  `anggota_tinggi` int(11) NOT NULL,
  `anggota_berat` int(11) NOT NULL,
  `anggota_alamat` varchar(255) NOT NULL,
  `anggota_telepon` varchar(13) NOT NULL,
  `anggota_pekerjaan` varchar(255) NOT NULL,
  `anggota_status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `email`, `password`, `api_token`, `created_at`, `updated_at`, `anggota_nik`, `anggota_foto`, `anggota_nama`, `anggota_tempat_lahir`, `anggota_tanggal_lahir`, `anggota_jenis_kelamin`, `anggota_tinggi`, `anggota_berat`, `anggota_alamat`, `anggota_telepon`, `anggota_pekerjaan`, `anggota_status`) VALUES
(1, 'wwww@gmail.com', '$2y$10$sxVrtyPitI3TkB./0aD5Ke804Y7GzJcg916JanEUIrLCUyhf8UP3G', '$2y$10$TrpU7vvbVZr4Gr0cWJRc1.scLQL19i.UcL3Haj6QCZK0lnANB0h2S', '2018-11-01 09:08:47', '2018-11-01 09:08:47', '43434343343434343', '1541088526.jpg', 'sdsdsdd', 'dsdsdsdsd', '2018-10-31', 'L', 212121, 121212, '121212', '232323', 'fdfdsdsd', '0'),
(2, 'weeqeq@gmail.com', '$2y$10$7dXpWTNHVsh08SI9BeMb4eTiCr/yrKut.WDPAzvPJhSDYT1.UwT4e', '$2y$10$jM2dgKh2FEsSVvQvQckLd.CTr1rgMzWoN/s/5Xjb26EmuVktQ7c9q', '2018-11-01 09:09:51', '2018-11-01 09:09:51', '43434343343434343', '1541088591.jpg', 'eqe', 'qeqe', '2018-11-01', 'L', 233, 3333, '333', '33', 'sdf', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `atlet`
--

CREATE TABLE `atlet` (
  `id_atlet` int(11) NOT NULL,
  `id_club` int(11) NOT NULL,
  `id_admin_club` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `api_token` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tanggal_terima` date DEFAULT NULL,
  `atlet_nama` varchar(200) NOT NULL,
  `atlet_foto` text NOT NULL,
  `atlet_tempat_lahir` varchar(200) NOT NULL,
  `atlet_tanggal_lahir` date NOT NULL,
  `atlet_jenis_kelamin` enum('L','P') NOT NULL,
  `atlet_golongan_darah` varchar(200) NOT NULL,
  `atlet_tinggi` int(11) NOT NULL,
  `atlet_berat` int(11) NOT NULL,
  `atlet_alamat` varchar(200) NOT NULL,
  `atlet_sekolah` varchar(200) NOT NULL,
  `atlet_telepon` varchar(200) NOT NULL,
  `atlet_nama_ortu` varchar(200) NOT NULL,
  `atlet_akte` text NOT NULL,
  `atlet_kk` text NOT NULL,
  `atlet_sp` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  `lulus` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `atlet`
--

INSERT INTO `atlet` (`id_atlet`, `id_club`, `id_admin_club`, `email`, `api_token`, `password`, `created_at`, `updated_at`, `tanggal_terima`, `atlet_nama`, `atlet_foto`, `atlet_tempat_lahir`, `atlet_tanggal_lahir`, `atlet_jenis_kelamin`, `atlet_golongan_darah`, `atlet_tinggi`, `atlet_berat`, `atlet_alamat`, `atlet_sekolah`, `atlet_telepon`, `atlet_nama_ortu`, `atlet_akte`, `atlet_kk`, `atlet_sp`, `status`, `lulus`) VALUES
(1, 1, 1, 'munip@gmail.com', '$2y$10$zrCcBfIKCgLsAZeQ4cWyW..hgZ5XNhUZp8o77F2DkTRJISuyIiBjm', '$2y$10$NZdrVICnpuT9Zx/pD.zBd.YbAjPVcGGQfT3v7o2oy6w9.Ov88kcO.', '2018-11-04 13:36:07', '2018-11-04 06:36:07', NULL, 'Munip Abdulla1', '1541338567.png', 'Pamekasan', '1996-06-01', 'L', 'B', 150, 60, 'Pamekasan1', 'SMA Pamekasan1', '0856666666661', 'Munaroh1', '1541338567.png', '1541338567.png', '1541338567.png', '1', '0'),
(2, 1, 1, 'indah@gmail.com', '$2y$10$hdurrwkyouXhAk3xYuWI0uoPY2KWFVPrzzx56JlClhDQaAiP4anB6', '$2y$10$v4nKNYuKNznc/lOLrc7SReJPXxW/1LeVfbEpyUEkqnbXnPBGYpG62', '2018-11-04 13:35:19', '2018-11-04 06:35:19', NULL, 'Indah Dewi Pertiwi', '1541338519.png', 'Pamekasan', '1994-10-06', 'P', 'A', 160, 60, 'Pamekasan', 'SMA Pamekasan', '0854444444', 'Munaroh', '1541338519.png', '1541338519.png', '1541338519.png', '1', '0'),
(3, 1, 1, 'atlet@gmail.com', '$2y$10$JtzN/Y5.JoaLRR5Iut9JAec.lcWIva0sLKwphqd39qY//hXyzQeeq', '$2y$10$rLXFwpTiWrcTdJtFsmL3f.fMS12S/cOchIpNObxAzLtNVNXHdx/qq', '2018-11-01 14:45:36', '2018-11-01 07:45:36', NULL, 'Maman Abdurrahman', '1540956749.png', 'Pamekasan', '1993-10-04', 'L', 'A', 170, 70, 'Pamekasan', 'SMA Pamekasan', '09866666666', 'Munaroh', '1540956749.png', '1540956749.png', '1540956749.png', '1', '0'),
(4, 2, 3, 'wqwq@gmail.com', '$2y$10$LGgdP4lS38q9UmtKqeHW/uaKOKbQDGK3esHQSLgl716dX7nVleYge', '$2y$10$TUZn8ZKXCGaWQmeXHq.kLuCofuboK2tnUXM6gCLULfqTl7EAIowdO', '2018-11-04 06:15:27', '2018-11-03 23:15:27', NULL, 'wqw', '1541157671.jpg', 'qwqwq', '2018-10-29', 'L', 'A', 123, 12, 'sdds', 'asas', '321212', 'sedrftgyhu', '1541157671.jpg', '1541157671.jpg', '1541157671.jpg', '1', '0'),
(5, 3, 4, 'adminachmaulanaa@gmail.com', '$2y$10$RI8zZDVSl9PXtuodjmQsuuOSBTVioSODMB0BFuqyc8loM8jDC9ROa', '$2y$10$Z5d.f119ofpxKHb4iBpwDuyi2ekKWXr0Py4kP0scimzcFmORGWLp.', '2018-11-09 01:17:19', '2018-11-08 18:17:19', NULL, 'aaaewewewewew', '1541641883.jpg', 'pmk', '2018-11-08', 'L', 'A', 123, 123, 'sdds', 'sdsdsd', '433', 'fgdhb', '1541641883.jpg', '1541641883.jpg', '1541641883.jpg', '1', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `club`
--

CREATE TABLE `club` (
  `id_club` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `logo_club` text NOT NULL,
  `nama_club` varchar(200) NOT NULL,
  `nomor_club` varchar(200) NOT NULL,
  `tanggal_keanggotaan` date NOT NULL,
  `alamat_club` varchar(200) NOT NULL,
  `telepon_club` varchar(200) NOT NULL,
  `tahun_berdiri` int(11) NOT NULL,
  `ketua_club` varchar(200) NOT NULL,
  `pembina_club` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `club`
--

INSERT INTO `club` (`id_club`, `id_admin`, `logo_club`, `nama_club`, `nomor_club`, `tanggal_keanggotaan`, `alamat_club`, `telepon_club`, `tahun_berdiri`, `ketua_club`, `pembina_club`) VALUES
(1, 1, '1541337910.png', 'Rasa Cita', '7654321', '2018-10-13', 'Pademawu, Pamekasan', '085555555555', 2016, 'Hamdani Rais', 'Hamdani Munif'),
(2, 1, '1541736713.png', 'Bonex', '111123', '2018-11-01', 'Pamekasan', '0977', 1998, 'Haidar', 'ABD'),
(3, 1, '1541736403.png', 'club123', '123', '2018-11-08', 'Pamekasan', '08', 2016, 'Rozi', 'Valen'),
(4, 1, '1541736937.png', 'persija', '123', '2018-11-10', 'asdas', '121212', 2018, 'hanip', 'hasan'),
(5, 1, '1541736976.png', 'Arema', '14342', '2011-12-12', 'asdas', '2332323', 2109, '123', '123123'),
(6, 1, '1541737982.png', 'nama club', '123123', '2018-11-17', '12', '123', 123, '12', '123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokumen`
--

CREATE TABLE `dokumen` (
  `id_dokumen` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `nama_dokumen` varchar(200) NOT NULL,
  `tanggal_dokumen` date NOT NULL,
  `keterangan_dokumen` varchar(200) NOT NULL,
  `dokumen` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dokumen`
--

INSERT INTO `dokumen` (`id_dokumen`, `id_admin`, `nama_dokumen`, `tanggal_dokumen`, `keterangan_dokumen`, `dokumen`) VALUES
(1, 1, 'Rekap Atliti', '2018-10-31', 'Rekap Atleti', '1541553183.pdf'),
(2, 1, 'Anggaran Dasar FPTI', '2018-11-08', 'Anggaran Dasar FPTI tahun 2017', '1541639824.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto`
--

CREATE TABLE `foto` (
  `id_foto` int(11) NOT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `id_pelatih` int(11) DEFAULT NULL,
  `judul_foto` varchar(200) NOT NULL,
  `tanggal_foto` date NOT NULL,
  `keterangan_foto` text NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `foto`
--

INSERT INTO `foto` (`id_foto`, `id_admin`, `id_pelatih`, `judul_foto`, `tanggal_foto`, `keterangan_foto`, `foto`) VALUES
(1, 1, NULL, 'Latihan rutin', '2018-10-06', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '1541339894.png'),
(2, 1, NULL, 'Latihan rutin minggu 2', '2018-10-06', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '1541339888.png'),
(3, 1, 3, 'tswwwwwww', '2018-11-01', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quae sunt tempora dolorem quidem rem pariatur laborum explicabo incidunt eius molestiae perferendis ratione id eligendi, ea voluptas placeat alias aliquam earum.', '1541339882.png'),
(4, 1, NULL, 'bisaa', '2018-11-08', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quae sunt tempora dolorem quidem rem pariatur laborum explicabo incidunt eius molestiae perferendis ratione id eligendi, ea voluptas placeat alias aliquam earum.', '1541640006.jpg'),
(5, 1, 2, 'EWEWE', '2018-11-07', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quae sunt tempora dolorem quidem rem pariatur laborum explicabo incidunt eius molestiae perferendis ratione id eligendi, ea voluptas placeat alias aliquam earum.', '1541641111.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `info`
--

CREATE TABLE `info` (
  `id_info` int(10) NOT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `id_pelatih` int(11) DEFAULT NULL,
  `judul_info` varchar(200) NOT NULL,
  `tanggal_info` date NOT NULL,
  `info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `info`
--

INSERT INTO `info` (`id_info`, `id_admin`, `id_pelatih`, `judul_info`, `tanggal_info`, `info`) VALUES
(1, 1, NULL, 'Persiapan Seleksi 2018', '2018-10-31', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(2, 1, 3, 'tes 123', '2018-11-01', 'dsdsddsd'),
(3, 1, NULL, 'aaAa', '2018-11-03', 'AaA'),
(4, 1, NULL, 'Alhamdulillah', '2018-11-08', 'bisa !'),
(7, 1, NULL, 'Test', '2018-11-08', 'asdfnsadl;fnsdf'),
(8, 1, NULL, 'Ini Info', '2018-11-08', 'Hello, ini info loh'),
(9, 1, NULL, 'rtyui', '2018-11-08', 'dfghjk'),
(10, 1, 2, 'sasasa', '2018-11-09', 'asasa as');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `nama_jabatan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `nama_jabatan`) VALUES
(3, 'Penasehat I'),
(4, 'Penasehat II'),
(5, 'Ketua Umum'),
(6, 'Ketua Harian');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kompetisi`
--

CREATE TABLE `kompetisi` (
  `id_kompetisi` int(11) NOT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `id_pelatih` int(11) DEFAULT NULL,
  `nama_kompetisi` varchar(200) NOT NULL,
  `tanggal_kompetisi` date NOT NULL,
  `dokumen_kompetisi` text NOT NULL,
  `laporan_kompetisi` text NOT NULL,
  `hasil_kompetisi` text NOT NULL,
  `keterangan_kompetisi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kompetisi`
--

INSERT INTO `kompetisi` (`id_kompetisi`, `id_admin`, `id_pelatih`, `nama_kompetisi`, `tanggal_kompetisi`, `dokumen_kompetisi`, `laporan_kompetisi`, `hasil_kompetisi`, `keterangan_kompetisi`) VALUES
(1, 1, NULL, 'Pemakasan Cup', '2018-10-05', '1541338031.pdf', '1541338031.pdf', '1541338031.pdf', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
(2, 1, NULL, 'work club1', '2018-11-01', '1541338021.pdf', '1541338021.pdf', '1541338021.pdf', 'aaaa'),
(3, NULL, 2, 'Pemekasan Cup 10', '2018-11-03', '1541338409.pdf', '1541338409.pdf', '1541338409.pdf', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_seleksi` int(11) NOT NULL,
  `id_pelatih` int(11) NOT NULL,
  `panjat_lead` int(11) NOT NULL,
  `panjat_speed` int(11) NOT NULL,
  `lari` int(11) NOT NULL,
  `push_up` int(11) NOT NULL,
  `pull_up` int(11) NOT NULL,
  `sit_up` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `id_seleksi`, `id_pelatih`, `panjat_lead`, `panjat_speed`, `lari`, `push_up`, `pull_up`, `sit_up`) VALUES
(1, 9, 2, 1, 1, 1, 1, 1, 1),
(2, 10, 2, 12, 12, 12, 12, 12, 22);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelatih`
--

CREATE TABLE `pelatih` (
  `id_pelatih` int(10) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `nama_pelatih` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan_pelatih` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_pelatih` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sertifikasi_pelatih` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sertifikat_pelatih` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_pelatih` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pelatih`
--

INSERT INTO `pelatih` (`id_pelatih`, `id_admin`, `nama_pelatih`, `email`, `jabatan_pelatih`, `alamat_pelatih`, `sertifikasi_pelatih`, `sertifikat_pelatih`, `foto_pelatih`, `api_token`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 1, 'Nur Maulidi Haryanto, S.Pd', 'nur@gmail.com', 'Kepala Pelatih', 'Pamekasan', 'Pelatih Level 1 Nasional', '1541337893.png', '1541337893.png', '$2y$10$n7nWUV35.LFSl1yVpKqmku1aPcUgwSd7NqOJRN6y385cawCswJ9wm', '$2y$10$szBxqh9sywT3PLvRK0KLtuZLyb1APghvc1t3zrTGhFxmxwgOh5xdy', NULL, '2018-11-01 06:43:41', '2018-11-04 06:24:53'),
(3, 1, 'Achmad Maulana', 'achmaulana@gmail.com', 'Asisten Pelatih', 'Pamekasan', 'Pelatih Level 1 Nasional', '1541337881.png', '1541337881.png', '$2y$10$S1pTWZfHhDv8kUBdCgKW..nucGqem8KMMp16fQLS0oRcd1LlQRFNW', '$2y$10$rMzt2XVCqONCKR0RN9GdKu4/0tjSWxmSfmrbWiY3eK4ndZ6GCCdfm', NULL, '2018-11-01 06:44:47', '2018-11-04 06:24:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prestasi`
--

CREATE TABLE `prestasi` (
  `id_prestasi` int(11) NOT NULL,
  `id_atlet` int(11) NOT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `kategori` varchar(200) NOT NULL,
  `peringkat` int(11) NOT NULL,
  `tingkat` varchar(200) NOT NULL,
  `tahun` year(4) NOT NULL,
  `tempat` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prestasi`
--

INSERT INTO `prestasi` (`id_prestasi`, `id_atlet`, `id_admin`, `kategori`, `peringkat`, `tingkat`, `tahun`, `tempat`) VALUES
(1, 1, 1, 'Spider Kid A', 1, 'Nasional', 2018, 'Jakarta'),
(2, 1, 1, 'wewewe', 3, 'pmk', 2016, 'paoama');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sarpras`
--

CREATE TABLE `sarpras` (
  `id_sarpras` int(11) NOT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `id_club` int(11) DEFAULT NULL,
  `items` varchar(100) NOT NULL,
  `volume` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `tahun_perolehan` int(11) NOT NULL,
  `keterangan_sarpras` varchar(75) NOT NULL,
  `kode_barang` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sarpras`
--

INSERT INTO `sarpras` (`id_sarpras`, `id_admin`, `id_club`, `items`, `volume`, `satuan`, `tahun_perolehan`, `keterangan_sarpras`, `kode_barang`) VALUES
(1, 1, NULL, 'Sepatu', 1, 'Pasang', 2016, 'Sepatu khusu panjat tebing', '87654321'),
(2, NULL, 1, 'Stopwatch', 2, 'Unit', 2016, 'Anytime (warna hitam plus tali biru)', 'XL-022'),
(3, 1, NULL, 'Tali Carmantel', 1, 'buah', 2016, 'Anytime (warna hitam plus tali biru)', 'XL-0224'),
(4, NULL, 3, 'Tali Carmantelsdsd', 21, 'Buah', 2017, 'Warnah putih', '123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sejarah`
--

CREATE TABLE `sejarah` (
  `id_sejarah` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `judul_sejarah` varchar(200) NOT NULL,
  `isi_sejarah` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sejarah`
--

INSERT INTO `sejarah` (`id_sejarah`, `id_admin`, `judul_sejarah`, `isi_sejarah`) VALUES
(1, 1, 'Sejarah FPTI Pamekasan', '<pre>\r\n<img alt="" src="/fpti/public/images/sejarah/images/300X600.png" style="height:301px; width:601px" />\r\n</pre>\r\n\r\n<p><span dir="rtl">Pada hari Senin Tanggal 22 November 2015, terbentuklah FPTI Pamekasan atas prakarsa insan panjat tebing yang dimotori oleh Organisasi Kepecintalaman di Pamekasan</span></p>\r\n\r\n<p><span dir="rtl">&bull;&nbsp;&nbsp; &nbsp;Pada hari Minggu tanggal 29 Nopember 2015, diadakan sosialisasi pembentukan FPTI Pamekasan yang dihadiri oleh Pengurus Kabupaten Pamekasan, Perwakilan KONI Pamekasan dan Organisasi Kepecintaalaman di Pamekasan, yang bertempat di ruang pertemuan KONI Pamekasan.</span></p>\r\n\r\n<p><span dir="rtl">&bull;&nbsp;&nbsp; &nbsp;Pada tanggal 10 Desember 2015 &nbsp;terbitlah Surat Rekomendasi dari KONI Pamekasan untuk mendapatkan Surat Keputusan Pengukuhan dari Pengurus Provinsi FPTI Jawa Timur.</span></p>'),
(2, 1, 'aaaaaaa', '<p><img alt="" src="blob:http://localhost/79bfe558-af1d-449f-b047-144b046a08eb" style="width:601px" /><img alt="" src="/public/images/sejarah/images/%E7%A4%BA%E4%BE%8B%E5%9B%BE%E7%89%87_02.jpg" style="height:333px; width:500px" /></p>\r\n\r\n<p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `seleksi`
--

CREATE TABLE `seleksi` (
  `id_seleksi` int(11) NOT NULL,
  `id_atlet` int(11) NOT NULL,
  `id_tahun_seleksi` int(11) NOT NULL,
  `lulus` enum('0','1') NOT NULL,
  `kirim` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `seleksi`
--

INSERT INTO `seleksi` (`id_seleksi`, `id_atlet`, `id_tahun_seleksi`, `lulus`, `kirim`) VALUES
(9, 4, 8, '0', '0'),
(10, 5, 8, '1', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `struktur`
--

CREATE TABLE `struktur` (
  `id_struktur` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `struktur_nama` varchar(200) NOT NULL,
  `struktur_telepon` varchar(200) NOT NULL,
  `struktur_alamat` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `struktur`
--

INSERT INTO `struktur` (`id_struktur`, `id_jabatan`, `id_admin`, `struktur_nama`, `struktur_telepon`, `struktur_alamat`) VALUES
(3, 3, 1, 'Nur Maulidi, S.Pd', '081', 'Pamekasan'),
(4, 4, 1, 'Eka Riyono, M.Pd', '087', 'Pamekasan'),
(5, 5, 1, 'Dedy Bagus Pramudi Wardana, S. Hut, M.Agr', '081', 'Pamekasan'),
(6, 6, 1, 'aAdedd', '099', 'Pamekasan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun_seleksi`
--

CREATE TABLE `tahun_seleksi` (
  `id_tahun_seleksi` int(11) NOT NULL,
  `tahun_seleksi` int(11) NOT NULL,
  `tanggal_buka` date NOT NULL,
  `tanggal_tutup` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tahun_seleksi`
--

INSERT INTO `tahun_seleksi` (`id_tahun_seleksi`, `tahun_seleksi`, `tanggal_buka`, `tanggal_tutup`) VALUES
(8, 2018, '2018-11-04', '2018-11-08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `video`
--

CREATE TABLE `video` (
  `id_video` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_pelatih` int(11) DEFAULT NULL,
  `tanggal_video` date NOT NULL,
  `judul_video` varchar(200) NOT NULL,
  `video` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `video`
--

INSERT INTO `video` (`id_video`, `id_admin`, `id_pelatih`, `tanggal_video`, `judul_video`, `video`) VALUES
(1, 1, NULL, '2018-10-06', 'Terbang 13 Menit Lalu Hilang!', 'Qpc3gMUG5A4'),
(2, 1, NULL, '2018-11-08', 'Atlet Muda FPTI Pamekasan HD', 'xTJtam8w1wY'),
(3, 1, 2, '2018-11-01', 'hhhhhAAA', 'U2nZ6O7YYO'),
(4, 1, NULL, '2018-11-10', 'Tanya Jawab Seru Bersama Ustadz Abdul Somad Lc, MA - Masjid Al-Munawwarah UIR', 'Y8_3RcKpcVc'),
(5, 1, NULL, '2018-11-03', 'test', 'Y8_3RcKpcVc');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `admin_club`
--
ALTER TABLE `admin_club`
  ADD PRIMARY KEY (`id_admin_club`),
  ADD KEY `id_club` (`id_club`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `atlet`
--
ALTER TABLE `atlet`
  ADD PRIMARY KEY (`id_atlet`),
  ADD KEY `id_club` (`id_club`),
  ADD KEY `id_admin` (`id_admin_club`);

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id_club`);

--
-- Indexes for table `dokumen`
--
ALTER TABLE `dokumen`
  ADD PRIMARY KEY (`id_dokumen`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id_foto`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_pelatih` (`id_pelatih`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id_info`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_pelatih` (`id_pelatih`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `kompetisi`
--
ALTER TABLE `kompetisi`
  ADD PRIMARY KEY (`id_kompetisi`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_pelatih` (`id_pelatih`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `id_seleksi` (`id_seleksi`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pelatih`
--
ALTER TABLE `pelatih`
  ADD PRIMARY KEY (`id_pelatih`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id_prestasi`),
  ADD KEY `id_atlet` (`id_atlet`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `sarpras`
--
ALTER TABLE `sarpras`
  ADD PRIMARY KEY (`id_sarpras`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_club` (`id_club`);

--
-- Indexes for table `sejarah`
--
ALTER TABLE `sejarah`
  ADD PRIMARY KEY (`id_sejarah`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `seleksi`
--
ALTER TABLE `seleksi`
  ADD PRIMARY KEY (`id_seleksi`),
  ADD KEY `id_atlet` (`id_atlet`),
  ADD KEY `id_tahun_seleksi` (`id_tahun_seleksi`);

--
-- Indexes for table `struktur`
--
ALTER TABLE `struktur`
  ADD PRIMARY KEY (`id_struktur`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_jabatan` (`id_jabatan`);

--
-- Indexes for table `tahun_seleksi`
--
ALTER TABLE `tahun_seleksi`
  ADD PRIMARY KEY (`id_tahun_seleksi`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id_video`),
  ADD KEY `id_admin` (`id_admin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `admin_club`
--
ALTER TABLE `admin_club`
  MODIFY `id_admin_club` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_anggota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `atlet`
--
ALTER TABLE `atlet`
  MODIFY `id_atlet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `club`
--
ALTER TABLE `club`
  MODIFY `id_club` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `dokumen`
--
ALTER TABLE `dokumen`
  MODIFY `id_dokumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id_info` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kompetisi`
--
ALTER TABLE `kompetisi`
  MODIFY `id_kompetisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pelatih`
--
ALTER TABLE `pelatih`
  MODIFY `id_pelatih` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id_prestasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sarpras`
--
ALTER TABLE `sarpras`
  MODIFY `id_sarpras` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sejarah`
--
ALTER TABLE `sejarah`
  MODIFY `id_sejarah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `seleksi`
--
ALTER TABLE `seleksi`
  MODIFY `id_seleksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `struktur`
--
ALTER TABLE `struktur`
  MODIFY `id_struktur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tahun_seleksi`
--
ALTER TABLE `tahun_seleksi`
  MODIFY `id_tahun_seleksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id_video` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `admin_club`
--
ALTER TABLE `admin_club`
  ADD CONSTRAINT `admin_club_ibfk_1` FOREIGN KEY (`id_club`) REFERENCES `club` (`id_club`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `dokumen`
--
ALTER TABLE `dokumen`
  ADD CONSTRAINT `dokumen_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `foto`
--
ALTER TABLE `foto`
  ADD CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `foto_ibfk_2` FOREIGN KEY (`id_pelatih`) REFERENCES `pelatih` (`id_pelatih`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `info`
--
ALTER TABLE `info`
  ADD CONSTRAINT `info_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `info_ibfk_2` FOREIGN KEY (`id_pelatih`) REFERENCES `pelatih` (`id_pelatih`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kompetisi`
--
ALTER TABLE `kompetisi`
  ADD CONSTRAINT `kompetisi_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kompetisi_ibfk_2` FOREIGN KEY (`id_pelatih`) REFERENCES `pelatih` (`id_pelatih`);

--
-- Ketidakleluasaan untuk tabel `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `nilai_ibfk_1` FOREIGN KEY (`id_seleksi`) REFERENCES `seleksi` (`id_seleksi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pelatih`
--
ALTER TABLE `pelatih`
  ADD CONSTRAINT `pelatih_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `sarpras`
--
ALTER TABLE `sarpras`
  ADD CONSTRAINT `sarpras_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sarpras_ibfk_2` FOREIGN KEY (`id_club`) REFERENCES `club` (`id_club`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `sejarah`
--
ALTER TABLE `sejarah`
  ADD CONSTRAINT `sejarah_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `seleksi`
--
ALTER TABLE `seleksi`
  ADD CONSTRAINT `seleksi_ibfk_1` FOREIGN KEY (`id_atlet`) REFERENCES `atlet` (`id_atlet`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `seleksi_ibfk_2` FOREIGN KEY (`id_tahun_seleksi`) REFERENCES `tahun_seleksi` (`id_tahun_seleksi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `struktur`
--
ALTER TABLE `struktur`
  ADD CONSTRAINT `struktur_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `struktur_ibfk_2` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `video_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
