<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminClub extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin_club';
    protected $table = 'admin_club';
    protected $primaryKey = 'id_admin_club';
    protected $guarded = [];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
