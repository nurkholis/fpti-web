<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Atlet extends Authenticatable
{
    use Notifiable;

    protected $guard = 'atlet';
    protected $table = 'atlet';
    protected $primaryKey = 'id_atlet';
    
    protected $guarded = [];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
