<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Club extends Authenticatable
{
    use Notifiable;

    protected $guard = 'club';
    protected $table = 'club';
    protected $primaryKey = 'id_club';
    public $timestamps = false;
    protected $guarded = [];
}
