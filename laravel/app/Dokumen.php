<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Dokumen extends Authenticatable
{
    use Notifiable;

    protected $guard = 'dokumen';
    protected $table = 'dokumen';
    protected $primaryKey = 'id_dokumen';
    public $timestamps = false;
    protected $guarded = [];
}
