<?php
namespace App\Exports;
use App\Atlet;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class atletExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    public function __construct(int $id_club = null)
    {
        $this->id_club = $id_club;
    }

    public function collection()
    {
        $a = $this->id_club;
        $model = new Atlet;
        $model = $model->select('atlet_nama', 'nama_club', 'atlet_jenis_kelamin', 'atlet_tempat_lahir', 'atlet_tanggal_lahir', 'atlet_golongan_darah',
                                'atlet_telepon', 'atlet_tinggi', 'atlet_berat', 'atlet_alamat', 'atlet_sekolah', 'atlet_nama_ortu')
            ->join('club', 'club.id_club', 'atlet.id_club');
        if( $a ){
            $model = $model->where('club.id_club', $this->id_club);
        }
        $model = $model->get();
        return $model;
    }
    public function headings(): array
    {
        return [
            'Nama', 'Club', 'Gender', 'Tempat Lahir', 'Tanggal Lahir', 'Golongan Darah', 'No Telepon', 'Tinggi', 'Berat', 'Alamat', 'Sekolah', 'Orang Tua',
        ];
    }
    // public function title(): string
    // {
    //     return 'Club ' . $this->nama_club;
    // }
}