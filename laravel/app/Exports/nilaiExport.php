<?php
namespace App\Exports;
use App\Nilai;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\DB;

class nilaiExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    public function __construct(int $tahun_seleksi)
    {
        $this->tahun_seleksi = $tahun_seleksi;
    }

    public function collection()
    {
        $model = new Nilai;
        // $model = $model->select('atlet.atlet_nama', 'atlet.atlet_jenis_kelamin', 'club.nama_club', 'nilai.panjat_lead', 'nilai.panjat_speed',
        //     'nilai.push_up + nilai.sit_up + nilai.push_up + nilai.lari  AS tes_fisik')
            $model = DB::table('nilai')->select(DB::raw('atlet.atlet_nama, atlet.atlet_jenis_kelamin, club.nama_club, ( (nilai.push_up + nilai.sit_up + nilai.push_up + nilai.lari) /4 ) AS tes_fisik, 
                nilai.panjat_lead, nilai.panjat_speed, IF(seleksi.lulus = "1", "LULUS", "GAGAL") AS lulus') )
            ->join('seleksi', 'seleksi.id_seleksi', 'nilai.id_seleksi')
            ->join('tahun_seleksi', 'tahun_seleksi.id_tahun_seleksi', 'seleksi.id_tahun_seleksi')
            ->join('atlet', 'atlet.id_atlet', 'seleksi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->where('tahun_seleksi.tahun_seleksi', $this->tahun_seleksi)
            ->get();
        return $model;
    }
    public function headings(): array
    {
        return [
            'Nama', 'Jenis Kelamin', 'Club', 'Tes Fisik', 'Panjat Lead', 'Panjat Speed' , 'Lulus'
        ];
    }
}