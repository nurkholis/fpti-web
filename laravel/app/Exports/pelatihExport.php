<?php
namespace App\Exports;
use App\Pelatih;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class pelatihExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    public function collection()
    {
        $model = new Pelatih;
        return $model->select('nama_pelatih', 'email', 'alamat_pelatih', 'sertifikasi_pelatih')->get();
    }
    public function headings(): array
    {
        return [
            'Nama', 'E-mail', 'Alamat', 'Sertifikasi', 
        ];
    }
}