<?php
namespace App\Exports;
use App\SarprasClub;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class sarprasClubExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    public function __construct(int $id_club)
    {
        $this->id_club = $id_club;
    }

    public function collection()
    {
        $model = new SarprasClub;
        $model = $model->select('items', 'volume', 'satuan', 'tahun_perolehan', 'kode_barang')
            ->where('id_club', $this->id_club)
            ->get();
        return $model;
    }
    public function headings(): array
    {
        return [
            'Items', 'Volume', 'Satuan', 'Tahun Perolehan', 'Kode Barang' 
        ];
    }
}