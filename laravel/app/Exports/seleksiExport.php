<?php
namespace App\Exports;
use App\Seleksi;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class seleksiExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    public function __construct(int $tahun_seleksi)
    {
        $this->tahun_seleksi = $tahun_seleksi;
    }

    public function collection()
    {
        $model = new Seleksi;
        $model = $model->select('seleksi.id_seleksi', 'atlet.atlet_nama', 'atlet.atlet_jenis_kelamin', 'club.nama_club', 'atlet.atlet_alamat', 'atlet.atlet_tanggal_lahir', 'atlet.atlet_tempat_lahir')
            ->join('tahun_seleksi', 'tahun_seleksi.id_tahun_seleksi', 'seleksi.id_tahun_seleksi')
            ->join('atlet', 'atlet.id_atlet', 'seleksi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->where('tahun_seleksi.tahun_seleksi', $this->tahun_seleksi)
            ->get();
        return $model;
    }
    public function headings(): array
    {
        return [
            'Id Seleksi', 'Nama', 'Gender', 'Club', 'Alamat', 'Tanggal Lahir', 'Tempat Lahir', 'Panjat Lead', 'Panjat Speed', 'Lari', 'Push Up', 'Pull Up', 'Sit Up'
        ];
    }
}