<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Foto extends Authenticatable
{
    use Notifiable;

    protected $guard = 'foto';
    protected $table = 'foto';
    protected $primaryKey = 'id_foto';
    public $timestamps = false;
    protected $guarded = [];
}
