<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Anggota;
use App\Transformers\AnggotaTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Auth;
use Illuminate\Support\Facades\File;

class AnggotaController extends Controller
{
    public function post(Request $request, Anggota $Anggota)
    {
        $this->validate($request, [
            'email' => 'required|unique:anggota',
            'anggota_nama' => 'required|min:3',
            'anggota_nik' => 'required|unique:anggota',
            'anggota_tempat_lahir' => 'required|min:6',
            'anggota_tanggal_lahir' => 'required|date_format:"Y-m-d',
            'anggota_jenis_kelamin' => 'required|in:"L","P"',
            'anggota_tinggi' => 'required:numeric',
            'anggota_berat' => 'required:numeric',
            'anggota_alamat' => 'required',
            'anggota_telepon' => 'required:numeric',
            'anggota_pekerjaan' => 'required',
            'foto' => 'required',
        ]);

        $foto = "";
        if(!$request->foto == null ){
            $destinationPath = "public/images/anggota";
            $image = $request->file('foto');
            $foto = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $foto);
        }

        $Anggota = $Anggota->create([
            'password' => bcrypt(123123123),
            'api_token' => bcrypt( $request->email ),
            'email' => $request->email,
            'anggota_nik' => $request->anggota_nik,
            'anggota_nama' => $request->anggota_nama,
            'anggota_tempat_lahir' => $request->anggota_tempat_lahir,
            'anggota_tanggal_lahir' => $request->anggota_tanggal_lahir,
            'anggota_jenis_kelamin' => $request->anggota_jenis_kelamin,
            'anggota_tinggi' => $request->anggota_tinggi,
            'anggota_berat' => $request->anggota_berat,
            'anggota_alamat' => $request->anggota_alamat,
            'anggota_telepon' => $request->anggota_telepon,
            'anggota_pekerjaan' => $request->anggota_pekerjaan,
            'anggota_foto' => $foto,
        ]);

        $response = fractal()
            ->item($Anggota)
            ->transformWith(new AnggotaTransformer)
            ->includePenjualan()
            ->toArray();

        return response()->json($response, 201);
    }
    public function login(Request $request, Anggota $Anggota)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
         $credential = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if(!Auth::guard('anggota')->attempt($credential)){
            return response()->json(['error' => 'your credential is wrong'], 401);
        }

        $Anggota =  $Anggota->find(Auth::guard('anggota')->id());
        //dd($toko);
        return fractal()
            ->item($Anggota)
            ->transformWith(new AnggotaTransformer)
            ->addMeta([
                'token' => $Anggota->api_token
            ])
            ->toArray();
    }
    public function pwReset(Request $request, Anggota $Anggota)
    {
        $this->validate($request, [
            'password_baru' => 'required|min:6',
            'password_lama' => 'required',
            'email' => 'required|email',
        ]);
        $credential = [
            'email' => $request->email,
            'password' => $request->password_lama
        ];
        if(!Auth::guard('anggota')->attempt($credential)){
            return response()->json(['error' => 'your credential is wrong'], 401);
        }

        $Anggota =  $Anggota->find(Auth::guard('anggota')->user()->id_anggota);
        $Anggota->password = bcrypt($request->password_baru);
        $Anggota->save();
        return fractal()
            ->item($Anggota)
            ->transformWith(new AnggotaTransformer)
            ->addMeta([
                'token' => $Anggota->api_token
            ])
            ->toArray();

    }
    public function edit(Request $request, Anggota $Anggota)
    {

        $Anggota = $Anggota->find(Auth::guard('api-anggota')->user()->id_anggota);
        $foto = "";
        if(!$request->foto == null ){
            $image_path = $destinationPath = "public/images/anggota/".$Anggota->anggota_foto;   
            if (File::exists($image_path)) {
                //dd($image_path, 'file ditemukan');
                File::delete($image_path);
            }
            $destinationPath = "public/images/anggota";
            $image = $request->file('foto');
            $foto = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $foto);

            $Anggota->anggota_foto = $foto;
        }
        $Anggota->email = $request->get('email', $Anggota->email);
        $Anggota->anggota_nik = $request->get('anggota_nik', $Anggota->anggota_nik);
        $Anggota->anggota_nama = $request->get('anggota_nama', $Anggota->anggota_nama);
        $Anggota->anggota_tempat_lahir = $request->get('anggota_tempat_lahir', $Anggota->anggota_tempat_lahir);
        $Anggota->anggota_tanggal_lahir = $request->get('anggota_tanggal_lahir', $Anggota->anggota_tanggal_lahir);
        $Anggota->anggota_jenis_kelamin = $request->get('anggota_jenis_kelamin', $Anggota->anggota_jenis_kelamin);
        $Anggota->anggota_tinggi = $request->get('anggota_tinggi', $Anggota->anggota_tinggi);
        $Anggota->anggota_berat = $request->get('anggota_berat', $Anggota->anggota_berat);
        $Anggota->anggota_alamat = $request->get('anggota_alamat', $Anggota->anggota_alamat);
        $Anggota->anggota_telepon = $request->get('anggota_telepon', $Anggota->anggota_telepon);
        $Anggota->anggota_pekerjaan = $request->get('anggota_pekerjaan', $Anggota->anggota_pekerjaan);
        $Anggota->save();

        $response = fractal()
            ->item($Anggota)
            ->transformWith(new AnggotaTransformer)
            ->addMeta([
                'token' => $Anggota->api_token
            ])
            ->toArray();

        return response()->json($response, 200);
    }
    public function profil(Request $request, Anggota $Anggota)
    {
        $Anggota = $Anggota->find(Auth::guard('api-anggota')->user()->id_anggota);
        $response = fractal()
            ->item($Anggota)
            ->transformWith(new AnggotaTransformer)
            ->toArray();
        return response()->json($response, 200);
    }
}
