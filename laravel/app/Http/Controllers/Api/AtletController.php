<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Atlet;
use App\Transformers\AtletTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class AtletController extends Controller
{
    public function get(Request $request, Atlet $Atlet){
        $param = [];
        $limit = 15;
        $data = $Atlet
            ->join('club', 'club.id_club', '=', 'atlet.id_club')
            ->orderBy('atlet.id_atlet', 'desc');
            if($request->id_atlet != "" ){
                $data = $data
                    ->where('atlet.id_atlet', $request->id_atlet);
                $param['id_atlet']=$request->id_seleksi;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new AtletTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
