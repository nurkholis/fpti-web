<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Dokumen;
use App\Transformers\DokumenTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class DokumenController extends Controller
{
    public function get(Request $request, Dokumen $Dokumen){
        $param = [];
        $limit = 15;
        $data = $Dokumen
            ->orderBy('dokumen.id_dokumen', 'desc');
            if($request->id_dokumen != "" ){
                $data = $data
                    ->where('dokumen.id_dokumen', $request->id_dokumen);
                $param['id_dokumen']=$request->id_dokumen;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
            //dd($data);
        return fractal()
            ->collection($data)
            ->transformWith(new DokumenTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
