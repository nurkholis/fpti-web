<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Foto;
use App\Transformers\FotoTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class FotoController extends Controller
{
    public function get(Request $request, Foto $Foto){
        $param = [];
        $limit = 15;
        $data = $Foto
            //->join('admin', 'admin.id_admin', '=', 'foto.id_admin')
            ->orderBy('foto.id_foto', 'desc');
            if($request->id_foto != "" ){
                $data = $data
                    ->where('foto.id_foto', $request->id_foto);
                $param['id_foto']=$request->id_foto;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new FotoTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
