<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Info;
use App\Transformers\InfoTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class InfoController extends Controller
{
    public function get(Request $request, Info $Info){
        $param = [];
        $limit = 15;
        $data = $Info
            //->join('admin', 'admin.id_admin', '=', 'info.id_admin')
            ->orderBy('info.id_info', 'desc');
            if($request->id_info != "" ){
                $data = $data
                    ->where('info.id_info', $request->id_info);
                $param['id_info']=$request->id_info;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new InfoTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
