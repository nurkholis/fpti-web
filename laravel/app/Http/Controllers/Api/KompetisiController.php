<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\kompetisi;
use App\Transformers\KompetisiTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class KompetisiController extends Controller
{
    public function get(Request $request, kompetisi $kompetisi){
        $param = [];
        $limit = 15;
        $data = $kompetisi
            ->orderBy('kompetisi.id_kompetisi', 'desc');
            if($request->id_kompetisi != "" ){
                $data = $data
                    ->where('kompetisi.id_kompetisi', $request->id_kompetisi);
                $param['id_kompetisi']=$request->id_kompetisi;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
            //dd($data);
        return fractal()
            ->collection($data)
            ->transformWith(new KompetisiTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
