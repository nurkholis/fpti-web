<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Pelatih;
use App\Transformers\PelatihTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class PelatihController extends Controller
{
    public function get(Request $request, Pelatih $Pelatih){
        $param = [];
        $limit = 15;
        $data = $Pelatih
            ->join('admin', 'admin.id_admin', '=', 'pelatih.id_admin')
            ->orderBy('pelatih.id_pelatih', 'desc');
            if($request->id_pelatih != "" ){
                $data = $data
                    ->where('pelatih.id_pelatih', $request->id_pelatih);
                $param['id_pelatih']=$request->id_pelatih;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
            //dd($data);
        return fractal()
            ->collection($data)
            ->transformWith(new PelatihTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
