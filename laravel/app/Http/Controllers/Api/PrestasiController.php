<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Prestasi;
use App\Transformers\PrestasiTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class PrestasiController extends Controller
{
    public function get(Request $request, Prestasi $Prestasi){
        $param = [];
        $limit = 15;
        $data = $Prestasi
            ->select('prestasi.*', 'atlet.id_atlet', 'atlet.atlet_nama', 'club.id_club', 'club.nama_club')
            ->join('atlet', 'atlet.id_atlet', 'prestasi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->orderBy('prestasi.id_prestasi', 'desc');
            if($request->id_prestasi != "" ){
                $data = $data
                    ->where('prestasi.id_prestasi', $request->id_prestasi);
                $param['id_prestasi']=$request->id_prestasi;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new PrestasiTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
