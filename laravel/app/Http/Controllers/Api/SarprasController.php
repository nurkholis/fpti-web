<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sarpras;
use App\Transformers\SarprasTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class SarprasController extends Controller
{
    public function get(Request $request, Sarpras $Sarpras){
        $param = [];
        $limit = 15;
        $data = $Sarpras->select('sarpras.*', 'club.nama_club', 'club.id_club')
            ->leftJoin('club', 'club.id_club', 'sarpras.id_club')
            ->orderBy('sarpras.id_sarpras', 'desc');
            if($request->id_sarpras != "" ){
                $data = $data
                    ->where('sarpras.id_sarpras', $request->id_sarpras);
                $param['id_sarpras']=$request->id_sarpras;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new SarprasTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
