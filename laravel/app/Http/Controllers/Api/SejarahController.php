<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Sejarah;
use App\Transformers\SejarahTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class SejarahController extends Controller
{
    public function get(Request $request, Sejarah $Sejarah){
        $param = [];
        $limit = 15;
        $data = $Sejarah
            ->orderBy('sejarah.id_sejarah', 'desc');
            if($request->id_sejarah != "" ){
                $data = $data
                    ->where('sejarah.id_sejarah', $request->id_sejarah);
                $param['id_sejarah']=$request->id_sejarah;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
            //dd($data);
        return fractal()
            ->collection($data)
            ->transformWith(new SejarahTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
