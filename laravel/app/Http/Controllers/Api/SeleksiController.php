<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Seleksi;
use App\Transformers\SeleksiTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class SeleksiController extends Controller
{
    public function get(Request $request, Seleksi $Seleksi){
        $param = [];
        $limit = 15;
        $data = $Seleksi
            ->join('tahun_seleksi', 'tahun_seleksi.id_tahun_seleksi', '=', 'seleksi.id_tahun_seleksi')
            ->orderBy('seleksi.id_seleksi', 'desc');
            if($request->id_seleksi != "" ){
                $data = $data
                    ->where('seleksi.id_seleksi', $request->id_seleksi);
                $param['id_seleksi']=$request->id_seleksi;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new SeleksiTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
