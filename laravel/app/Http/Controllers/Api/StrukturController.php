<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Struktur;
use App\Transformers\StrukturTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class StrukturController extends Controller
{
    public function get(Request $request, Struktur $Struktur){
        $param = [];
        $limit = 15;
        $data = $Struktur
            ->join('jabatan', 'jabatan.id_jabatan', 'struktur.id_jabatan')
            ->orderBy('struktur.id_struktur', 'desc');
            if($request->id_struktur != "" ){
                $data = $data
                    ->where('struktur.id_struktur', $request->id_struktur);
                $param['id_struktur']=$request->id_struktur;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
            //dd($data);
        return fractal()
            ->collection($data)
            ->transformWith(new StrukturTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
