<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Video;
use App\Transformers\VideoTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class VideoController extends Controller
{
    public function get(Request $request, Video $Video){
        $param = [];
        $limit = 15;
        $data = $Video
            //->join('admin', 'admin.id_admin', '=', 'video.id_admin')
            ->orderBy('video.id_video', 'desc');
            if($request->id_video != "" ){
                $data = $data
                    ->where('video.id_video', $request->id_video);
                $param['id_video']=$request->id_video;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new VideoTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
