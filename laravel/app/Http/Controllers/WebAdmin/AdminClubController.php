<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Club;
use App\AdminClub;
use Illuminate\Support\Facades\File;
use Auth;
use Excel;
use App\Exports\pelatihExport;

class AdminClubController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, AdminClub $AdminClub)
    {   
        $param = [];
        $data = $AdminClub
            ->join('club', 'club.id_club', 'admin_club.id_club')
            ->orderBy('admin_club.id_admin_club', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('admin_club.nama_admin_club', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        return view('webAdmin.adminClub.adminClub-index', ['data' => $data]);
    }
    public function create(Club $Club)
    {
        $Club = $Club->all();
        return view('webAdmin.adminClub.adminClub-form', [
            'club' => $Club,
        ]);
    }
    public function store(Request $request, AdminClub $AdminClub )
    {
        $this->validate($request, [
            'email' => 'required|unique:admin_club',
            'foto_admin_club' => 'required',
        ]);

        $destinationPath = "public/images/foto_admin_club";
        $image = $request->file('foto_admin_club');
        $foto_admin_club = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $foto_admin_club);
        
        $AdminClub = $AdminClub->create([
            'email' => $request->email,
            'nama_admin_club' => $request->nama_admin_club,
            'id_club' => $request->id_club,
            'foto_admin_club' => $foto_admin_club,
            'password' => bcrypt(123123123),
            'api_token' => bcrypt($request->email),
        ]);
        return redirect(route('admin.adminClub.index'));
    }
    public function edit($id, Pelatih $Pelatih)
    {
        $Pelatih = $Pelatih->find($id);
        return view('webAdmin.pelatih.pelatih-form', [
            'edit'  => $Pelatih,
            ]);
    }
    public function reset($id, AdminClub $AdminClub)
    {
        $AdminClub = $AdminClub->find($id);
        $AdminClub->password = bcrypt(123123123);
        $AdminClub->save();
        return back();
    }
    public function destroy($id, AdminClub $AdminClub)
    {
        $AdminClub = $AdminClub->find($id);

        $image_path = "public/images/admin_club/".$AdminClub->foto_admin_club;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $AdminClub->delete();
        return back();
    }

    public function export(Pelatih $Pelatih)
    {
        return Excel::download(new pelatihExport, 'jadi.xlsx');
    }
    
}
