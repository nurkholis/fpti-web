<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Anggota;
use Illuminate\Support\Facades\File;
use Auth;

class AnggotaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Anggota $Anggota)
    {   
        $param = [];
        $data = $Anggota
            ->orderBy('anggota.id_anggota', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('anggota.anggota_nama', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webAdmin.anggota.anggota-index', ['data' => $data]);
    }
    public function destroy($id, Anggota $Anggota)
    {
        $Anggota = $Anggota->find($id);

        $image_path = "public/images/anggota/".$Anggota->anggota_foto;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $Anggota = $Anggota->delete();
        return back();
    }
    
}
