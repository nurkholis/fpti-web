<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Dokumen;
use Illuminate\Support\Facades\File;
use Auth;

class DokumenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Dokumen $Dokumen)
    {   
        $param = [];
        $data = $Dokumen
            ->orderBy('dokumen.id_dokumen', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('dokumen.nama_dokumen', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        return view('webAdmin.dokumen.dokumen-index', ['data' => $data]);
    }
    public function create()
    {
        return view('webAdmin.dokumen.dokumen-form');
    }
    public function store(Request $request, Dokumen $Dokumen )
    {
        $this->validate($request, [
            'nama_dokumen' => 'required|min:6',
            'dokumen' => 'required',
        ]);

        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d');
        
        $destinationPath = "public/files/dokumen";
        $file = $request->file('dokumen');
        $dokumen = time().'.'.$file->getClientOriginalExtension();
        $file->move($destinationPath, $dokumen);

        $Dokumen = $Dokumen->create([
            'id_admin' => Auth::guard('admin')->user()->id_admin,
            'nama_dokumen' => $request->nama_dokumen,
            'tanggal_dokumen' => $date,
            'keterangan_dokumen' => $request->keterangan_dokumen,
            'dokumen' => $dokumen
        ]);

        return redirect(route('admin.dokumen.index'));
    }
    public function edit($id, Dokumen $Dokumen)
    {
        $Dokumen = $Dokumen->find($id);
        return view('webAdmin.dokumen.dokumen-form', [
            'edit'  => $Dokumen,
            ]);
    }
    public function update(Request $request, $id, Dokumen $Dokumen)
    {
        $Dokumen = $Dokumen->find($id);

        if(!$request->dokumen == null ){
            //menghapus gambar lama
            $image_path = $destinationPath = "public/files/dokumen/".$Dokumen->dokumen;     
            if (File::exists($image_path)) {
                //dd($image_path, 'file ditemukan');
                File::delete($image_path);
            }
            //upload gambar baru
            $destinationPath = "public/files/dokumen";
            $file = $request->file('dokumen');
            $dokumen = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $dokumen);
            $Dokumen->dokumen = $dokumen;

        }
        $Dokumen->nama_dokumen = $request->get('nama_dokumen', $Dokumen->nama_dokumen);
        $Dokumen->keterangan_dokumen = $request->get('keterangan_dokumen', $Dokumen->keterangan_dokumen);
        $Dokumen->save();

        return redirect(route('admin.dokumen.index'));
    }
    public function destroy($id, Dokumen $Dokumen)
    {
        $Dokumen = $Dokumen->find($id);

        $image_path = "public/files/dokumen/".$Dokumen->dokumen;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $Dokumen = $Dokumen->delete();
        return back();
    }
    
}
