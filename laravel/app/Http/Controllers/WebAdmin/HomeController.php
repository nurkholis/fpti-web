<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        return view('webAdmin.home');
    }
    
}
