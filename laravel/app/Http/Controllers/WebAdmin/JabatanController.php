<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jabatan;
use Illuminate\Support\Facades\File;
use Auth;

class JabatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Jabatan $Jabatan)
    {   
        $param = [];
        $data = $Jabatan
            ->orderBy('jabatan.id_jabatan', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('jabatan.nama_jabatan', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webAdmin.jabatan.jabatan-index', ['data' => $data]);
    }
    public function create()
    {
        return view('webAdmin.jabatan.jabatan-form');
    }
    public function store(Request $request, Jabatan $Jabatan )
    {
        $this->validate($request, [
            'nama_jabatan' => 'required|min:3',
        ]);

        $Jabatan = $Jabatan->create([
            'nama_jabatan' => $request->nama_jabatan,
        ]);

        return redirect(route('admin.jabatan.index'));
    }
    public function edit($id, Jabatan $Jabatan)
    {
        $Jabatan = $Jabatan->find($id);
        return view('webAdmin.jabatan.jabatan-form', [
            'edit'  => $Jabatan,
            ]);
    }
    public function update(Request $request, $id, Jabatan $Jabatan)
    {
        $Jabatan = $Jabatan->find($id);
        $Jabatan->nama_jabatan = $request->get('nama_jabatan', $Jabatan->nama_jabatan);
        $Jabatan->save();

        return redirect(route('admin.jabatan.index'));
    }
    public function destroy($id, Jabatan $Jabatan)
    {
        $Jabatan = $Jabatan->find($id)->delete();
        return back();
    }
    
}
