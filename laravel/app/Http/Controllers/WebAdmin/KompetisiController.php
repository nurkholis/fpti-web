<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Kompetisi;
use Illuminate\Support\Facades\File;
use Auth;

class KompetisiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Kompetisi $Kompetisi)
    {   
        $param = [];
        $data = $Kompetisi
            ->where('kompetisi.id_admin', Auth::guard('admin')->user()->id_admin)
            ->orderBy('kompetisi.id_kompetisi', 'desc');
        if($request->search != "" ){
            $data = $data
                ->where('kompetisi.nama_kompetisi', 'LIKE', '%'. $request->search . '%' );
                $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        return view('webAdmin.kompetisi.kompetisi-index', ['data' => $data]);
    }
    public function create()
    {
        return view('webAdmin.kompetisi.kompetisi-form');
    }
    public function store(Request $request, Kompetisi $Kompetisi )
    {
        $this->validate($request, [
            'nama_kompetisi' => 'required|min:3',
            'dokumen_kompetisi' => 'required',
            'laporan_kompetisi' => 'required',
            'hasil_kompetisi' => 'required',
        ]);
        
        $destinationPath = "public/files/hasil_kompetisi";
        $file = $request->file('hasil_kompetisi');
        $hasil_kompetisi = time().'.'.$file->getClientOriginalExtension();
        $file->move($destinationPath, $hasil_kompetisi);
        
        $destinationPath = "public/files/dokumen_kompetisi";
        $file = $request->file('dokumen_kompetisi');
        $dokumen_kompetisi = time().'.'.$file->getClientOriginalExtension();
        $file->move($destinationPath, $dokumen_kompetisi);
        
        $destinationPath = "public/files/laporan_kompetisi";
        $file = $request->file('laporan_kompetisi');
        $laporan_kompetisi = time().'.'.$file->getClientOriginalExtension();
        $file->move($destinationPath, $laporan_kompetisi);

        $Kompetisi = $Kompetisi->create([
            'id_admin' => Auth::guard('admin')->user()->id_admin,
            'nama_kompetisi' => $request->nama_kompetisi,
            'keterangan_kompetisi' => $request->keterangan_kompetisi,
            'tanggal_kompetisi' => $request->tanggal_kompetisi,
            'dokumen_kompetisi' => $dokumen_kompetisi,
            'hasil_kompetisi' => $hasil_kompetisi,
            'laporan_kompetisi' => $laporan_kompetisi,

        ]);

        return redirect(route('admin.kompetisi.index'));
    }
    public function edit($id, Kompetisi $Kompetisi)
    {
        $Kompetisi = $Kompetisi->find($id);
        return view('webAdmin.kompetisi.kompetisi-form', [
            'edit'  => $Kompetisi,
            ]);
    }
    public function update(Request $request, $id, Kompetisi $Kompetisi)
    {
        $Kompetisi = $Kompetisi->find($id);

        if(!$request->hasil_kompetisi == null ){
            $image_path = $destinationPath = "public/files/hasil_kompetisi/".$Kompetisi->hasil_kompetisi;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/files/hasil_kompetisi";
            $file = $request->file('hasil_kompetisi');
            $hasil_kompetisi = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $hasil_kompetisi);
            $Kompetisi->hasil_kompetisi = $hasil_kompetisi;
        }

        if(!$request->dokumen_kompetisi == null ){
            $image_path = $destinationPath = "public/files/dokumen_kompetisi/".$Kompetisi->dokumen_kompetisi;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/files/dokumen_kompetisi";
            $file = $request->file('dokumen_kompetisi');
            $dokumen_kompetisi = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $dokumen_kompetisi);
            $Kompetisi->dokumen_kompetisi = $dokumen_kompetisi;
        }

        if(!$request->laporan_kompetisi == null ){
            $image_path = $destinationPath = "public/files/laporan_kompetisi/".$Kompetisi->laporan_kompetisi;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/files/laporan_kompetisi";
            $file = $request->file('laporan_kompetisi');
            $laporan_kompetisi = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $laporan_kompetisi);
            $Kompetisi->laporan_kompetisi = $laporan_kompetisi;
        }

        $Kompetisi->nama_kompetisi = $request->get('nama_kompetisi', $Kompetisi->nama_kompetisi);
        $Kompetisi->keterangan_kompetisi = $request->get('keterangan_kompetisi', $Kompetisi->keterangan_kompetisi);
        $Kompetisi->tanggal_kompetisi = $request->get('tanggal_kompetisi', $Kompetisi->tanggal_kompetisi);
        $Kompetisi->save();

        return redirect(route('admin.kompetisi.index'));
    }
    public function destroy($id, Kompetisi $Kompetisi)
    {
        $Kompetisi = $Kompetisi->find($id);

        $image_path = "public/files/dokumen_kompetisi/".$Kompetisi->dokumen_kompetisi;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $image_path = "public/files/laporan_kompetisi/".$Kompetisi->laporan_kompetisi;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $image_path = "public/files/hasil_kompetisi/".$Kompetisi->hasil_kompetisi;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $Kompetisi = $Kompetisi->delete();
        return back();
    }
    
}
