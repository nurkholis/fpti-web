<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Pelatih;
use Illuminate\Support\Facades\File;
use Auth;
use Excel;
use App\Exports\pelatihExport;

class PelatihController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Pelatih $Pelatih)
    {   
        $param = [];
        $data = $Pelatih
            ->orderBy('pelatih.id_pelatih', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('pelatih.nama_pelatih', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        return view('webAdmin.pelatih.pelatih-index', ['data' => $data]);
    }
    public function create()
    {
        return view('webAdmin.pelatih.pelatih-form');
    }
    public function store(Request $request, Pelatih $Pelatih )
    {
        $this->validate($request, [
            'nama_pelatih' => 'required|min:6',
            'foto_pelatih' => 'required',
            'sertifikat_pelatih' => 'required',
            'email' => 'required|unique:pelatih'
        ]);
        
        $destinationPath = "public/images/pelatih";
        $image = $request->file('foto_pelatih');
        $foto_pelatih = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $foto_pelatih);

        $destinationPath = "public/images/sertifikat";
        $image = $request->file('sertifikat_pelatih');
        $sertifikat_pelatih = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $sertifikat_pelatih);

        $Pelatih = $Pelatih->create([
            'email' => $request->email,
            'id_admin' => Auth::guard('admin')->user()->id_admin,
            'jabatan_pelatih' => $request->jabatan_pelatih,
            'nama_pelatih' => $request->nama_pelatih,
            'alamat_pelatih' => $request->alamat_pelatih,
            'sertifikasi_pelatih' => $request->sertifikasi_pelatih,
            'foto_pelatih' => $foto_pelatih,
            'sertifikat_pelatih' => $sertifikat_pelatih,
            'password' => bcrypt(123123123),
            'api_token' => bcrypt($request->email),
        ]);

        return redirect(route('admin.pelatih.index'));
    }
    public function edit($id, Pelatih $Pelatih)
    {
        $Pelatih = $Pelatih->find($id);
        return view('webAdmin.pelatih.pelatih-form', [
            'edit'  => $Pelatih,
            ]);
    }
    public function update(Request $request, $id, Pelatih $Pelatih)
    {
        $Pelatih = $Pelatih->find($id);
        // jika edit gambar
        if(!$request->foto_pelatih == null ){
            //menghapus gambar lama
            $image_path = $destinationPath = "public/images/pelatih/".$Pelatih->foto_pelatih;     
            if (File::exists($image_path)) {
                //dd($image_path, 'file ditemukan');
                File::delete($image_path);
            }
            //upload gambar baru
            $destinationPath = "public/images/pelatih";
            $image = $request->file('foto_pelatih');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Pelatih->foto_pelatih = $image_name;
        }
        if(!$request->sertifikat_pelatih == null ){
            //menghapus gambar lama
            $image_path = $destinationPath = "public/images/sertifikat/".$Pelatih->sertifikat_pelatih;     
            if (File::exists($image_path)) {
                //dd($image_path, 'file ditemukan');
                File::delete($image_path);
            }
            //upload gambar baru
            $destinationPath = "public/images/sertifikat";
            $image = $request->file('sertifikat_pelatih');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Pelatih->sertifikat_pelatih = $image_name;
        }
        $Pelatih->nama_pelatih = $request->get('nama_pelatih', $Pelatih->nama_pelatih);
        $Pelatih->jabatan_pelatih = $request->get('jabatan_pelatih', $Pelatih->jabatan_pelatih);
        $Pelatih->alamat_pelatih = $request->get('alamat_pelatih', $Pelatih->alamat_pelatih);
        $Pelatih->sertifikasi_pelatih = $request->get('sertifikasi_pelatih', $Pelatih->sertifikasi_pelatih);
        $Pelatih->nama_pelatih = $request->get('nama_pelatih', $Pelatih->nama_pelatih);
        $Pelatih->save();

        return redirect(route('admin.pelatih.index'));
    }
    public function reset($id, Pelatih $Pelatih)
    {
        $Pelatih = $Pelatih->find($id);
        $Pelatih->password = bcrypt(123123123);
        $Pelatih->save();
        return back();
    }
    public function destroy($id, Pelatih $Pelatih)
    {
        $Pelatih = $Pelatih->find($id);

        $image_path = "public/images/pelatih/".$Pelatih->foto_pelatih;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $image_path = "public/images/sertifikat/".$Pelatih->sertifikat_pelatih;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $Pelatih = $Pelatih->delete();
        return back();
    }

    public function export(Pelatih $Pelatih)
    {
        return Excel::download(new pelatihExport, 'jadi.xlsx');
    }
    
}
