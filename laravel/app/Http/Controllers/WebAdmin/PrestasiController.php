<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Prestasi;
use App\Atlet;
use Illuminate\Support\Facades\File;
use Auth;

class PrestasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Prestasi $Prestasi)
    {   
        $param = [];
        $data = $Prestasi
            ->join('atlet', 'atlet.id_atlet', 'prestasi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->orderBy('prestasi.id_prestasi', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('atlet.nama_atlet', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webAdmin.prestasi.prestasi-index', ['data' => $data]);
    }
    public function create(Atlet $Atlet)
    {
        $Atlet = $Atlet->where('status' , '1')->get();
        return view('webAdmin.prestasi.prestasi-form',[
            'atlet' => $Atlet,
        ]);
    }
    public function store(Request $request, Prestasi $Prestasi )
    {
        $this->validate($request, [
            'id_atlet' => 'required',
            'tahun' => 'required|digits:4|integer|min:1900|max:'.(date('Y')+1),
        ]);

        $Prestasi = $Prestasi->create([
            'id_admin' => Auth::guard('admin')->user()->id_admin,
            'id_atlet' => $request->id_atlet,
            'kategori' => $request->kategori,
            'peringkat' => $request->peringkat,
            'tingkat' => $request->tingkat,
            'tahun' => $request->tahun,
            'tempat' => $request->tempat,
        ]);

        return redirect(route('admin.prestasi.index'));
    }
    public function edit($id, Prestasi $Prestasi, Atlet $Atlet)
    {
        $Prestasi = $Prestasi->find($id);
        $Atlet = $Atlet->where('status' , '1')->get();
        return view('webAdmin.prestasi.prestasi-form', [
                'edit'  => $Prestasi,
                'atlet' => $Atlet,
            ]);
    }
    public function update(Request $request, $id, Prestasi $Prestasi)
    {
        $Prestasi = $Prestasi->find($id);
        $Prestasi->id_atlet = $request->get('id_atlet', $Prestasi->id_atlet);
        $Prestasi->kategori = $request->get('kategori', $Prestasi->kategori);
        $Prestasi->peringkat = $request->get('peringkat', $Prestasi->peringkat);
        $Prestasi->tingkat = $request->get('tingkat', $Prestasi->tingkat);
        $Prestasi->tahun = $request->get('tahun', $Prestasi->tahun);
        $Prestasi->tempat = $request->get('tempat', $Prestasi->tempat);
        $Prestasi->save();

        return redirect(route('admin.prestasi.index'));
    }
    public function destroy($id, Prestasi $Prestasi)
    {
        $Prestasi = $Prestasi->find($id)->delete();
        return back();
    }
    
}
