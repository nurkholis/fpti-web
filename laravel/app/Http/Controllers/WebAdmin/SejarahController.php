<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sejarah;
use Illuminate\Support\Facades\File;
use Auth;

class SejarahController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Sejarah $Sejarah)
    {   
        $param = [];
        $data = $Sejarah
            ->orderBy('sejarah.id_sejarah', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('sejarah.judul_sejarah', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webAdmin.sejarah.sejarah-index', ['data' => $data]);
    }
    public function create()
    {
        return view('webAdmin.sejarah.sejarah-form');
    }
    public function store(Request $request, Sejarah $Sejarah )
    {
        $this->validate($request, [
            'judul_sejarah' => 'required|min:3',
            'isi_sejarah' => 'required|min:6',
        ]);

        $Sejarah = $Sejarah->create([
            'id_admin' => Auth::guard('admin')->user()->id_admin,
            'judul_sejarah' => $request->judul_sejarah,
            'isi_sejarah' => $request->isi_sejarah,
        ]);

        return redirect(route('admin.sejarah.index'));
    }
    public function edit($id, Sejarah $Sejarah)
    {
        $Sejarah = $Sejarah->find($id);
        return view('webAdmin.sejarah.sejarah-form', [
            'edit'  => $Sejarah,
            ]);
    }
    public function update(Request $request, $id, Sejarah $Sejarah)
    {
        $Sejarah = $Sejarah->find($id);
        $Sejarah->judul_sejarah = $request->get('judul_sejarah', $Sejarah->judul_sejarah);
        $Sejarah->isi_sejarah = $request->get('isi_sejarah', $Sejarah->isi_sejarah);
        $Sejarah->save();

        return redirect(route('admin.sejarah.index'));
    }
    public function destroy($id, Sejarah $Sejarah)
    {
        $Sejarah = $Sejarah->find($id)->delete();
        return back();
    }
    
}
