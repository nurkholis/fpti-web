<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Seleksi;
use App\TahunSeleksi;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Nilai;
use App\Atlet;
use App\Exports\seleksiExport;
use Excel;

class SeleksiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Seleksi $Seleksi, TahunSeleksi $TahunSeleksi, Nilai $Nilai)
    {   
        $param = [];
        $q_jumlah_peserta = '(SELECT COUNT(id_seleksi) FROM seleksi WHERE seleksi.id_tahun_seleksi = tahun_seleksi.id_tahun_seleksi) AS jumlah_peseta';
        $q_lulus = ', (SELECT COUNT(id_seleksi) FROM seleksi WHERE seleksi.id_tahun_seleksi = tahun_seleksi.id_tahun_seleksi AND seleksi.lulus  = "1") AS lulus';
        $ts = DB::table('tahun_seleksi')
            ->select(DB::raw(' *, ' . $q_jumlah_peserta . $q_lulus))
            ->orderBy('id_tahun_seleksi', 'desc');
        if($request->tahun_seleksi != "" ){
            $ts = $ts
            ->where('tahun_seleksi.tahun_seleksi', $request->tahun_seleksi);
        }
        $ts = $ts->first();
        //ridir tahun seleksi jika tidak ada
        if( ! $ts){
            return redirect()->route('admin.tahunSeleksi.index');
        }
        $t = $TahunSeleksi->all();

        $nilai = $Nilai
            ->join('seleksi', 'seleksi.id_seleksi', 'nilai.id_seleksi')
            ->join('atlet', 'atlet.id_atlet', 'seleksi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->join('tahun_seleksi', 'tahun_seleksi.id_tahun_seleksi', 'seleksi.id_tahun_seleksi')
            ->where('seleksi.id_tahun_seleksi', $ts->id_tahun_seleksi)
            ->where('seleksi.kirim', '1');
        if($request->search_nilai != "" ){
            $nilai = $nilai
                ->where('atlet.atlet_nama', 'LIKE', '%'. $request->search_nilai . '%' );
                $param['search_nilai']=$request->search_nilai;
        }
        $nilai = $nilai->paginate(20)
            ->appends($param);

        $seleksi = $Seleksi
            ->join('atlet', 'atlet.id_atlet', 'seleksi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->join('tahun_seleksi', 'tahun_seleksi.id_tahun_seleksi', 'seleksi.id_tahun_seleksi')
            ->where('seleksi.id_tahun_seleksi', $ts->id_tahun_seleksi);
        if($request->search_seleksi != "" ){
            $seleksi = $seleksi
                ->where('atlet.atlet_nama', 'LIKE', '%'. $request->search_seleksi . '%' );
                $param['search_seleksi']=$request->search_seleksi;
        }

        $seleksi = $seleksi->paginate(20)
            ->appends($param);
        //dd($seleksi);

        $param = [];
        $tahunSeleksi = $TahunSeleksi
            ->orderBy('tahun_seleksi.id_tahun_seleksi', 'desc');
        if($request->search != "" ){
            $tahunSeleksi = $tahunSeleksi
            ->where('tahun_seleksi.tahun_seleksi', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $tahunSeleksi = $tahunSeleksi->paginate(2)
            ->appends($param);
        //dd($tahunSeleksi);

        return view('webAdmin.seleksi.seleksi-index', [
                'tahunSeleksi' => $tahunSeleksi,
                'nilai' => $nilai,
                'seleksi' => $seleksi,
                'tahun_seleksi' => $ts,
                'data_tahun_seleksi' => $t,
            ]);
    }
    public function export($tahun_seleksi)
    {
        return Excel::download(new seleksiExport($tahun_seleksi), 'Seleksi.xlsx');
    }
    public function verifikasi($id_atlet, $status, Atlet $Atlet)
    {
        $Atlet = $Atlet->find($id_atlet);
        $Atlet->status = $status;
        $Atlet->save();
        return back();
    }
    
}
