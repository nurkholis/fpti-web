<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Struktur;
use App\Jabatan;
use Illuminate\Support\Facades\File;
use Auth;

class StrukturController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Struktur $Struktur)
    {   
        $param = [];
        $data = $Struktur
            ->join('jabatan', 'jabatan.id_jabatan', 'struktur.id_jabatan')
            ->orderBy('struktur.id_struktur', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('jabatan.nama_jabatan', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webAdmin.struktur.struktur-index', ['data' => $data]);
    }
    public function create(Jabatan $Jabatan)
    {
        $Jabatan = $Jabatan->all();
        return view('webAdmin.struktur.struktur-form',[
            'jabatan' => $Jabatan,
        ]);
    }
    public function store(Request $request, Struktur $Struktur )
    {
        $this->validate($request, [
            'struktur_nama' => 'required|min:3',
            'id_jabatan' => 'required|unique:struktur'
        ],[
            'id_jabatan.unique' => 'Jabatan sudah diguanakan'
        ]);

        $Struktur = $Struktur->create([
            'id_admin' => Auth::guard('admin')->user()->id_admin,
            'id_jabatan' => $request->id_jabatan,
            'struktur_nama' => $request->struktur_nama,
            'struktur_alamat' => $request->struktur_alamat,
            'struktur_telepon' => $request->struktur_telepon,
        ]);

        return redirect(route('admin.struktur.index'));
    }
    public function edit($id, Struktur $Struktur, Jabatan $Jabatan)
    {
        $Struktur = $Struktur->find($id);
        $Jabatan = $Jabatan->all();
        return view('webAdmin.struktur.struktur-form', [
                'edit'  => $Struktur,
                'jabatan' => $Jabatan,
            ]);
    }
    public function update(Request $request, $id, Struktur $Struktur)
    {
        $Struktur = $Struktur->find($id);
        $Struktur->id_jabatan = $request->get('id_jabatan', $Struktur->id_jabatan);
        $Struktur->struktur_nama = $request->get('struktur_nama', $Struktur->struktur_nama);
        $Struktur->struktur_alamat = $request->get('struktur_alamat', $Struktur->struktur_alamat);
        $Struktur->struktur_telepon = $request->get('struktur_telepon', $Struktur->struktur_telepon);
        $Struktur->save();

        return redirect(route('admin.struktur.index'));
    }
    public function destroy($id, Struktur $Struktur)
    {
        $Struktur = $Struktur->find($id)->delete();
        return back();
    }
    
}
