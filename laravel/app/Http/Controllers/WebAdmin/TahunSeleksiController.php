<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TahunSeleksi;
use Illuminate\Support\Facades\File;
use Auth;

class TahunSeleksiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, TahunSeleksi $TahunSeleksi)
    {   
        $param = [];
        $data = $TahunSeleksi
            ->orderBy('tahun_seleksi.id_tahun_seleksi', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('tahun_seleksi.tahun_seleksi', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webAdmin.tahunSeleksi.tahunSeleksi-index', ['data' => $data]);
    }
    public function create()
    {
        return view('webAdmin.tahunSeleksi.tahunSeleksi-form');
    }
    public function store(Request $request, TahunSeleksi $TahunSeleksi )
    {
        $this->validate($request, [
            'tahun_seleksi' => 'required|min:4|unique:tahun_seleksi',
        ]);

        $TahunSeleksi = $TahunSeleksi->create([
            'tahun_seleksi' => $request->tahun_seleksi,
            'tanggal_buka' => $request->tanggal_buka,
            'tanggal_tutup' => $request->tanggal_tutup,
        ]);

        return redirect(route('admin.seleksi.index'));
    }
    public function edit($id, TahunSeleksi $TahunSeleksi)
    {
        $TahunSeleksi = $TahunSeleksi->find($id);
        return view('webAdmin.tahunSeleksi.tahunSeleksi-form', [
            'edit'  => $TahunSeleksi,
            ]);
    }
    public function update(Request $request, $id, TahunSeleksi $TahunSeleksi)
    {
        $TahunSeleksi = $TahunSeleksi->find($id);
        $TahunSeleksi->tahun_seleksi = $request->get('tahun_seleksi', $TahunSeleksi->tahun_seleksi);
        $TahunSeleksi->tanggal_buka = $request->get('tanggal_buka', $TahunSeleksi->tanggal_buka);
        $TahunSeleksi->tanggal_tutup = $request->get('tanggal_tutup', $TahunSeleksi->tanggal_tutup);
        $TahunSeleksi->save();

        return redirect(route('admin.seleksi.index'));
    }
    public function destroy($id, TahunSeleksi $TahunSeleksi)
    {
        $TahunSeleksi = $TahunSeleksi->find($id)->delete();
        return back();
    }
    
}
