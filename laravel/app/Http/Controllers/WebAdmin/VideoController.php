<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Video;
use Illuminate\Support\Facades\File;
use Auth;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Video $Video)
    {   
        $param = [];
        $data = $Video
            ->orderBy('video.id_video', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('video.judul_video', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webAdmin.video.video-index', ['data' => $data]);
    }
    public function create()
    {
        return view('webAdmin.video.video-form');
    }
    public function store(Request $request, Video $Video )
    {
        $id_admin = Auth::guard('admin')->user() ? Auth::guard('admin')->user()->id_admin: null ;
        $id_pelatih = Auth::guard('pelatih')->user() ? Auth::guard('pelatih')->user()->id_pelatih : null;
        $this->validate($request, [
            'judul_video' => 'required|min:3',
        ]);

        $Video = $Video->create([
            'id_admin' => $id_admin,
            'id_pelatih' => $id_pelatih,
            'judul_video' => $request->judul_video,
            'tanggal_video' => $request->tanggal_video,
            'video' => $request->video,
        ]);

        return redirect(route('admin.video.index'));
    }
    public function edit($id, Video $Video)
    {
        $Video = $Video->find($id);
        return view('webAdmin.video.video-form', [
            'edit'  => $Video,
            ]);
    }
    public function update(Request $request, $id, Video $Video)
    {
        $Video = $Video->find($id);
        $Video->judul_video = $request->get('judul_video', $Video->judul_video);
        $Video->tanggal_video = $request->get('tanggal_video', $Video->tanggal_video);
        $Video->video = $request->get('video', $Video->video);
        $Video->save();

        return redirect(route('admin.video.index'));
    }
    public function destroy($id, Video $Video)
    {
        $Video = $Video->find($id)->delete();
        return back();
    }
    
}
