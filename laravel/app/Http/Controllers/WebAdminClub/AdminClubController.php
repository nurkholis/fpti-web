<?php

namespace App\Http\Controllers\WebAdminClub;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminClub;
use Illuminate\Support\Facades\File;
use Auth;

class AdminClubController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:adminClub');
    }
    public function index(Request $request, AdminClub $AdminClub)
    {   
        $param = [];
        $data = $AdminClub
            ->join('club', 'club.id_club', 'admin_club.id_club')
            ->find(Auth::guard('adminClub')->user()->id_admin_club);
            //dd($data);
        return view('webAdminClub.adminClub.adminClub-index', ['edit' => $data]);
    }
    public function update(Request $request, AdminClub $AdminClub)
    {
        $this->validate($request, [
            'nama_admin_club' => 'required|min:3',
        ]);
        $old = $AdminClub->find(Auth::guard('adminClub')->user()->id_admin_club);
        $AdminClub = $AdminClub->find(Auth::guard('adminClub')->user()->id_admin_club);
        
        if( $old->email != $request->email ){
            $this->validate($request, [
                'email' => 'required|unique:pelatih'
            ]);
            $Admin->email = $request->email;
            $Admin->api_token = bcrypt($request->email);
        }
        if(!$request->foto_admin_club == null ){
            $image_path = $destinationPath = "public/images/admin_club/".$AdminClub->foto_admin_club;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/admin_club";
            $image = $request->file('foto_admin_club');
            $foto_admin_club = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $foto_admin_club);
            $AdminClub->foto_admin_club = $foto_admin_club;
        }

        $AdminClub->nama_admin_club = $request->get('nama_admin_club', $AdminClub->nama_admin_club);
        $AdminClub->save();
        return redirect(route('adminClub.adminClub.index'));
    }
    public function updatePw(Request $request, AdminClub $AdminClub)
    {
        $this->validate($request, [
            'password_baru' => 'required|min:6',
        ]);
        
        if( $request->password_baru != $request->password_konfirmasi ){
            return back()->withErrors(['password tidak konfirmasi tidak sama']);
        }

        $credential = [
            'email' => Auth::guard('adminClub')->user()->email,
            'password' => $request->password_lama,
        ];
        if(Auth::guard('adminClub')->attempt($credential, $request->member)){

            $AdminClub = $AdminClub->find(Auth::guard('adminClub')->user()->id_admin_club);
            $AdminClub->password = bcrypt($request->password_baru);
            $AdminClub->api_token = bcrypt($request->email);
            $AdminClub->save();
            return redirect(route('adminClub.adminClub.index'));

        }else{
            return back()->withErrors(['password salah']);
        }
    }
    
}
