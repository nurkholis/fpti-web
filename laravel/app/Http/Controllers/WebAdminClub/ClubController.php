<?php

namespace App\Http\Controllers\WebAdminClub;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Club;
use App\Pelatih;
use Illuminate\Support\Facades\File;
use Auth;

class ClubController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:adminClub');
    }
    public function index(Request $request, Club $Club)
    {
        $Club = $Club->find( Auth::guard('adminClub')->user()->id_club );
        return view('webAdminClub.club.club-index', ['club' => $Club]);
    }
    public function create()
    {
        return view('webAdmin.club.club-form');
    }
    public function store(Request $request, Club $Club )
    {
        $this->validate($request, [
            'logo_club' => 'required',
        ]);
        
        $destinationPath = "public/images/club";
        $image = $request->file('logo_club');
        $logo_club = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $logo_club);

        $Club = $Club->create([
            'id_admin' => Auth::guard('admin')->user()->id_admin,
            'nama_club' => $request->nama_club,
            'logo_club' => $logo_club,
            'nomor_club' => $request->nomor_club,
            'tanggal_keanggotaan' => $request->tanggal_keanggotaan,
            'alamat_club' => $request->alamat_club,
            'tahun_berdiri' => $request->tahun_berdiri,
            'telepon_club' => $request->telepon_club,
            'ketua_club' => $request->ketua_club,
            'pembina_club' => $request->pembina_club,
        ]);

        return redirect(route('admin.club.index'));
    }
    public function edit($id, Club $Club)
    {
        $Club = $Club->find($id);
        return view('webAdmin.club.club-form', [
            'edit'  => $Club,
            ]);
    }
    public function update(Request $request, $id, Club $Club)
    {
        $Club = $Club->find($id);
        // jika edit gambar
        if(!$request->logo_club == null ){
            //menghapus gambar lama
            $image_path = $destinationPath = "public/images/club/".$Club->logo_club;     
            if (File::exists($image_path)) {
                //dd($image_path, 'file ditemukan');
                File::delete($image_path);
            }
            //upload gambar baru
            $destinationPath = "public/images/club";
            $image = $request->file('logo_club');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Club->logo_club = $image_name;
        }
        $Club->nama_club = $request->get('nama_club', $Club->nama_club);
        $Club->nomor_club = $request->get('nomor_club', $Club->nomor_club);
        $Club->tanggal_keanggotaan = $request->get('tanggal_keanggotaan', $Club->tanggal_keanggotaan);
        $Club->alamat_club = $request->get('alamat_club', $Club->alamat_club);
        $Club->tahun_berdiri = $request->get('tahun_berdiri', $Club->tahun_berdiri);
        $Club->telepon_club = $request->get('telepon_club', $Club->telepon_club);
        $Club->ketua_club = $request->get('ketua_club', $Club->ketua_club);
        $Club->pembina_club = $request->get('pembina_club', $Club->pembina_club);
        $Club->save();

        return redirect(route('admin.club.index'));
    }
    public function destroy($id, Club $Club)
    {
        $Club = $Club->find($id);

        $image_path = "public/images/club/".$Club->logo_club;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $Club = $Club->delete();
        return back();
    }
    
}
