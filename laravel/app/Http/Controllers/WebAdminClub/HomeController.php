<?php

namespace App\Http\Controllers\WebAdminClub;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:adminClub');
    }
    public function index()
    {
        return view('webAdminClub.home');
    }
    
}
