<?php

namespace App\Http\Controllers\WebAdminClub;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sarpras;
use Illuminate\Support\Facades\File;
use Auth;
use Excel;
use App\Exports\sarprasClubExport;

class SarprasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:adminClub');
    }
    public function index(Request $request, Sarpras $Sarpras)
    {   
        $param = [];
        $data = $Sarpras
            ->where('sarpras.id_club', '=', Auth::guard('adminClub')->user()->id_club)
            ->orderBy('sarpras.id_sarpras', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('sarpras.items', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webAdminClub.sarpras.sarpras-index', ['data' => $data]);
    }
    public function create()
    {
        //dd(0);
        return view('webAdminClub.sarpras.sarpras-form');
    }
    public function store(Request $request, Sarpras $Sarpras )
    {
        $this->validate($request, [
            'items' => 'required',
            'kode_barang' => 'required|unique:sarpras',
        ]);

        $Sarpras = $Sarpras->create([
            'id_club' => Auth::guard('adminClub')->user()->id_club,
            'items' => $request->items,
            'volume' => $request->volume,
            'satuan' => $request->satuan,
            'tahun_perolehan' => $request->tahun_perolehan,
            'keterangan_sarpras' => $request->keterangan_sarpras,
            'kode_barang' => $request->kode_barang,
        ]);

        return redirect(route('adminClub.sarpras.index'));
    }
    public function edit($id, Sarpras $Sarpras)
    {
        $Sarpras = $Sarpras->find($id);
        return view('webAdminClub.sarpras.sarpras-form', [
            'edit'  => $Sarpras,
            ]);
    }
    public function update(Request $request, $id, Sarpras $Sarpras)
    {
        $Sarpras = $Sarpras->find($id);
        $Sarpras->items = $request->get('items', $Sarpras->items);
        $Sarpras->volume = $request->get('volume', $Sarpras->volume);
        $Sarpras->satuan = $request->get('satuan', $Sarpras->satuan);
        $Sarpras->tahun_perolehan = $request->get('tahun_perolehan', $Sarpras->tahun_perolehan);
        $Sarpras->keterangan_sarpras = $request->get('keterangan_sarpras', $Sarpras->keterangan_sarpras);
        $Sarpras->kode_barang = $request->get('kode_barang', $Sarpras->kode_barang);
        $Sarpras->save();

        return redirect(route('adminClub.sarpras.index'));
    }
    public function destroy($id, Sarpras $Sarpras)
    {
        $Sarpras = $Sarpras->find($id)->delete();
        return back();
    }
    
}
