<?php

namespace App\Http\Controllers\WebAdminClub;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Atlet;
use App\Seleksi;
use App\TahunSeleksi;
use Illuminate\Support\Facades\File;
use \Illuminate\Support\Facades\DB;
use Auth;
use Config\Helper as Helper;

class SeleksiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:adminClub');
    }
    public function index(Request $request, Atlet $Atlet, Seleksi $Seleksi, TahunSeleksi $TahunSeleksi)
    {   
         // cek apakah pendaftaran unntuk tahun ini sudah di buka
        $buka = $TahunSeleksi->cekTahunSeleksi();
        if( ! count($buka) )
        {
            $baru_buka = $TahunSeleksi->getTahunSeleksi();
            return view('webAdminClub.seleksi.seleksi-index', [
                'baru_buka' => $baru_buka,
            ]);
        }
        
        
        $param0 = [];
        $atlet0 = $Atlet
            ->whereNotIn('atlet.id_atlet', DB::table('seleksi')->pluck('seleksi.id_atlet'))
            ->orderBy('atlet.id_atlet', 'desc');
        if($request->search0 != "" ){
            $atlet0 = $atlet0
            ->where('atlet.atlet_nama', 'LIKE', '%'. $request->search0 . '%' );
            $param0['search0']=$request->search0;
        }
        $atlet0 = $atlet0->paginate(10)
            ->appends($param0);

        $param1 = [];
        $atlet1 = $Seleksi
            ->join('atlet', 'atlet.id_atlet', 'seleksi.id_atlet')
            ->orderBy('atlet.id_atlet', 'desc');
        if($request->search1 != "" ){
            $atlet1 = $atlet1
            ->where('atlet.atlet_nama', 'LIKE', '%'. $request->search1 . '%' );
            $param1['search1']=$request->search1;
        }
        $atlet1 = $atlet1->paginate(10)
            ->appends($param1);
            
        //dd($atlet1);


        return view('webAdminClub.seleksi.seleksi-index', [
                'atlet0' => $atlet0,
                'atlet1' => $atlet1,
            ]);
    }
    public function daftar(Request $request, Seleksi $Seleksi, TahunSeleksi $TahunSeleksi, $status, $id)
    {
        if($status == '1'){
            date_default_timezone_set('Asia/Jakarta');
            $year = date('Y');
            $TahunSeleksi = $TahunSeleksi->where('tahun_seleksi', $year)->first();
            $Seleksi = $Seleksi->create([
                'id_atlet' => $request->id,
                'id_tahun_seleksi' => $TahunSeleksi->id_tahun_seleksi,
                'lulus' => '0',
            ]);
        }else{
            $Seleksi = $Seleksi->where('id_atlet', $id)->delete();
        }
        return back();
    }
    
}
