<?php

namespace App\Http\Controllers\WebAtlet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Atlet;
use Illuminate\Support\Facades\File;
use Auth;

class AtletController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:atlet');
    }
    public function index(Request $request, Atlet $Atlet)
    {   
        $param = [];
        $data = $Atlet->find(Auth::guard('atlet')->user()->id_atlet);
        // dd($data);
        return view('webAtlet.atlet.atlet-index', ['edit' => $data]);
    }
    public function update(Request $request, Atlet $atlet)
    {
        $old = $atlet->find(Auth::guard('atlet')->user()->id_atlet);
        $atlet = $atlet->find(Auth::guard('atlet')->user()->id_atlet);
        if( $old->email != $request->email ){
            $this->validate($request, [
                'email' => 'required|unique:atlet'
            ]);
            $atlet->email = $request->email;
            $atlet->api_token = bcrypt($request->email);
        }
        // jika edit gambar
        if(!$request->atlet_foto == null ){
            //menghapus gambar lama
            $image_path = $destinationPath = "public/images/atlet/foto".$atlet->atlet_foto;     
            if (File::exists($image_path)) {
                //dd($image_path, 'file ditemukan');
                File::delete($image_path);
            }
            //upload gambar baru
            $destinationPath = "public/images/atlet/foto";
            $image = $request->file('atlet_foto');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $atlet->atlet_foto = $image_name;
        }
        $atlet->atlet_nama = $request->get('atlet_nama', $atlet->atlet_nama);
        $atlet->atlet_tempat_lahir = $request->get('atlet_tempat_lahir', $atlet->atlet_tempat_lahir);
        $atlet->atlet_tanggal_lahir = $request->get('atlet_tanggal_lahir', $atlet->atlet_tanggal_lahir);
        $atlet->atlet_jenis_kelamin = $request->get('atlet_jenis_kelamin', $atlet->atlet_jenis_kelamin);
        $atlet->atlet_golongan_darah = $request->get('atlet_golongan_darah', $atlet->atlet_golongan_darah);
        $atlet->atlet_tinggi = $request->get('atlet_tinggi', $atlet->atlet_tinggi);
        $atlet->atlet_berat = $request->get('atlet_berat', $atlet->atlet_berat);
        $atlet->atlet_alamat = $request->get('atlet_alamat', $atlet->atlet_alamat);
        $atlet->atlet_sekolah = $request->get('atlet_sekolah', $atlet->atlet_sekolah);
        $atlet->atlet_telepon = $request->get('atlet_telepon', $atlet->atlet_alamat);
        $atlet->atlet_nama_ortu = $request->get('atlet_nama_ortu', $atlet->atlet_nama_ortu);
        $atlet->save();

        return redirect(route('atlet.atlet.index'));
    }
    public function updatePw(Request $request, Atlet $Atlet)
    {
        $this->validate($request, [
            'password_baru' => 'required|min:6',
        ]);
        
        if( $request->password_baru != $request->password_konfirmasi ){
            return back()->withErrors(['password tidak konfirmasi tidak sama']);
        }

        $credential = [
            'email' => Auth::guard('atlet')->user()->email,
            'password' => $request->password_lama,
        ];
        if(Auth::guard('atlet')->attempt($credential, $request->member)){

            $Atlet = $Atlet->find(Auth::guard('atlet')->user()->id_atlet);
            $Atlet->password = bcrypt($request->password_baru);
            $Atlet->api_token = bcrypt($request->email);
            $Atlet->save();
            return redirect(route('atlet.atlet.index'));

        }else{
            return back()->withErrors(['password salah']);
        }
    }
    public function destroy($id, Sejarah $Sejarah)
    {
        $Sejarah = $Sejarah->find($id)->delete();
        return back();
    }
    
}
