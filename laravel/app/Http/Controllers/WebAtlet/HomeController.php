<?php

namespace App\Http\Controllers\WebAtlet;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:atlet');
    }
    public function index()
    {
        return view('webAtlet.home');
    }
    
}
