<?php

namespace App\Http\Controllers\WebPelatih;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Atlet;
use Illuminate\Support\Facades\File;
use Auth;
use Excel;
use App\Exports\atletExport;

class AtletController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pelatih');
    }
    public function index(Request $request, Atlet $Atlet)
    {   
        $param = [];
        $data = $Atlet
            ->orderBy('atlet.id_atlet', 'desc')
            ->join('club', 'club.id_club', 'atlet.id_club');
            //->where('atlet.id_club', Auth::guard('pelatih')->user()->id_club);
        if($request->search != "" ){
            $data = $data
            ->where('atlet.atlet_nama', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webPelatih.atlet.atlet-index', [
                'data' => $data,
            ]);
    }
    public function create()
    {
        return view('webAdminClub.atlet.atlet-form');
    }
    public function store(Request $request, Atlet $atlet )
    {
        $this->validate($request, [
            'atlet_foto' => 'required',
            'atlet_akte' => 'required',
            'atlet_kk' => 'required',
            'atlet_sp' => 'required',
            'email' => 'required|unique:atlet'
        ]);
        
        $destinationPath = "public/images/atlet/foto";
        $image = $request->file('atlet_foto');
        $atlet_foto = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $atlet_foto);

        $destinationPath = "public/images/atlet/akte";
        $image = $request->file('atlet_akte');
        $atlet_akte = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $atlet_akte);

        $destinationPath = "public/images/atlet/kk";
        $image = $request->file('atlet_kk');
        $atlet_kk = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $atlet_kk);

        $destinationPath = "public/images/atlet/sp";
        $image = $request->file('atlet_sp');
        $atlet_sp = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $atlet_sp);

        $atlet = $atlet->create([
            'id_admin_club' => Auth::guard('adminClub')->user()->id_admin_club,
            'id_club' => Auth::guard('adminClub')->user()->id_club,
            
            'password' => bcrypt(123123123),
            'api_token' => bcrypt($request->email),
            'email' => $request->email,
            'atlet_nama' => $request->atlet_nama,
            'atlet_tempat_lahir' => $request->atlet_tempat_lahir,
            'atlet_tanggal_lahir' => $request->atlet_tanggal_lahir,
            'atlet_jenis_kelamin' => $request->atlet_jenis_kelamin,
            'atlet_golongan_darah' => $request->atlet_golongan_darah,
            'atlet_tinggi' => $request->atlet_tinggi,
            'atlet_berat' => $request->atlet_berat,
            'atlet_alamat' => $request->atlet_alamat,
            'atlet_sekolah' => $request->atlet_sekolah,
            'atlet_telepon' => $request->atlet_telepon,
            'atlet_nama_ortu' => $request->atlet_nama_ortu,
            'atlet_foto' => $atlet_foto,
            'atlet_akte' => $atlet_akte,
            'atlet_kk' => $atlet_kk,
            'atlet_sp' => $atlet_sp,
        ]);

        return redirect(route('adminClub.atlet.index'));
    }
    public function edit($id, atlet $atlet)
    {
        $atlet = $atlet->find($id);
        return view('webAdminClub.atlet.atlet-form', [
            'edit'  => $atlet,
            ]);
    }
    public function update(Request $request, $id, Atlet $atlet)
    {
        $atlet = $atlet->find($id);

        if(!$request->atlet_foto == null ){
            $image_path = $destinationPath = "public/images/atlet/foto/".$atlet->atlet_foto;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/atlet/foto";
            $image = $request->file('atlet_foto');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $atlet->atlet_foto = $image_name;
        }

        if(!$request->atlet_kk == null ){
            $image_path = $destinationPath = "public/images/atlet/kk/".$atlet->atlet_kk;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/atlet/kk";
            $image = $request->file('atlet_kk');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $atlet->atlet_kk = $image_name;
        }

        if(!$request->atlet_akte == null ){
            $image_path = $destinationPath = "public/images/atlet/akte/".$atlet->atlet_akte;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/atlet/akte";
            $image = $request->file('atlet_akte');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $atlet->atlet_akte = $image_name;
        }

        if(!$request->atlet_sp == null ){
            $image_path = $destinationPath = "public/images/atlet/sp/".$atlet->atlet_sp;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/atlet/sp";
            $image = $request->file('atlet_sp');
            $as = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $as);
            $atlet->atlet_sp = $as;
        }

        $atlet->email = $request->get('email', $atlet->atlet_email);
        $atlet->atlet_nama = $request->get('atlet_nama', $atlet->atlet_nama);
        $atlet->atlet_tempat_lahir = $request->get('atlet_tempat_lahir', $atlet->atlet_tempat_lahir);
        $atlet->atlet_tanggal_lahir = $request->get('atlet_tanggal_lahir', $atlet->atlet_tanggal_lahir);
        $atlet->atlet_jenis_kelamin = $request->get('atlet_jenis_kelamin', $atlet->atlet_jenis_kelamin);
        $atlet->atlet_golongan_darah = $request->get('atlet_golongan_darah', $atlet->atlet_golongan_darah);
        $atlet->atlet_tinggi = $request->get('atlet_tinggi', $atlet->atlet_tinggi);
        $atlet->atlet_berat = $request->get('atlet_berat', $atlet->atlet_berat);
        $atlet->atlet_alamat = $request->get('atlet_alamat', $atlet->atlet_alamat);
        $atlet->atlet_sekolah = $request->get('atlet_sekolah', $atlet->atlet_sekolah);
        $atlet->atlet_telepon = $request->get('atlet_telepon', $atlet->atlet_alamat);
        $atlet->atlet_nama_ortu = $request->get('atlet_nama_ortu', $atlet->atlet_nama_ortu);
        $atlet->save();

        return redirect(route('adminClub.atlet.index'));
    }
    public function destroy($id, Atlet $atlet)
    {
        $atlet = $atlet->find($id);

        $image_path = "public/images/atlet/foto".$atlet->atlet_foto;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $image_path = "public/images/atlet/akte".$atlet->atlet_akte;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $image_path = "public/images/atlet/kk".$atlet->atlet_kk;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $image_path = "public/images/atlet/sp".$atlet->atlet_sp;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $atlet = $atlet->delete();
        return back();
    }
    
    public function export()
    {
        return Excel::download(new atletExport(null), 'Atlet.xlsx');
    }
    
}
