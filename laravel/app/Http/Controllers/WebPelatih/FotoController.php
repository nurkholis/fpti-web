<?php

namespace App\Http\Controllers\WebPelatih;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Foto;
use Illuminate\Support\Facades\File;
use Auth;

class FotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pelatih');
    }
    public function index(Request $request, Foto $Foto)
    {   
        $param = [];
        $data = $Foto
            ->where('foto.id_pelatih', Auth::guard('pelatih')->user()->id_pelatih)
            ->orderBy('foto.id_foto', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('foto.judul_foto', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webPelatih.foto.foto-index', ['data' => $data]);
    }
    public function create()
    {
        return view('webPelatih.foto.foto-form');
    }
    public function store(Request $request, Foto $Foto )
    {
        $id_admin = Auth::guard('admin')->user() ? Auth::guard('admin')->user()->id_admin: null ;
        $id_pelatih = Auth::guard('pelatih')->user() ? Auth::guard('pelatih')->user()->id_pelatih : null;
        $this->validate($request, [
            'judul_foto' => 'required|min:3',
            'foto' => 'required',
        ]);
        
        $destinationPath = "public/images/foto";
        $image = $request->file('foto');
        $foto = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $foto);

        $Foto = $Foto->create([
            'id_admin' => $id_admin,
            'id_pelatih' => $id_pelatih,
            'judul_foto' => $request->judul_foto,
            'tanggal_foto' => $request->tanggal_foto,
            'keterangan_foto' => $request->keterangan_foto,
            'foto' => $foto,
        ]);

        return redirect(route('pelatih.foto.index'));
    }
    public function edit($id, Foto $Foto)
    {
        $Foto = $Foto->find($id);
        return view('webPelatih.foto.foto-form', [
            'edit'  => $Foto,
            ]);
    }
    public function update(Request $request, $id, Foto $Foto)
    {
        $Foto = $Foto->find($id);
        // jika edit gambar
        if(!$request->foto == null ){
            //menghapus gambar lama
            $image_path = $destinationPath = "public/images/foto/".$Foto->foto;     
            if (File::exists($image_path)) {
                //dd($image_path, 'file ditemukan');
                File::delete($image_path);
            }
            //upload gambar baru
            $destinationPath = "public/images/foto";
            $image = $request->file('foto');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Foto->foto = $image_name;
        }
        $Foto->judul_foto = $request->get('judul_foto', $Foto->judul_foto);
        $Foto->tanggal_foto = $request->get('tanggal_foto', $Foto->tanggal_foto);
        $Foto->keterangan_foto = $request->get('keterangan_foto', $Foto->keterangan_foto);
        $Foto->save();

        return redirect(route('pelatih.foto.index'));
    }
    public function destroy($id, Foto $Foto)
    {
        $Foto = $Foto->find($id);

        $image_path = "public/images/foto/".$Foto->foto;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $Foto = $Foto->delete();
        return back();
    }
    
}
