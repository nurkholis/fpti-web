<?php

namespace App\Http\Controllers\WebPelatih;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pelatih');
    }
    public function index()
    {
        return view('webPelatih.home');
    }
    
}
