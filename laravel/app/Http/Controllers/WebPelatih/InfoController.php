<?php

namespace App\Http\Controllers\WebPelatih;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Info;
use Illuminate\Support\Facades\File;
use Auth;

class InfoController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:pelatih');
    }
    public function index(Request $request, Info $Info)
    {   
        $param = [];
        $data = $Info
            ->where('info.id_pelatih', Auth::guard('pelatih')->user()->id_pelatih)
            ->orderBy('info.id_info', 'desc');
        if($request->search != "" ){
            $data = $data
            ->where('info.judul_info', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $data = $data->paginate(10)
            ->appends($param);
        //dd($data);
        return view('webPelatih.info.info-index', ['data' => $data]);
    }
    public function create()
    {
        return view('webPelatih.info.info-form');
    }
    public function store(Request $request, Info $Info )
    {
        $id_admin = Auth::guard('admin')->user() ? Auth::guard('admin')->user()->id_admin: null ;
        $id_pelatih = Auth::guard('pelatih')->user() ? Auth::guard('pelatih')->user()->id_pelatih : null;
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d');

        $this->validate($request, [
            'judul_info' => 'required|min:3',
        ]);

        $Info = $Info->create([
            'id_admin' => $id_admin,
            'id_pelatih' => $id_pelatih,
            'judul_info' => $request->judul_info,
            'tanggal_info' => $date,
            'info' => $request->info,
        ]);
        return redirect(route('pelatih.info.index'));
    }
    public function edit($id, Info $Info)
    {
        $Info = $Info->find($id);
        return view('webPelatih.info.info-form', [
            'edit'  => $Info,
            ]);
    }
    public function update(Request $request, $id, Info $Info)
    {
        $Info = $Info->find($id);
        $Info->judul_info = $request->get('judul_info', $Info->judul_info);
        $Info->info = $request->get('info', $Info->info);
        $Info->save();

        return redirect(route('pelatih.info.index'));
    }
    public function destroy($id, Info $Info)
    {
        $Info = $Info->find($id)->delete();
        return back();
    }
    
}
