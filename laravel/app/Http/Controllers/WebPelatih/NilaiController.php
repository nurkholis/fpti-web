<?php

namespace App\Http\Controllers\WebPelatih;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Nilai;
use App\Seleksi;
use App\TahunSeleksi;
use \Illuminate\Support\Facades\DB;
use Auth;
use Excel;
use App\Exports\seleksiExport;
use App\Exports\nilaiExport;
use DataTables;

class NilaiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pelatih');
    }
    public function index(Request $request, Nilai $Nilai, Seleksi $Seleksi, TahunSeleksi $TahunSeleksi)
    {           
        //data tahun seleksi

        $t = $TahunSeleksi;
        $ts = $TahunSeleksi
            ->orderBy('tahun_seleksi.id_tahun_seleksi', 'desc');
        if($request->tahun_seleksi != "" ){
            $ts = $ts
            ->where('tahun_seleksi.tahun_seleksi', $request->tahun_seleksi);
        }
        $ts = $ts->first();
        $TahunSeleksi = $TahunSeleksi->all();
        // dd($ts->id_tahun_seleksi);
        //data seleksi
        $param_seleksi = [];
        $seleksi = $Seleksi
            ->join('atlet', 'atlet.id_atlet', 'seleksi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->join('tahun_seleksi', 'tahun_seleksi.id_tahun_seleksi', 'seleksi.id_tahun_seleksi')
            ->whereNotIn('seleksi.id_seleksi', DB::table('nilai')->pluck('nilai.id_seleksi'))
            ->where('seleksi.id_tahun_seleksi', $ts->id_tahun_seleksi)
            ->where('seleksi.lulus', '0')
            ->orderBy('seleksi.id_seleksi', 'desc');
        if($request->search_seleksi != "" ){
            $seleksi = $seleksi
            ->where('atlet.atlet_nama', 'LIKE', '%'. $request->search_seleksi . '%' );
            $param['search_seleksi']=$request->search_seleksi;
        }
        $seleksi = $seleksi->paginate(20)
            ->appends($param_seleksi);

        //data nilai
        $param = [];
        $nilai = $Nilai
            ->join('seleksi', 'seleksi.id_seleksi', 'nilai.id_seleksi')
            ->join('atlet', 'atlet.id_atlet', 'seleksi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->join('tahun_seleksi', 'tahun_seleksi.id_tahun_seleksi', 'seleksi.id_tahun_seleksi')
            ->where('seleksi.id_tahun_seleksi', $ts->id_tahun_seleksi)
            ->where('seleksi.lulus', '0')
            ;
        if($request->search_nilai != "" ){
            $nilai = $nilai
            ->where('atlet.atlet_nama', 'LIKE', '%'. $request->search_nilai . '%' );
            $param['search_nilai']=$request->search;
        }
        $nilai = $nilai->paginate(20)
            ->appends($param);
        //dd($nilai);
        
        $nilai_lulus = $Nilai
            ->join('seleksi', 'seleksi.id_seleksi', 'nilai.id_seleksi')
            ->join('atlet', 'atlet.id_atlet', 'seleksi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->join('tahun_seleksi', 'tahun_seleksi.id_tahun_seleksi', 'seleksi.id_tahun_seleksi')
            ->where('seleksi.id_tahun_seleksi', $ts->id_tahun_seleksi)
            ->where('seleksi.lulus', '1')
            ->orderBy('nilai.id_nilai', 'desc');
        if($request->search != "" ){
            $nilai_lulus = $nilai_lulus
            ->where('atlet.atlet_nama', 'LIKE', '%'. $request->search . '%' );
            $param['search']=$request->search;
        }
        $nilai_lulus = $nilai_lulus->paginate(10)
            ->appends($param);
        // dd($nilai_lulus);

        return view('webPelatih.nilai.nilai-index', [
                'nilai' => $nilai,
                'nilai_lulus' => $nilai_lulus,
                'seleksi' => $seleksi,
                'tahun_seleksi' => $ts,
                'data_tahun_seleksi' => $TahunSeleksi,
            ]);
    }
    public function store(Request $request, Nilai $Nilai)
    {
        $Nilai = $Nilai->create([
            'id_seleksi' => $request->id_seleksi,
            'id_pelatih' => Auth::guard('pelatih')->user()->id_pelatih,
            'panjat_lead' => $request->panjat_lead,
            'panjat_speed' => $request->panjat_speed,
            'lari' => $request->lari,
            'push_up' => $request->push_up,
            'pull_up' => $request->pull_up,
            'sit_up' => $request->sit_up,
        ]);
        return back();
    }
    public function data( Nilai $Nilai)
    {
        $data = $Nilai
            ->join('seleksi', 'seleksi.id_seleksi', 'nilai.id_seleksi')
            ->join('atlet', 'atlet.id_atlet', 'seleksi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->join('tahun_seleksi', 'tahun_seleksi.id_tahun_seleksi', 'seleksi.id_tahun_seleksi')
            // ->where('seleksi.id_tahun_seleksi', $ts2->id_tahun_seleksi)
            ->where('seleksi.lulus', '1')
            ->orderBy('nilai.id_nilai', 'desc')->get();
        return DataTables::of($data) 
            ->addColumn('action', function ($data) {
                return '<a href="#edit-'.$data->id_seleksi.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->make(true);
    }
    public function lulus($id_seleksi, $lulus ,  Seleksi $Seleksi)
    {
        $Seleksi = $Seleksi->find($id_seleksi);
        $Seleksi->lulus = $lulus;
        $Seleksi->save();
        return back();
    }
    public function kirim($id_seleksi, $kirim ,  Seleksi $Seleksi)
    {
        $Seleksi = $Seleksi->find($id_seleksi);
        $Seleksi->kirim = $kirim;
        $Seleksi->save();
        return back();
    }
    public function exportSeleksi($tahun_seleksi)
    {
        return Excel::download(new seleksiExport($tahun_seleksi), 'Seleksi.xlsx');
    }
    public function export($tahun_seleksi)
    {
        return Excel::download(new nilaiExport($tahun_seleksi), 'Nilai.xlsx');
    }
    
}
