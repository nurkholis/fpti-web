<?php

namespace App\Http\Controllers\WebPelatih;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pelatih;
use Illuminate\Support\Facades\File;
use Auth;

class PelatihController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pelatih');
    }
    public function index(Request $request, Pelatih $Pelatih)
    {   
        $param = [];
        $data = $Pelatih->find(Auth::guard('pelatih')->user()->id_pelatih);
        return view('webPelatih.pelatih.pelatih-index', ['edit' => $data]);
    }
    public function update(Request $request, Pelatih $Pelatih)
    {
        $this->validate($request, [
            'nama_pelatih' => 'required|min:6',
        ]);
        $old = $Pelatih->find(Auth::guard('pelatih')->user()->id_pelatih);
        $Pelatih = $Pelatih->find(Auth::guard('pelatih')->user()->id_pelatih);

        if( $old->email != $request->email ){
            $this->validate($request, [
                'email' => 'required|unique:pelatih'
            ]);
            $Pelatih->email = $request->email;
            $Pelatih->api_token = bcrypt($request->email);
        }
        // jika edit gambar
        if(!$request->foto_pelatih == null ){
            //menghapus gambar lama
            $image_path = $destinationPath = "public/images/pelatih/".$Pelatih->foto_pelatih;     
            if (File::exists($image_path)) {
                //dd($image_path, 'file ditemukan');
                File::delete($image_path);
            }
            //upload gambar baru
            $destinationPath = "public/images/pelatih";
            $image = $request->file('foto_pelatih');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Pelatih->foto_pelatih = $image_name;
        }
        $Pelatih->nama_pelatih = $request->get('nama_pelatih', $Pelatih->nama_pelatih);
        $Pelatih->jabatan_pelatih = $request->get('jabatan_pelatih', $Pelatih->jabatan_pelatih);
        $Pelatih->alamat_pelatih = $request->get('alamat_pelatih', $Pelatih->alamat_pelatih);
        $Pelatih->sertifikasi_pelatih = $request->get('sertifikasi_pelatih', $Pelatih->sertifikasi_pelatih);
        $Pelatih->nama_pelatih = $request->get('nama_pelatih', $Pelatih->nama_pelatih);
        $Pelatih->save();

        return redirect(route('pelatih.pelatih.index'));
    }
    public function updatePw(Request $request, Pelatih $Pelatih)
    {
        $this->validate($request, [
            'password_baru' => 'required|min:6',
        ]);
        
        if( $request->password_baru != $request->password_konfirmasi ){
            return back()->withErrors(['password tidak konfirmasi tidak sama']);
        }

        $credential = [
            'email' => Auth::guard('pelatih')->user()->email,
            'password' => $request->password_lama,
        ];
        if(Auth::guard('pelatih')->attempt($credential, $request->member)){

            $Pelatih = $Pelatih->find(Auth::guard('pelatih')->user()->id_pelatih);
            $Pelatih->password = bcrypt($request->password_baru);
            $Pelatih->api_token = bcrypt($request->email);
            $Pelatih->save();
            return redirect(route('pelatih.pelatih.index'));

        }else{
            return back()->withErrors(['password salah']);
        }
    }
    public function destroy($id, Sejarah $Sejarah)
    {
        $Sejarah = $Sejarah->find($id)->delete();
        return back();
    }
    
}
