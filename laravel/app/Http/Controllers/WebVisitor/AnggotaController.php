<?php

namespace App\Http\Controllers\WebVisitor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Anggota;
use Illuminate\Support\Facades\File;
use Auth;
use Excel;
use App\Exports\anggotaExport;

class AnggotaController extends Controller
{
    public function __construct()
    {
    }
    public function store(Request $request, Anggota $Anggota )
    {

        $this->validate($request, [
            'anggota_foto' => 'required',
            'email' => 'required|unique:anggota',
            'anggota_nik' => 'required|unique:anggota',
        ]);
        
        $destinationPath = "public/images/anggota";
        $image = $request->file('anggota_foto');
        $anggota_foto = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $anggota_foto);

        $Anggota = $Anggota->create([
            'password' => bcrypt(123123123),
            'api_token' => bcrypt($request->email),
            'email' => $request->email,
            'anggota_nik' => $request->anggota_nik,
            'anggota_pekerjaan' => $request->anggota_pekerjaan,
            'anggota_nama' => $request->anggota_nama,
            'anggota_tempat_lahir' => $request->anggota_tempat_lahir,
            'anggota_tanggal_lahir' => $request->anggota_tanggal_lahir,
            'anggota_jenis_kelamin' => $request->anggota_jenis_kelamin,
            'anggota_tinggi' => $request->anggota_tinggi,
            'anggota_berat' => $request->anggota_berat,
            'anggota_alamat' => $request->anggota_alamat,
            'anggota_telepon' => $request->anggota_telepon,
            'anggota_foto' => $anggota_foto,
        ]);
        return back()->with('success', ['berhasil']);
    }
    
}
