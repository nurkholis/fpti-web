<?php

namespace App\Http\Controllers\WebVisitor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Anggota;
use App\Exports\anggotaExport;

class DownloadController extends Controller
{
    public function __construct()
    {
    }
    public function index( $url )
    {
        //$file = base64_encode("http://localhost/fpti/download/12333.pdf");
        $url = base64_decode( $url );
        //dd( $url );
        return view('webVisitor.download', [
                'url' => $url,
            ]);
    }
    
}
