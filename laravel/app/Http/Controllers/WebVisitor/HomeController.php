<?php

namespace App\Http\Controllers\WebVisitor;

use App\Http\Controllers\Controller;
use App\Foto;
use App\Info;
use App\Video;
use App\Sejarah;
use App\Sarpras;
use App\Pelatih;
use App\Dokumen;
use App\Kompetisi;
use App\Atlet;
use App\Prestasi;
use App\Struktur;
use App\Club;
use DataTables;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    public function index(Info $Info, Foto $Foto, Video $Video, Club $Club)
    {
        $Info = $Info
            ->orderBy('info.id_info', 'desc')->take(4)->get();
        $Video = $Video
            ->orderBy('video.id_video', 'desc')->take(8)->get();
        $Foto = $Foto
            ->orderBy('foto.id_foto', 'desc')->take(8)->get();
        $Club = $Club
            ->select('logo_club')
            ->orderBy('club.id_club', 'desc')->get();
        // dd($Club);
        return view('webVisitor.home', [
                'info' => $Info,
                'foto' => $Foto,
                'video' => $Video,
                'club' => $Club,
            ]);
    }
    public function sejarah( Sejarah $Sejarah)
    {
        $Sejarah = $Sejarah
            ->orderBy('sejarah.id_sejarah', 'desc')->get();
        return view('webVisitor.sejarah', ['sejarah' => $Sejarah]);
    }
    public function info( Info $Info)
    {
        $Info = $Info
            ->orderBy('info.id_info', 'desc')->paginate(1);
        return view('webVisitor.info', ['info' => $Info]);
    }
    public function video(Video $Video)
    {
        $Video = $Video
            ->orderBy('video.id_video', 'desc')->paginate(12);
        return view('webVisitor.video', ['video' => $Video]);
    }
    public function foto(Foto $Foto)
    {
        $Foto = $Foto
            ->orderBy('foto.id_foto', 'desc')->paginate(12);
        return view('webVisitor.foto', ['foto' => $Foto]);
    }
    public function fotoView($id_foto, Foto $Foto)
    {
        $Foto = $Foto->find($id_foto);
        //dd($Foto);
        return view('webVisitor.fotoView', ['data' => $Foto]);
    }
    public function pelatih(Pelatih $Pelatih)
    {
        $Pelatih = $Pelatih
            ->orderBy('pelatih.id_pelatih', 'desc')->paginate(10);
        //dd($Pelatih);
        return view('webVisitor.pelatih', ['pelatih' => $Pelatih]);
    }
    public function dokumen(Dokumen $Dokumen)
    {
        $Dokumen = $Dokumen
            ->orderBy('dokumen.id_dokumen', 'desc')->paginate(10);
        //dd($Pelatih);
        return view('webVisitor.dokumen', ['dokumen' => $Dokumen]);
    }
    public function kompetisi(Kompetisi $Kompetisi)
    {
        $Kompetisi = $Kompetisi
            ->orderBy('kompetisi.id_kompetisi', 'desc')->paginate(10);
        //dd($Pelatih);
        return view('webVisitor.kompetisi', ['kompetisi' => $Kompetisi]);
    }
    public function atletProfil($id_atlet, Atlet $Atlet)
    {
        $Atlet = $Atlet
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->find($id_atlet);
        //dd($Atlet);
        return view('webVisitor.atletProfil', ['atlet' => $Atlet]);
    }
    public function prestasiData(Prestasi $Prestasi)
    {
        $data = $Prestasi
            ->join('atlet', 'atlet.id_atlet', 'prestasi.id_atlet')
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->orderBy('prestasi.id_prestasi', 'desc')->get();
        return DataTables::of($data)->make(true);
    }
    public function strukturData(Struktur $Struktur)
    {
        $data = $Struktur
            ->join('jabatan', 'jabatan.id_jabatan', 'struktur.id_jabatan')
            ->orderBy('jabatan.id_jabatan', 'desc')->get();
        return DataTables::of($data)->make(true);
    }
    public function sarprasData(Sarpras $Sarpras)
    {
        $data = $Sarpras
            ->leftJoin('club', 'club.id_club', 'sarpras.id_club')
            ->orderBy('sarpras.id_sarpras', 'desc')->get();
        return DataTables::of($data)
            ->addColumn('pemilik', function ($data) {
                if( $data->id_admin == null){
                    return 'FPTI Pamekasan';
                }else{
                    return $data->nama_club;
                }
            })
            ->make(true);
    }
    public function atletData(Atlet $Atlet)
    {
        $data = $Atlet
            ->join('club', 'club.id_club', 'atlet.id_club')
            ->orderBy('atlet.id_club', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="#edit-'.$data->id_atlet.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
            ';
        })
        ->addColumn('foto', function ($data) {
            return '
                <a href="' . asset('/public/images/atlet/foto').'/'.$data->atlet_foto .'" target="_blank">
                    <img src=" ' . asset('/public/images/atlet/foto').'/'.$data->atlet_foto .' " height="50">
                </a>
            ';
        })
        ->rawColumns(['foto','action'])
        ->make(true);
    }
    
}