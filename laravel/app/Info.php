<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Info extends Authenticatable
{
    use Notifiable;

    protected $guard = 'info';
    protected $table = 'info';
    protected $primaryKey = 'id_info';
    public $timestamps = false;
    protected $guarded = [];
}
