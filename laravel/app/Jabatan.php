<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Jabatan extends Authenticatable
{
    use Notifiable;

    protected $guard = 'jabatan';
    protected $table = 'jabatan';
    protected $primaryKey = 'id_jabatan';
    public $timestamps = false;
    protected $guarded = [];
}
