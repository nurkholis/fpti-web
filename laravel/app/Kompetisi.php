<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Kompetisi extends Authenticatable
{
    use Notifiable;

    protected $guard = 'kompetisi';
    protected $table = 'kompetisi';
    protected $primaryKey = 'id_kompetisi';
    public $timestamps = false;
    protected $guarded = [];
}
