<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Nilai extends Authenticatable
{
    use Notifiable;
    protected $table = 'nilai';
    protected $primaryKey = 'id_nilai';
    public $timestamps = false;
    protected $guarded = [];
}
