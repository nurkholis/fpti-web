<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pelatih extends Authenticatable
{
    use Notifiable;

    protected $guard = 'pelatih';
    protected $table = 'pelatih';
    protected $primaryKey = 'id_pelatih';
    protected $guarded = [];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
