<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Prestasi extends Authenticatable
{
    use Notifiable;

    protected $guard = 'prestasi';
    protected $table = 'prestasi';
    protected $primaryKey = 'id_prestasi';
    public $timestamps = false;
    protected $guarded = [];
}
