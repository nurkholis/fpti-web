<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Sarpras extends Authenticatable
{
    use Notifiable;

    protected $guard = 'sarpras';
    protected $table = 'sarpras';
    protected $primaryKey = 'id_sarpras';
    public $timestamps = false;
    protected $guarded = [];
}
