<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class SarprasClub extends Authenticatable
{
    use Notifiable;

    protected $guard = 'sarpras_club';
    protected $table = 'sarpras_club';
    protected $primaryKey = 'id_sarpras_club';
    public $timestamps = false;
    protected $guarded = [];
}
