<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Seleksi extends Authenticatable
{
    use Notifiable;

    protected $guard = 'seleksi';
    protected $table = 'seleksi';
    protected $primaryKey = 'id_seleksi';
    protected $guarded = [];
    public $timestamps = false;
}
