<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Struktur extends Authenticatable
{
    use Notifiable;

    protected $guard = 'struktur';
    protected $table = 'struktur';
    protected $primaryKey = 'id_struktur';
    public $timestamps = false;
    protected $guarded = [];
}
