<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class TahunSeleksi extends Authenticatable
{
    use Notifiable;

    protected $guard = 'tahun_seleksi';
    protected $table = 'tahun_seleksi';
    protected $primaryKey = 'id_tahun_seleksi';
    public $timestamps = false;
    protected $guarded = [];

    public function cekTahunSeleksi()
    {
        date_default_timezone_set('Asia/Jakarta');
        $dateNow = date('Y-m-d');
        $year = date('Y');
        $data =  $this
            ->where("tahun_seleksi", '=', $year)
            ->where("tanggal_buka", "<=", $dateNow)
            ->where("tanggal_tutup", ">=", $dateNow)
            ->get();
        return $data;
    }
    public function getTahunSeleksi()
    {
        date_default_timezone_set('Asia/Jakarta');
        $year = date('Y');
        $data =  $this
            ->where("tahun_seleksi", '=', $year)
            ->first();
        return $data;
    }
}
