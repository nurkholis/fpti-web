<?php

namespace App\Transformers;

use App\Anggota;
use League\Fractal\TransformerAbstract;

class AnggotaTransformer extends TransformerAbstract
{
    public function transform(Anggota $Model)
    {
        return [
            'id_anggota' => $Model->id_anggota,
            'anggota_nik' => $Model->anggota_nik,
            'anggota_email' => $Model->email,
            'anggota_nama' => $Model->anggota_nama,
            'anggota_tempat_lahir' => $Model->anggota_tempat_lahir,
            'anggota_tanggal_lahir' => $Model->anggota_tanggal_lahir,
            'anggota_jenis_kelamin' => $Model->anggota_jenis_kelamin,
            'anggota_tinggi' => $Model->anggota_tinggi,
            'anggota_berat' => $Model->anggota_berat,
            'anggota_alamat' => $Model->anggota_alamat,
            'anggota_telepon' => $Model->anggota_telepon,
            'anggota_pekerjaan' => $Model->anggota_pekerjaan,
            'anggota_foto' => $Model->anggota_foto,
        ];
    }
}