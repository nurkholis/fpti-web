<?php

namespace App\Transformers;

use App\Atlet;
use League\Fractal\TransformerAbstract;

class AtletTransformer extends TransformerAbstract
{
    public function transform(Atlet $Model)
    {
        return [
            'id_atlet' => $Model->id_atlet,
            'atlet_nama' => $Model->atlet_nama,
            'atlet_tempat_lahir' => $Model->atlet_tempat_lahir,
            'atlet_tanggal_lahir' => $Model->atlet_tanggal_lahir,
            'atlet_jenis_kelamin' => $Model->atlet_jenis_kelamin,
            'atlet_golongan_darah' => $Model->atlet_golongan_darah,
            'atlet_tinggi' => $Model->atlet_tinggi,
            'atlet_berat' => $Model->atlet_berat,
            'atlet_alamat' => $Model->atlet_alamat,
            'atlet_sekolah' => $Model->atlet_sekolah,
            'atlet_telepon' => $Model->atlet_telepon,
            'atlet_nama_ortu' => $Model->atlet_nama_ortu,
            'atlet_foto' => $Model->atlet_foto,
            'atlet_akte' => $Model->atlet_akte,
            'atlet_kk' => $Model->atlet_kk,
            'atlet_sp' => $Model->atlet_sp,
            'club' => [
                'id_club' =>$Model->id_club,
                'nama_club'  => $Model->nama_club,
            ]
        ];
    }
}