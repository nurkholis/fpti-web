<?php

namespace App\Transformers;

use App\Dokumen;
use League\Fractal\TransformerAbstract;

class DokumenTransformer extends TransformerAbstract
{
    public function transform(Dokumen $Model)
    {
        return [
            'id_dokumen' => $Model->id_dokumen,
            'nama_dokumen' => $Model->nama_dokumen,
            'tanggal_dokumen' => $Model->tanggal_dokumen,
            'keterangan_dokumen' => $Model->keterangan_dokumen,
            'dokumen' => $Model->dokumen,
            'en_dokumen' => base64_encode( asset('public/files/dokumen') . '/' . $Model->dokumen ),
        ];
    }
}