<?php

namespace App\Transformers;

use App\Foto;
use League\Fractal\TransformerAbstract;

class FotoTransformer extends TransformerAbstract
{
    public function transform(Foto $Foto)
    {
        return [
            'id_foto' => $Foto->id_foto,
            'judul_foto' => $Foto->judul_foto,
            'tanggal_foto' => $Foto->tanggal_foto,
            'keterangan_foto' => $Foto->keterangan_foto,
            'foto' => $Foto->foto,
            'admin' => [
                'id_admin' =>$Foto->id_admin,
                'nama_admin'  => $Foto->nama_admin,
            ]
        ];
    }
}