<?php

namespace App\Transformers;

use App\Info;
use League\Fractal\TransformerAbstract;

class InfoTransformer extends TransformerAbstract
{
    public function transform(Info $Info)
    {
        return [
            'id_info' => $Info->id_info,
            'judul_info' => $Info->judul_info,
            'tanggal_info' => $Info->tanggal_info,
            'info' => $Info->info,
            'admin' => [
                'id_admin' =>$Info->id_admin,
                'nama_admin'  => $Info->nama_admin,
            ]
        ];
    }
}