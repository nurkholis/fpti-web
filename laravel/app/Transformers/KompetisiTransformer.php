<?php

namespace App\Transformers;

use App\Kompetisi;
use League\Fractal\TransformerAbstract;

class KompetisiTransformer extends TransformerAbstract
{
    public function transform(Kompetisi $Model)
    {
        return [
            'id_kompetisi' => $Model->id_kompetisi,
            'id_admin' => $Model->id_admin,
            'id_pelatih' => $Model->id_pelatih,
            'nama_kompetisi' => $Model->nama_kompetisi,
            'tanggal_kompetisi' => $Model->tanggal_kompetisi,
            'dokumen_kompetisi' => $Model->dokumen_kompetisi,
            'laporan_kompetisi' => $Model->laporan_kompetisi,
            'hasil_kompetisi' => $Model->hasil_kompetisi,
            'en_dokumen_kompetisi' => base64_encode( asset('public/files/dokumen_kompetisi') . '/' . $Model->dokumen_kompetisi ),
            'en_laporan_kompetisi' => base64_encode( asset('public/files/laporan_kompetisi') . '/' . $Model->laporan_kompetisi ),
            'en_hasil_kompetisi' => base64_encode( asset('public/files/hasil_kompetisi') . '/' . $Model->hasil_kompetisi ),
        ];
    }
}