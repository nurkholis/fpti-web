<?php

namespace App\Transformers;

use App\Pelatih;
use League\Fractal\TransformerAbstract;

class PelatihTransformer extends TransformerAbstract
{
    public function transform(Pelatih $Pelatih)
    {
        return [
            'id_pelatih' => $Pelatih->id_pelatih,
            'email' => $Pelatih->email,
            'nama_pelatih' => $Pelatih->nama_pelatih,
            'alamat_pelatih' => $Pelatih->alamat_pelatih,
            'jabatan_pelatih' => $Pelatih->jabatan_pelatih,
            'sertifikat_pelatih' => $Pelatih->sertifikat_pelatih,
            'sertifikasi_pelatih' => $Pelatih->sertifikasi_pelatih,
            'foto_pelatih' => $Pelatih->foto_pelatih,
            'admin' => [
                'id_admin' =>$Pelatih->id_admin,
                'nama_admin'  => $Pelatih->nama_admin,
            ]
        ];
    }
}