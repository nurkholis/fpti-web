<?php

namespace App\Transformers;

use App\Prestasi;
use League\Fractal\TransformerAbstract;

class PrestasiTransformer extends TransformerAbstract
{
    public function transform(Prestasi $Model)
    {
        return [
            'id_prestasi' => $Model->id_prestasi,
            'kategori' => $Model->kategori,
            'peringkat' => $Model->peringkat,
            'tingkat' => $Model->tingkat,
            'tahun' => $Model->tahun,
            'tempat' => $Model->tempat,
            'atlet' => [
                'id_atlet' =>$Model->id_atlet,
                'atlet_nama'  => $Model->atlet_nama,
            ],
            'club' => [
                'id_club' =>$Model->id_club,
                'nama_club'  => $Model->nama_club,
            ]
        ];
    }
}