<?php

namespace App\Transformers;

use App\Sarpras;
use League\Fractal\TransformerAbstract;

class SarprasTransformer extends TransformerAbstract
{
    public function transform(Sarpras $Model)
    {
        $pemilik = 'FPTI Pamekasan';
        if($Model->id_club != null ){
            $pemilik = $Model->nama_club;
        }
        return [
            'id_sarpras' => $Model->id_sarpras,
            'items' => $Model->items,
            'volume' => $Model->volume,
            'satuan' => $Model->satuan,
            'keterangan_sarpras' => $Model->keterangan_sarpras,
            'kode_barang' => $Model->kode_barang,
            'id_admin' => $Model->id_admin,
            'pemilik' => $pemilik,
            'club' => [
                'id_club' =>$Model->id_club,
                'nama_club'  => $Model->nama_club,
            ]
        ];
    }
}