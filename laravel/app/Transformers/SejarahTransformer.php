<?php

namespace App\Transformers;

use App\Sejarah;
use League\Fractal\TransformerAbstract;

class SejarahTransformer extends TransformerAbstract
{
    public function transform(Sejarah $Model)
    {
        return [
            'id_sejarah' => $Model->id_sejarah,
            'judul_sejarah' => $Model->judul_sejarah,
            'isi_sejarah' => $Model->isi_sejarah,
        ];
    }
}