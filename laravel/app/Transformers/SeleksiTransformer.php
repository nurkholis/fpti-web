<?php

namespace App\Transformers;

use App\Seleksi;
use League\Fractal\TransformerAbstract;

class SeleksiTransformer extends TransformerAbstract
{
    public function transform(Seleksi $Model)
    {
        return [
            'id_seleksi' => $Model->id_seleksi,
            'seleksi_nama' => $Model->seleksi_nama,
            'seleksi_tempat_lahir' => $Model->seleksi_tempat_lahir,
            'seleksi_tanggal_lahir' => $Model->seleksi_tanggal_lahir,
            'seleksi_jenis_kelamin' => $Model->seleksi_jenis_kelamin,
            'seleksi_golongan_darah' => $Model->seleksi_golongan_darah,
            'seleksi_tinggi' => $Model->seleksi_tinggi,
            'seleksi_berat' => $Model->seleksi_berat,
            'seleksi_alamat' => $Model->seleksi_alamat,
            'seleksi_sekolah' => $Model->seleksi_sekolah,
            'seleksi_telepon' => $Model->seleksi_telepon,
            'seleksi_nama_ortu' => $Model->seleksi_nama_ortu,
            'seleksi_foto' => $Model->seleksi_foto,
            'seleksi_akte' => $Model->seleksi_akte,
            'seleksi_kk' => $Model->seleksi_kk,
            'seleksi_sp' => $Model->seleksi_sp,
            'tahun_seleksi' => [
                'id_tahun_seleksi' =>$Model->id_tahun_seleksi,
                'tahun_seleksi'  => $Model->tahun_seleksi,
            ]
        ];
    }
}