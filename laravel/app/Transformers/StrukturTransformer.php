<?php

namespace App\Transformers;

use App\Struktur;
use League\Fractal\TransformerAbstract;

class StrukturTransformer extends TransformerAbstract
{
    public function transform(Struktur $Model)
    {
        return [
            'id_struktur' => $Model->id_struktur,
            'struktur_nama' => $Model->struktur_nama,
            'struktur_telepon' => $Model->struktur_telepon,
            'struktur_alamat' => $Model->struktur_alamat,
            'jabatan' => [
                'id_jabatan' =>$Model->id_jabatan,
                'nama_jabatan'  => $Model->nama_jabatan,
            ]
        ];
    }
}