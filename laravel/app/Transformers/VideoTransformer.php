<?php

namespace App\Transformers;

use App\Video;
use League\Fractal\TransformerAbstract;

class VideoTransformer extends TransformerAbstract
{
    public function transform(Video $Video)
    {
        return [
            'id_video' => $Video->id_video,
            'judul_video' => $Video->judul_video,
            'tanggal_video' => $Video->tanggal_video,
            'video' => $Video->video,
            'admin' => [
                'id_admin' =>$Video->id_admin,
                'nama_admin'  => $Video->nama_admin,
            ]
        ];
    }
}