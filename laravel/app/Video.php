<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Video extends Authenticatable
{
    use Notifiable;

    protected $guard = 'video';
    protected $table = 'video';
    protected $primaryKey = 'id_video';
    public $timestamps = false;
    protected $guarded = [];
}
