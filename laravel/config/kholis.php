<?php

namespace Config;
use App\Http\Controllers\Controller;

class Kholis extends Controller
{ 
    public static function coba(){
        dd(0);
    }
    public static function tanggal($tanggal_berita){
        $timestamp = strtotime($tanggal_berita);
        $day = date('D', $timestamp);
        $pecah = explode("-",$tanggal_berita);
        if( count( $pecah ) < 2 ){
            return "";
        }
        switch ($day){
            case 'Mon':
                $day = 'Senin';break;
            case 'Tue':
                $day = 'Selasa';break;
            case 'Wed':
                $day = 'Rabu'; break;
            case 'Thu':
                $day = 'Kamis';break;
            case 'Fri':
                $day = 'Jumat';break;   
            case 'Sat':
                 $day = 'Sabtu';break;
            case 'Sun':
                $day = 'Minggu'; break;
            default:
        }
        $month = date('M', $timestamp);
        switch ($month){
            case 'Jan':
                $month = 'Januari';break;
            case 'Feb':
                $month = 'Faburari';break;
            case 'Mar':
                $month = 'Maret'; break;
            case 'Apr':
                $month = 'April';break;
            case 'May':
                $month = 'Mei';break;   
            case 'Jun':
                 $month = 'Juni';break;
            case 'Jul':
                $month = 'Juli'; break;
            case 'Aug':
                $month = 'Agustus';break;
            case 'Sep':
                $month = 'September';break;
            case 'Oct':
                $month = 'Oktober'; break;
            case 'Nov':
                $month = 'November';break;
            case 'Dec':
                $month = 'Desember';break;
            default:
        }
        $jadi = $day . ', ' . $pecah[2] .' ' . $month . ' ' . $pecah[0];
        return $jadi;
    }

    public static function kelasUmur( $birthDate )
    {
        $birthDate = explode("-", $birthDate);
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[0], $birthDate[1]))) > date("md")
            ? ((date("Y") - $birthDate[0]) - 1)
            : (date("Y") - $birthDate[0]));
        $kelas = "";
        if( $age >= 7 && $age <= 9 ){
            $kelas = "Spider Kid C";
        }
        if( $age >= 10 && $age <= 11 ){
            $kelas = "Spider Kid B";
        }
        if( $age >= 12 && $age <= 13 ){
            $kelas = "Spider Kid A";
        }
        if( $age >= 14 && $age <= 15 ){
            $kelas = "Youth B";
        }
        if( $age >= 16 && $age <= 17 ){
            $kelas = "Youth A";
        }
        if( $age >= 18  ){
            $kelas = "Junior";
        }
        return $kelas;
    }
}