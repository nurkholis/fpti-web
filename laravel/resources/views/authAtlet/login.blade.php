<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Material Dashboard by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('public/assets/css/material-dashboard.css?v=2.1.0') }}" rel="stylesheet">
  @yield('js')
</head>

<body>
<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <br><br><br><br>
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title">Login</h4>
                  <p class="card-category">Atlet FPTI</p>
                </div>
                <div class="card-body">
                    <form class="login-form" method="POST" action="{{ route('atlet.login.submit') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="bmd-label-floating">Username</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="bmd-label-floating">Password</label>
                            <input type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <br>
                        <button type="submit" class="btn btn-info pull-right">Login</button>
                    </form>
                </div>
              </div>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
</body>

</html>