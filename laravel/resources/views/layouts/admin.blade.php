<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
  {{ config('app.name') }}
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('public/assets/css/material-dashboard.css?v=2.1.0') }}" rel="stylesheet">
  <link href="{{ asset('public/assets/css/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('public/assets/css/pagination.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('public/assets/vendor/font-awesome/font-awesome.min.css') }}" rel="stylesheet"> -->
  @yield('css')
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          {{ Auth::guard('admin')->user()->nama_admin }}
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          
          <li class="nav-item">
            <a href="{{ route('admin.home') }}" class="nav-link">
              <i class="material-icons">home</i>
              <p>Beranda</p>
            </a>
          </li>
          
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown">
              <i class="material-icons">menu</i>
              <p>Profil</p>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="{{ route('admin.sejarah.index') }}"> <i class="material-icons">history</i> Sejarah</a>
              <a class="dropdown-item" href="{{ route('admin.jabatan.index') }}"> <i class="material-icons">border_inner</i> Jabatan</a>
              <a class="dropdown-item" href="{{ route('admin.struktur.index') }}"> <i class="material-icons">border_clear</i> Struktur</a>
              <a class="dropdown-item" href="{{ route('admin.sarpras.index') }}"> <i class="material-icons">weekend</i> Sarpras</a>
              <a class="dropdown-item" href="{{ route('admin.pelatih.index') }}"> <i class="material-icons">directions_walk</i> Pelatih</a>
              <a class="dropdown-item" href="{{ route('admin.dokumen.index') }}"> <i class="material-icons">folder</i> Dokumen</a>
            </div>
          </li>
          
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown"">
              <i class="material-icons">photo_library</i>
              <p>Media</p>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="{{ route('admin.info.index') }}"> <i class="material-icons">info</i> Informasi</a>
              <a class="dropdown-item" href="{{ route('admin.video.index') }}"> <i class="material-icons">videocam</i> Video</a>
              <a class="dropdown-item" href="{{ route('admin.foto.index') }}"> <i class="material-icons">photo</i> Foto</a>
            </div>
          </li>
          
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown"">
              <i class="material-icons">group_work</i>
              <p>Club</p>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="{{ route('admin.club.index') }}"> <i class="material-icons">group_work</i> Club</a>
              <a class="dropdown-item" href="{{ route('admin.adminClub.index') }}"> <i class="material-icons">room_service</i> Admin Club</a>
            </div>
          </li>
          
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown"">
              <i class="material-icons">local_bar</i>
              <p>Kompetisi</p>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="{{ route('admin.kompetisi.index') }}"> <i class="material-icons">insert_drive_file</i> Kompetisi</a>
            </div>
          </li>
          
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown"">
              <i class="material-icons">directions_bike</i>
              <p>Atlet</p>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
            {{-- <a class="dropdown-item" href="{{ route('admin.tahunSeleksi.index') }}"> <i class="material-icons">calendar_today</i> Tahun Seleksi</a> --}}
            <a class="dropdown-item" href="{{ route('admin.anggota.index') }}"> <i class="material-icons">account_box</i> Pendaftaran umum</a>
            <a class="dropdown-item" href="{{ route('admin.seleksi.index') }}"> <i class="material-icons">account_box</i> Seleksi Atlet</a>
            <a class="dropdown-item" href="{{ route('admin.atlet.index') }}"> <i class="material-icons">directions_bike</i> Data Atlet</a>
            <a class="dropdown-item" href="{{ route('admin.prestasi.index') }}"> <i class="material-icons">star</i> Prestasi</a>
            </div>
          </li>

        <li class="nav-item active-pro ">
            <a class="nav-link" href="https://documenter.getpostman.com/view/2916424/RWgm2gF1#96a4e0dc-d9fe-4330-8133-9a17fe5ddd54">
                <i class="material-icons">code</i>
                <p>Developer API</p>
            </a>
            <!-- <p class=" text-center ">Copyright 2018 FPTI Pamekasan</p> -->
        </li>

          <!-- <li class="nav-item active-pro ">
                <a class="nav-link" href="./upgrade.html">
                    <i class="material-icons">unarchive</i>
                    <p>Upgrade to PRO</p>
                </a>
            </li> -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
          @yield('breadcrumb')
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.logout') }}">
                  <i class="material-icons">arrow_forward</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      @yield('content') 
      {{-- <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li><i class="material-icons">place</i> <span>Pamekasan</span></li>
              <li><i class="material-icons">phone</i> <span>08555555555</span></li>
              <li><i class="material-icons">email</i> <span>fpti.pamekasan@gmail.com</span></li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>document.write(new Date().getFullYear())</script> FPTI Pamekasan
          </div>
        </div>
      </footer> --}}
    </div>
  </div>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->

  <script src="{{ asset('public/assets/js/core/jquery.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/core/bootstrap-material-design.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/plugins/chartist.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/plugins/bootstrap-notify.js') }}"></script>
  <script src="{{ asset('public/assets/js/material-dashboard.min.js?v=2.1.0') }}"></script>
  @yield('js')
</body>

</html>