<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
  {{ config('app.name') }}
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('public/assets/css/material-dashboard.css?v=2.1.0') }}" rel="stylesheet">
  <link href="{{ asset('public/assets/css/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('public/assets/css/pagination.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('public/assets/vendor/font-awesome/font-awesome.min.css') }}" rel="stylesheet"> -->
  @yield('css')
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="orange" data-background-color="azure" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          {{ Auth::guard('pelatih')->user()->nama_pelatih }}
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          
          <li class="nav-item">
            <a href="{{ route('pelatih.home') }}" class="nav-link">
              <i class="material-icons">home</i>
              <p>Beranda</p>
            </a>
          </li>
          
          <li class="nav-item ">
            <a class="nav-link" href="{{ route('pelatih.pelatih.index') }}">
              <i class="material-icons">face</i>
              <p>Profil</p>
            </a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown"">
              <i class="material-icons">photo_library</i>
              <p>Media</p>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="{{ route('pelatih.info.index') }}"> <i class="material-icons">info</i> Informasi</a>
              <a class="dropdown-item" href="{{ route('pelatih.video.index') }}"> <i class="material-icons">videocam</i> Video</a>
              <a class="dropdown-item" href="{{ route('pelatih.foto.index') }}"> <i class="material-icons">photo</i> Foto</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown"">
              <i class="material-icons">local_bar</i>
              <p>Kompetisi</p>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="{{ route('pelatih.kompetisi.index') }}"> <i class="material-icons">insert_drive_file</i> Dokumen</a>
            </div>
          </li>
          
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown"">
              <i class="material-icons">directions_bike</i>
              <p>Atlet</p>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="{{ route('pelatih.nilai.index') }}"> <i class="material-icons">account_box</i> Seleksi</a>
            <a class="dropdown-item" href="{{ route('pelatih.atlet.index') }}"> <i class="material-icons">directions_bike</i> Data Atlet</a>
            <a class="dropdown-item" href="{{ route('pelatih.prestasi.index') }}"> <i class="material-icons">star</i> Prestasi</a>
            </div>
          </li>

        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
          @yield('breadcrumb')
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="{{ route('pelatih.logout') }}">
                  <i class="material-icons">arrow_forward</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      @yield('content') 
      <!-- <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href="https://www.creative-tim.com">
                  Creative Tim
                </a>
              </li>
              <li>
                <a href="https://creative-tim.com/presentation">
                  About Us
                </a>
              </li>
              <li>
                <a href="http://blog.creative-tim.com">
                  Blog
                </a>
              </li>
              <li>
                <a href="https://www.creative-tim.com/license">
                  Licenses
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
          </div>
        </div>
      </footer> -->
    </div>
  </div>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->

  <script src="{{ asset('public/assets/js/core/jquery.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/core/bootstrap-material-design.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/plugins/chartist.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/plugins/bootstrap-notify.js') }}"></script>
  <script src="{{ asset('public/assets/js/material-dashboard.min.js?v=2.1.0') }}"></script>
  @yield('js')
</body>

</html>