<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/public/images/logo.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('/public/images/logo.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
   FPTI Pamekasan
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('public/assets/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('public/assets/css/visitor.css') }}" rel="stylesheet">
  <link href="{{ asset('public/assets/css/pagination.css') }}" rel="stylesheet">
  {{-- <link href="{{ asset('public/assets/css/pagination.css') }}" rel="stylesheet"> --}}
  @yield('css')

</head>
<body class="visitor">
	<header>
    <nav class="navbar  navbar-expand-lg navbar-light bg-light">
      <div class="container">
          <img src="{{ asset('/public/images/logo.png') }}" class="img-brand" alt="">
        <a class="navbar-brand" href="{{ route('visitor.visitor.index') }}"> <b>FPTI Pamekasan</b></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Tentang
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ route('visitor.sejarah.index') }}">Sejarah</a>
              <a class="dropdown-item" href="{{ route('visitor.struktur.index') }}">Pengurus</a>
              <a class="dropdown-item" href="{{ route('visitor.sarpras.index') }}">Sarana dan Prasarana</a>
              <a class="dropdown-item" href="{{ route('visitor.pelatih.index') }}">Pelatih</a>
              <a class="dropdown-item" href="{{ route('visitor.dokumen.index') }}">Dokumen</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Media
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ route('visitor.info.index') }}">Informasi</a>
              <a class="dropdown-item" href="{{ route('visitor.video.index') }}">Video</a>
              <a class="dropdown-item" href="{{ route('visitor.foto.index') }}">Foto</a>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ route('visitor.kompetisi.index') }}">Kompetisi</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Atlet
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ route('visitor.atlet.index') }}">Atlet FPTI</a>
              <a class="dropdown-item" href="{{ route('visitor.prestasi.index') }}">Prestasi</a>
            <a class="dropdown-item" href="{{ route('visitor.daftar.index') }}">Pendaftaran</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://documenter.getpostman.com/view/2916424/RWgm2gF1">REST Api</a>
          </li>
        </ul>
        </div>
      </div>
    </nav>
	</header>

  <div class="content">
    @yield('content')
  <div>

<!-- Footer -->
<section id="footer">
  <div class="container">
    <div class="row text-center text-xs-center text-sm-left text-md-left">
      <div class="col-xs-12 col-sm-4 col-md-4">
        <h5>FPTI Pamekasan</h5>
        <ul class="list-unstyled quick-links">
          <li><i class="material-icons">place</i> <span>Sekretariat : Jl. Nugroho Gg. I / No. 4b Pamekasan
              69323 </span></li>
          <li><i class="material-icons">phone</i> <span> 0811353113</span></li>
          <li><i class="material-icons">email</i> <span>fpti.pamekasan@gmail.com
            </span></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
        <ul class="list-unstyled list-inline social text-center">
          <li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/profile.php?id=100012465304722"><i class="fa fa-facebook"></i></a></li>
          <li class="list-inline-item"><a target="_blank" href="https://www.instagram.com/fpti.pamekasan/?hl=id"><i class="fa fa-instagram"></i></a></li>
          <li class="list-inline-item"><a target="_blank" href="http://www.fptipamekasan.or.id/"><i class="fa fa-globe"></i></a></li>
        </ul>
      </div>
      </hr>
    </div>	
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white"
        <p class="h6">&copy <script>document.write(new Date().getFullYear())</script> FPTI Pamekasan</p>
      </div>
      </hr>
    </div>	
  </div>
</section>
<!-- ./Footer -->
  <script src="{{ asset('public/assets/js/core/jquery.min.js') }}"></script>
  <script src="{{ asset('public/assets/js//bootstrap.bundle.min.js') }}"></script>
    @yield('js')
  
</body>
</html>