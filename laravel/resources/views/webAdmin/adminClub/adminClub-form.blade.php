@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('admin.adminClub.index') }}">Admin Club</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Admin Club</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.adminClub.put', $edit->id_admin_club) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.adminClub.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">nama</label>
    <input type="text" class="form-control" name="nama_admin_club" 
    @if( isset($edit) ) value="{{ $edit->nama_admin_club }}" @else value="{{ old('nama_admin_club') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">email</label>
    <input type="email" class="form-control" name="email"
     @if( isset($edit) ) value="{{ $edit->email }}" @else value="{{ old('email') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">Club</label>
    <select class="form-control" name="id_club">
        @if (isset($edit))
            @foreach ($club as $item)
                <option value="{{ $item->id_club }}" {{ $item->id_club == $edit->id_club ? 'selected' : '' }} >{{ $item->nama_club }}</option>
            @endforeach
        @else
            @foreach ($club as $item)
                <option value="{{ $item->id_club }}"> {{ $item->nama_club }}</option>
            @endforeach
        @endif
    </select>
</div>

<div class="input-file-box row">
    <div class="col-md-4 mt-40">
        <button type="submit" class="btn btn-success">Foto</button>
        <input class="form-control input-file" type="file" id="input-foto" name="foto_admin_club" accept=".jpg,.png">
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                    <img src="" id="prev-foto" class="img-preview"> 
                @if(isset($edit))
                    <span>baru</span>
                @endif    
            </div>
            <div class="col-md-6">
                @if(isset($edit))
                    <img src="{{ asset('/public/images/admin_club').'/'.$edit->foto_admin_club }}" alt="" class="img-preview">
                    <span>lama</span>
                @endif    
            </div>
        </div>
    </div>
    <hr>
 </div>

<br>

<div class="alert alert-info alert-with-icon" data-notify="container">
    <i class="material-icons" data-notify="icon">info</i>
    <span data-notify="message">Default Password untuk akun baru <strong>123123123</strong></span>
</div>

                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
    $("#input-foto").change(function() {
        readURL(this);
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection