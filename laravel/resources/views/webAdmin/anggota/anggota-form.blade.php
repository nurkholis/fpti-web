@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('admin.jabatan.index') }}">Jabatan</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Jabatan</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.jabatan.put', $edit->id_jabatan) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.jabatan.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">nama jabatan</label>
    <input type="text" class="form-control" name="nama_jabatan"
     @if( isset($edit) ) value="{{ $edit->nama_jabatan }}" @else value="{{ old('nama_jabatan') }}" @endif required>
</div>

                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-sertifikat').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-foto").change(function() {
        readURL(this);
    });

    $("#input-sertifikat").change(function() {
        readURL1(this);
    });
</script>
@endsection