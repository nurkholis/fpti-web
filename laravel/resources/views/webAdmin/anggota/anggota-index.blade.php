@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
@endsection

@php
use Config\Kholis as Helper;
@endphp

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
          <h4 class="card-title">Anggota</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                @if( Request::get('search') )
                <div class="alert alert-info alert-with-icon" data-notify="container">
                  <i class="material-icons" data-notify="icon">info</i>
                  <a href="{{ route('admin.anggota.index') }}" class="close" aria-label="Close">
                    <i class="material-icons">close</i>
                  </a>
                  <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search') }}"</strong></span>
                </div>
                @endif
              </div>
              <div class="col-md-6 float-right">
              <form class="navbar-form">
                <span class="bmd-form-group"><div class="input-group no-border">
                  <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Cari..." autofocus>
                  <button type="submit" class="btn btn-success btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                    <div class="ripple-container"></div>
                  </button>
                </div>
                </span>
              </form>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-success">
                  <th>NIK</th>
                  <th>Nama </th>
                  <th>Email </th>
                  <th>Alamat</th>
                  <th>Gender</th>
                  <th>Tetala</th>
                  <th>Telepon</th>
                  <th>Pekerjaan</th>
                  <th>Berat</th>
                  <th>Tinggi</th>
                  <th>Aksi</th>
                </thead>
                <tbody>
                  @foreach($data as $item)
                    <tr>
                      <td>{{ $item->anggota_nik }}</td>
                      <td>{{ $item->anggota_nama }}</td>
                      <td>{{ $item->email }}</td>
                      <td>{{ $item->anggota_alamat }}</td>
                      <td>{{ $item->anggota_jenis_kelamin }}</td>
                      <td>{{ $item->anggota_tempat_lahir }}, {{ Helper::tanggal( $item->anggota_tanggal_lahir ) }}</td>
                      <td>{{ $item->anggota_telepon }}</td>
                      <td>{{ $item->anggota_pekerjaan }}</td>
                      <td>{{ $item->anggota_berat }} kg</td>
                      <td>{{ $item->anggota_tinggi }} cm</td>
                      <td class="td-actions">
                        <a href="{{ route('admin.anggota.delete', $item->id_anggota) }}" onclick="confirmDelete()" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Hapus">
                          <i class="material-icons">close</i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="float-right">
                  {{ $data->links() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
    <script>
        function confirmDelete(){
            if (!confirm("hapus data?")) {
                event.preventDefault()
            }
        }
    </script>
@endsection