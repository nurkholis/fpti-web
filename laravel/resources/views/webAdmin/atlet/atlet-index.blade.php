@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
@endsection

@php
use Config\Kholis as Helper;
@endphp

@section('content')


<div class="content">
  {{-- <a href="{{ route('admin.atlet.create') }}" class="btn btn-success btn-round btn-just-icon btn-floating">
    <i class="material-icons">add</i>
    <div class="ripple-container"></div>
  </a> --}}
  <button id="btn-print" class="btn btn-success btn-round btn-just-icon btn-floating b-100">
    <i class="material-icons">print</i>
    <div class="ripple-container"></div>
  </button>
  <a href="{{ route('admin.atlet.export') }}" class="btn btn-success btn-round btn-just-icon btn-floating b-150">
    <i class="material-icons">import_export</i>
    <div class="ripple-container"></div>
  </a>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">

        <div class="card">
          <div class="card-header card-header-success">
          <h4 class="card-title">Data Atlet</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                @if( Request::get('search') )
                <div class="alert alert-info alert-with-icon" data-notify="container">
                  <i class="material-icons" data-notify="icon">info</i>
                  <a href="{{ route('admin.atlet.index') }}" class="close" aria-label="Close">
                    <i class="material-icons">close</i>
                  </a>
                  <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search') }}"</strong></span>
                </div>
                @endif
              </div>
              <div class="col-md-6 float-right">
              <form class="navbar-form">
                <span class="bmd-form-group"><div class="input-group no-border">
                  <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Cari..." autofocus>
                  <button type="submit" class="btn btn-success btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                    <div class="ripple-container"></div>
                  </button>
                </div>
                </span>
              </form>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table" id="tabel-atlet">
                <thead class=" text-success">
                  <th>Nama</th>
                  <th>Club</th>
                  <th>Email</th>
                  <th>Gender</th>
                  <th>Tinggi</th>
                  <th>Berat</th>
                  <th>Tetala</th>
                  <th>Telepon</th>
                  <th>Alamat</th>
                  <th>Verifikasi</th>
                  <th>Aksi</th>
                </thead>
                <tbody>
                  @foreach($data as $item)
                    <tr>
                      <td>{{ $item->atlet_nama }}</td>
                      <td>{{ $item->nama_club }}</td>
                      <td>{{ $item->email }}</td>
                      <td>{{ $item->atlet_jenis_kelamin }}</td>
                      <td>{{ $item->atlet_tinggi }}</td>
                      <td>{{ $item->atlet_berat }}</td>
                      <td>{{ $item->atlet_tempat_lahir }}, {{ Helper::tanggal($item->atlet_tanggal_lahir) }}</td>
                      <td>{{ $item->atlet_telepon }}</td>
                      <td>{{ $item->atlet_alamat }}</td>
                      <td>
                        @if ($item->status)
                          <span class="badge badge-primary">ya</span>
                        @else
                          <span class="badge badge-danger">tidak</span>
                        @endif
                      </td>
                      <td class="td-actions">
                        <a id="btn-info" data_id="{{ $item->id_atlet }}" class="btn btn-success btn-link btn-sm">
                          <i class="material-icons">info</i>
                        </a>
                        <div class="ripple-container"></div>
                        <a href="{{ route('admin.atlet.edit', $item->id_atlet) }}" class="btn btn-success btn-link btn-sm" >
                          <i class="material-icons">edit</i>
                        <div class="ripple-container"></div>
                      </a>
                        <a href="{{ route('admin.atlet.delete', $item->id_atlet) }}" onclick="confirmDelete()" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Hapus">
                          <i class="material-icons">close</i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="float-right">
                  {{ $data->links() }}
              </div>
            </div>
          </div>
        </div>

        <div class="table-responsive" id="print" style="display:none">
          <b><h3 class="text-center" >Data Atlet</h3></b>
          <br>
          <table class="table">
            <thead class="">
              <th>Nama</th>
              <th>Club</th>
              <th>Email</th>
              <th>Gender</th>
              <th>Tinggi</th>
              <th>Berat</th>
              <th>Tetala</th>
              <th>Telepon</th>
              <th>Alamat</th>
            </thead>
            <tbody>
              @foreach($data as $item)
                <tr>
                  <td>{{ $item->atlet_nama }}</td>
                  <td>{{ $item->nama_club }}</td>
                  <td>{{ $item->email }}</td>
                  <td>{{ $item->atlet_jenis_kelamin }}</td>
                  <td>{{ $item->atlet_tinggi }}</td>
                  <td>{{ $item->atlet_berat }}</td>
                  <td>{{ $item->atlet_tempat_lahir }}, {{ Helper::tanggal($item->atlet_tanggal_lahir) }}</td>
                  <td>{{ $item->atlet_telepon }}</td>
                  <td>{{ $item->atlet_alamat }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <div class="float-right">
              {{ $data->links() }}
          </div>
        </div>

        <div id="modal">
            
          <!-- Modal -->
          <div class="modal fade" id="modal-atlet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Info Seleksi</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex justify-content-center" >
                        <div class="loader d-flex justify-content-center" id="loader"></div>
                    </div>
                  <div class="table-responsive">
                      <table class="table" id="tabel-atlet">
                        <tbody>
                          <tr>
                            <td class="text-success">Golongan Darah</td>
                            <td id="info-atlet_golongan_darah"></td>
                          </tr>
                          <tr>
                            <td class="text-success">Sekolah</td>
                            <td id="info-atlet_sekolah"></td>
                          </tr>
                          <tr>
                            <td class="text-success">Nama Ortu</td>
                            <td id="info-atlet_nama_ortu"></td>
                          </tr>
                          <tr>
                            <td class="text-success">Foto</td>
                            <td id="info-atlet_foto"><a href="" target="_blank">
                              <img src="" >
                            </td>
                          </tr>
                          <tr>
                            <td class="text-success">Akte</td>
                            <td id="info-atlet_akte"><a href="" target="_blank">
                              <img src="">
                            </td>
                          </tr>
                          <tr>
                            <td class="text-success">Surat Pernyataan</td>
                            <td id="info-atlet_sp"><a href="" target="_blank">
                              <img src="">
                            </td>
                          </tr>
                          <tr>
                            <td class="text-success">KK</td>
                            <td  id="info-atlet_kk"><a href="" target="_blank">
                              <img src="">
                            </td>
                          </tr>
                        </tbody>
                      </table>
                </div>

                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
    <script>
      
        function confirmDelete(){
          if (!confirm("hapus data?")) {
              event.preventDefault()
          }
        }
        $( "#btn-print" ).click(function() {
          printdiv('print');
        });
        $("#tabel-atlet").on("click", "#btn-info", function() {
          $('#loader').removeClass('hide');
          $('#loader').addClass('show');
            var data_id =  $(this).attr('data_id');
            
            $('#modal-atlet').modal('show');
            //alert(data_id);
            getSeleksi(data_id);
        });
        function getSeleksi(id){
            $.ajax({
                type: 'GET',
                url: '{{ asset("api/atlet?id_atlet=") }}' + id,
                success: function(data){
                    $('#loader').removeClass('show');
                    $('#loader').addClass('hide');
                    var data = data.data[0]
                    console.log(data.atlet_golongan_darah);
                    $('#info-atlet_golongan_darah').html(data.atlet_golongan_darah);
                    $('#info-atlet_sekolah').html(data.atlet_sekolah);
                    $('#info-atlet_nama_ortu').html(data.atlet_nama_ortu);

                    $('#info-atlet_foto img').attr( "src" , "{{ asset('/public/images/atlet/foto').'/' }}" + data.atlet_foto );
                    $('#info-atlet_foto a').attr( "href" , "{{ asset('/public/images/atlet/foto').'/' }}" + data.atlet_foto );

                    $('#info-atlet_akte img').attr( "src" , "{{ asset('/public/images/atlet/akte').'/' }}" + data.atlet_akte );
                    $('#info-atlet_akte a').attr( "href" , "{{ asset('/public/images/atlet/akte').'/' }}" + data.atlet_akte );

                    $('#info-atlet_kk img').attr( "src" , "{{ asset('/public/images/atlet/kk').'/' }}" + data.atlet_kk );
                    $('#info-atlet_kk a').attr( "href" , "{{ asset('/public/images/atlet/kk').'/' }}" + data.atlet_kk );

                    $('#info-atlet_sp img').attr( "src" , "{{ asset('/public/images/atlet/sp').'/' }}" + data.atlet_sp );
                    $('#info-atlet_sp a').attr( "href" , "{{ asset('/public/images/atlet/sp').'/' }}" + data.atlet_sp );
                    $('#loader').hide();
                }
            });
        }
        function printdiv(printpage)
        {
          var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr+newstr+footstr;
            console.log(newstr);
            
            location.reload();
            window.print();
            document.body.innerHTML = oldstr;
        }
    </script>
@endsection