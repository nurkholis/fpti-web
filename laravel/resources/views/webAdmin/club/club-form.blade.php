@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('admin.club.index') }}">Club</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Club</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.club.put', $edit->id_club) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.club.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">nama club</label>
    <input type="text" class="form-control" name="nama_club"
     @if( isset($edit) ) value="{{ $edit->nama_club }}" @else value="{{ old('nama_club') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">nomor club</label>
    <input type="number" class="form-control" name="nomor_club"
     @if( isset($edit) ) value="{{ $edit->nomor_club }}" @else value="{{ old('nomor_club') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">tanggal keanggotaan</label>
    <input type="date" class="form-control" name="tanggal_keanggotaan" 
    @if( isset($edit) ) value="{{ $edit->tanggal_keanggotaan }}" @else value="{{ old('tanggal_keanggotaan') }}" @endif required>
</div>


<div class="form-group">
    <label class="bmd-label-floating text-capitalize">alamat club</label>
    <input type="text" class="form-control" name="alamat_club"
     @if( isset($edit) ) value="{{ $edit->alamat_club }}" @else value="{{ old('alamat_club') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">telepon club</label>
    <input type="number" class="form-control" name="telepon_club"
     @if( isset($edit) ) value="{{ $edit->telepon_club }}" @else value="{{ old('telepon_club') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">tahun berdiri</label>
    <input type="number" class="form-control" name="tahun_berdiri"
     @if( isset($edit) ) value="{{ $edit->tahun_berdiri }}" @else value="{{ old('tahun_berdiri') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">ketua club</label>
    <input type="text" class="form-control" name="ketua_club"
     @if( isset($edit) ) value="{{ $edit->ketua_club }}" @else value="{{ old('ketua_club') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">pembina club</label>
    <input type="text" class="form-control" name="pembina_club"
     @if( isset($edit) ) value="{{ $edit->pembina_club }}" @else value="{{ old('pembina_club') }}" @endif required>
</div>

<div class="input-file-box row">
   <div class="col-md-4 mt-40">
       <button type="submit" class="btn btn-success">Foto</button>
       <input class="form-control input-file" type="file" id="input-foto" name="logo_club" accept=".jpg,.png">
   </div>
   <div class="col-md-8">
       <div class="row">
           <div class="col-md-6">
                   <img src="" id="prev-foto" class="img-preview"> 
               @if(isset($edit))
                   <span>baru</span>
               @endif    
           </div>
           <div class="col-md-6">
               @if(isset($edit))
                   <img src="{{ asset('/public/images/club').'/'.$edit->logo_club }}" alt="" class="img-preview">
                   <span>lama</span>
               @endif    
           </div>
       </div>
   </div>
   <hr>
</div>
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
$(document).ready(function () {
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-foto").change(function() {
        readURL(this);
    });
}); 

</script>
        
@endsection