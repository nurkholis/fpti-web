@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('admin.dokumen.index') }}">Dokumen</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Dokumen</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.dokumen.put', $edit->id_dokumen) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.dokumen.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">nama dokumen</label>
    <input type="text" class="form-control" name="nama_dokumen" @if( isset($edit) ) value="{{ $edit->nama_dokumen }}" 
    @else value="{{ old('nama_dokumen') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">keterangan dokumen</label>
    <input type="text" class="form-control" name="keterangan_dokumen" @if( isset($edit) ) value="{{ $edit->keterangan_dokumen }}" 
    @else value="{{ old('keterangan_dokumen') }}" @endif required>
</div>

<div class="input-file-box row">
   <div class="col-md-4 mt-40">
       <button type="submit" class="btn btn-success">Dokumen</button>
       <input class="form-control input-file" type="file" id="input-file" name="dokumen" accept=".pdf,.doc">
   </div>
   <div class="col-md-8">
       <div class="row">
           <div class="col-md-6">
                   <h3 id="nama_file"></h3>
               @if(isset($edit))
                   <span>baru</span>
               @endif    
           </div>
           <div class="col-md-6">
               @if(isset($edit))
                   <h3>{{ $edit->dokumen }}</h3>
                   <span>lama</span>
               @endif    
           </div>
       </div>
   </div>
   <hr>
</div>

                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            $('#nama_file').html(input.files[0]['name']);
        }
    }

    $("#input-file").change(function() {
        readURL(this);
    });
</script>
@endsection