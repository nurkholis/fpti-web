@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('admin.foto.index') }}">Foto</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Foto</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.foto.put', $edit->id_foto) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.foto.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">judul foto</label>
    <input type="text" class="form-control" name="judul_foto"
     @if( isset($edit) ) value="{{ $edit->judul_foto }}" @else value="{{ old('judul_foto') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">tanggal foto</label>
    <input type="date" class="form-control" name="tanggal_foto" 
    @if( isset($edit) ) value="{{ $edit->tanggal_foto }}" @else value="{{ old('tanggal_foto') }}" @endif required>
</div>

<div class="input-file-box row">
   <div class="col-md-4 mt-40">
       <button type="submit" class="btn btn-success">Foto</button>
       <input class="form-control input-file" type="file" id="input-foto" name="foto" accept=".jpg,.png">
   </div>
   <div class="col-md-8">
       <div class="row">
           <div class="col-md-6">
                   <img src="" id="prev-foto" class="img-preview"> 
               @if(isset($edit))
                   <span>baru</span>
               @endif    
           </div>
           <div class="col-md-6">
               @if(isset($edit))
                   <img src="{{ asset('/public/images/foto').'/'.$edit->foto }}" alt="" class="img-preview">
                   <span>lama</span>
               @endif    
           </div>
       </div>
   </div>
   <hr>
</div>

<div class="form-group">
    <div class="form-group bmd-form-group text-capitalize">
        <label class="bmd-label-floating">keterangan foto</label>
        <textarea class="form-control" name="keterangan_foto" rows="5" required>@if( isset($edit) ) {{ $edit->keterangan_foto }} @else {{ old('keterangan_foto') }} @endif</textarea>
    </div>
</div>
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
    <script src="{{ asset('public/assets/js/plugins/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/plugins/select2.min.js') }}"></script>
<script>
$(document).ready(function () {
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-foto").change(function() {
        readURL(this);
    });
}); 

</script>
        
@endsection