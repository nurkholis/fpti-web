@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
@endsection

@section('content')


<div class="content">
  <a href="{{ route('admin.pelatih.create') }}" class="btn btn-success btn-round btn-just-icon btn-floating ">
    <i class="material-icons">add</i>
    <div class="ripple-container"></div>
  </a>
  <a href="{{ route('admin.pelatih.export') }}" class="btn btn-success btn-round btn-just-icon btn-floating b-100">
    <i class="material-icons">print</i>
    <div class="ripple-container"></div>
  </a>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
          <h4 class="card-title">Data Pelatih</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                @if( Request::get('search') )
                <div class="alert alert-info alert-with-icon" data-notify="container">
                  <i class="material-icons" data-notify="icon">info</i>
                  <a href="{{ route('admin.pelatih.index') }}" class="close" aria-label="Close">
                    <i class="material-icons">close</i>
                  </a>
                  <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search') }}"</strong></span>
                </div>
                @endif
              </div>
              <div class="col-md-6 float-right">
              <form class="navbar-form">
                <span class="bmd-form-group"><div class="input-group no-border">
                  <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Cari..." autofocus>
                  <button type="submit" class="btn btn-success btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                    <div class="ripple-container"></div>
                  </button>
                </div>
                </span>
              </form>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-success">
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Jabatan</th>
                  <th>Alamat</th>
                  <th>Sertifikasi</th>
                  <th>Foto</th>
                  <th>Sertifikasi</th>
                  <th>Aksi</th>
                </thead>
                <tbody>
                  @foreach($data as $item)
                    <tr>
                      <td>{{ $item->nama_pelatih }}</td>
                      <td>{{ $item->email }}</td>
                      <td>{{ $item->jabatan_pelatih }}</td>
                      <td>{{ $item->alamat_pelatih }}</td>
                      <td>{{ $item->sertifikasi_pelatih }}</td>
                      
                      <td>
                          <a href="{{ asset('/public/images/pelatih').'/'.$item->foto_pelatih }}" target="_blank">
                              <img src="{{ asset('/public/images/pelatih').'/'.$item->foto_pelatih }}">
                          </a>
                      </td>
                        
                      <td>
                          <a href="{{ asset('/public/images/sertifikat').'/'.$item->sertifikat_pelatih }}" target="_blank">
                              <img src="{{ asset('/public/images/sertifikat').'/'.$item->sertifikat_pelatih }}">
                          </a>
                        </td>
                      <td class="td-actions">
                        <a href="{{ route('admin.pelatih.edit', $item->id_pelatih) }}" rel="tooltip" title="" class="btn btn-success btn-link btn-sm" data-original-title="Sunting">
                          <i class="material-icons">edit</i>
                        </a>
                        <a href="{{ route('admin.pelatih.reset', $item->id_pelatih) }}" rel="tooltip" title="" class="btn btn-success btn-link btn-sm" data-original-title="Reset Password">
                          <i class="material-icons">lock_open</i>
                        </a>
                        <a href="{{ route('admin.pelatih.delete', $item->id_pelatih) }}" onclick="confirmDelete()" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Hapus">
                          <i class="material-icons">close</i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="float-right">
                  {{ $data->links() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
    <script>
        function confirmDelete(){
            if (!confirm("hapus data?")) {
                event.preventDefault()
            }
        }
    </script>
@endsection