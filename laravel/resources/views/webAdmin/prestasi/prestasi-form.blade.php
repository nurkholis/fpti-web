@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('admin.prestasi.index') }}">Prestasi</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Prestasi</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.prestasi.put', $edit->id_prestasi) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.prestasi.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">Atlet</label>
    <select class="form-control" name="id_atlet">
        @if (isset($edit))
            @foreach ($atlet as $item)
                <option value="{{ $item->id_atlet }}" {{ $item->id_atlet == $edit->id_atlet ? 'selected' : '' }} >{{ $item->atlet_nama }}</option>
            @endforeach
        @else
            @foreach ($atlet as $item)
                <option value="{{ $item->id_atlet }}"> {{ $item->atlet_nama }}</option>
            @endforeach
        @endif
    </select>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">Kategori</label>
    <input type="text" class="form-control" name="kategori"
     @if( isset($edit) ) value="{{ $edit->kategori }}" @else value="{{ old('kategori') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">Peringkat</label>
    <input type="number" class="form-control" name="peringkat"
     @if( isset($edit) ) value="{{ $edit->peringkat }}" @else value="{{ old('peringkat') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">tingkat</label>
    <input type="text" class="form-control" name="tingkat"
     @if( isset($edit) ) value="{{ $edit->tingkat }}" @else value="{{ old('tingkat') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">tahun</label>
    <input type="number" class="form-control" name="tahun"
     @if( isset($edit) ) value="{{ $edit->tahun }}" @else value="{{ old('tahun') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">tempat</label>
    <input type="text" class="form-control" name="tempat"
     @if( isset($edit) ) value="{{ $edit->tempat }}" @else value="{{ old('tempat') }}" @endif required>
</div>

                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
@endsection