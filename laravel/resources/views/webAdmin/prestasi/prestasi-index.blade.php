@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
@endsection

@section('content')


<div class="content">
  <a href="{{ route('admin.prestasi.create') }}" class="btn btn-success btn-round btn-just-icon btn-floating">
    <i class="material-icons">add</i>
    <div class="ripple-container"></div>
  </a>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
          <h4 class="card-title">Data Prestasi</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                @if( Request::get('search') )
                <div class="alert alert-info alert-with-icon" data-notify="container">
                  <i class="material-icons" data-notify="icon">info</i>
                  <a href="{{ route('admin.prestasi.index') }}" class="close" aria-label="Close">
                    <i class="material-icons">close</i>
                  </a>
                  <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search') }}"</strong></span>
                </div>
                @endif
              </div>
              <div class="col-md-6 float-right">
              <form class="navbar-form">
                <span class="bmd-form-group"><div class="input-group no-border">
                  <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Cari..." autofocus>
                  <button type="submit" class="btn btn-success btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                    <div class="ripple-container"></div>
                  </button>
                </div>
                </span>
              </form>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-success">
                  <th>No</th>
                  <th>Nama Atlet</th>
                  <th>Club</th>
                  <th>Kategori</th>
                  <th>Peringkat</th>
                  <th>Tingkat</th>
                  <th>Tahun</th>
                  <th>Tempat</th>
                  <th>Aksi</th>
                </thead>
                <tbody>
                  @foreach($data as $item)
                    <tr>
                      <td>{{ $loop->iteration  }}</td>
                      <td>{{ $item->atlet_nama }}</td>
                      <td>{{ $item->nama_club }}</td>
                      <td>{{ $item->kategori }}</td>
                      <td>{{ $item->peringkat }}</td>
                      <td>{{ $item->tingkat }}</td>
                      <td>{{ $item->tahun }}</td>
                      <td>{{ $item->tempat }}</td>
                      <td class="td-actions">
                        <a href="{{ route('admin.prestasi.edit', $item->id_prestasi) }}" rel="tooltip" title="" class="btn btn-success btn-link btn-sm" data-original-title="Sunting">
                          <i class="material-icons">edit</i>
                        <div class="ripple-container"></div>
                      </a>
                        <a href="{{ route('admin.prestasi.delete', $item->id_prestasi) }}" onclick="confirmDelete()" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Hapus">
                          <i class="material-icons">close</i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="float-right">
                  {{ $data->links() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
    <script>
        function confirmDelete(){
            if (!confirm("hapus data?")) {
                event.preventDefault()
            }
        }
    </script>
@endsection