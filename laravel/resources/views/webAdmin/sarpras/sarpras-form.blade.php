@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('admin.sarpras.index') }}">Sarpras</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Sarpras</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.sarpras.put', $edit->id_sarpras) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.sarpras.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">items</label>
    <input type="text" class="form-control" name="items"
     @if( isset($edit) ) value="{{ $edit->items }}" @else value="{{ old('items') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">volume</label>
    <input type="number" class="form-control" name="volume"
     @if( isset($edit) ) value="{{ $edit->volume }}" @else value="{{ old('volume') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">satuan</label>
    <input type="text" class="form-control" name="satuan"
     @if( isset($edit) ) value="{{ $edit->satuan }}" @else value="{{ old('satuan') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">tahun perolehan</label>
    <input type="number" class="form-control" name="tahun_perolehan"
     @if( isset($edit) ) value="{{ $edit->tahun_perolehan }}" @else value="{{ old('tahun_perolehan') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">keterangan</label>
    <input type="text" class="form-control" name="keterangan_sarpras"
     @if( isset($edit) ) value="{{ $edit->keterangan_sarpras }}" @else value="{{ old('keterangan_sarpras') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">kode barang</label>
    <input type="text" class="form-control" name="kode_barang"
     @if( isset($edit) ) value="{{ $edit->kode_barang }}" @else value="{{ old('kode_barang') }}" @endif required>
</div>
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-sertifikat').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-foto").change(function() {
        readURL(this);
    });

    $("#input-sertifikat").change(function() {
        readURL1(this);
    });
</script>
@endsection