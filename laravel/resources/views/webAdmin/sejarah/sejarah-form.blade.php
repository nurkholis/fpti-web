@extends('layouts.admin')

@section('css')
  <link href="{{ asset('public/assets/font-awesome/font-awesome.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('admin.sejarah.index') }}">Sejarah</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Sejarah</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.sejarah.put', $edit->id_sejarah) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.sejarah.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">judul sejarah</label>
    <input type="text" class="form-control" name="judul_sejarah"
     @if( isset($edit) ) value="{{ $edit->judul_sejarah }}" @else value="{{ old('judul_sejarah') }}" @endif required>
</div>

<div class="form-group">
    <div class="form-group bmd-form-group text-capitalize">
        <label class="bmd-label-floating">isi sejarah</label>
        <textarea class="form-control" name="isi_sejarah" id="isi-sejarah" rows="10" required>@if( isset($edit) ) {{ $edit->isi_sejarah }} @else {{ old('isi_sejarah') }} @endif</textarea>
    </div>
</div>
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
    <script src="{{ asset('public/assets/vendor/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'isi-sejarah' );
    </script>
@endsection