@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('admin.pelatih.index') }}">Pelatih</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Pelatih</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.pelatih.put', $edit->id_pelatih) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.pelatih.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">nama pelatih</label>
    <input type="text" class="form-control" name="nama_pelatih" @if( isset($edit) ) value="{{ $edit->nama_pelatih }}" @else value="{{ old('nama_pelatih') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">email</label>
    <input type="email" class="form-control" name="email" @if( isset($edit) ) value="{{ $edit->email }}" @else value="{{ old('email') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">alamat pelatih</label>
    <input type="text" class="form-control" name="alamat_pelatih" @if( isset($edit) ) value="{{ $edit->alamat_pelatih }}" @else value="{{ old('alamat_pelatih') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">jabatan pelatih</label>
    <input type="text" class="form-control" name="jabatan_pelatih" @if( isset($edit) ) value="{{ $edit->jabatan_pelatih }}" @else value="{{ old('jabatan_pelatih') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">sertifikasi pelatih</label>
    <input type="text" class="form-control" name="sertifikasi_pelatih" @if( isset($edit) ) value="{{ $edit->sertifikasi_pelatih }}" @else value="{{ old('sertifikasi_pelatih') }}" @endif required>
</div>

<div class="input-file-box row">
   <div class="col-md-4 mt-40">
       <button type="submit" class="btn btn-success">Foto</button>
       <input class="form-control input-file" type="file" id="input-foto" name="foto_pelatih" accept=".jpg,.png">
   </div>
   <div class="col-md-8">
       <div class="row">
           <div class="col-md-6">
                   <img src="" id="prev-foto" class="img-preview"> 
               @if(isset($edit))
                   <span>baru</span>
               @endif    
           </div>
           <div class="col-md-6">
               @if(isset($edit))
                   <img src="{{ asset('/public/images/pelatih').'/'.$edit->foto_pelatih }}" alt="" class="img-preview">
                   <span>lama</span>
               @endif    
           </div>
       </div>
   </div>
   <hr>
</div>

<div class="input-file-box row">
   <div class="col-md-4 mt-40">
       <button type="submit" class="btn btn-success">Sertifikat</button>
       <input class="form-control input-file" type="file" id="input-sertifikat" name="sertifikat_pelatih" accept=".jpg,.png">
   </div>
   <div class="col-md-8">
       <div class="row">
           <div class="col-md-6">
                   <img src="" id="prev-sertifikat" class="img-preview"> 
               @if(isset($edit))
                   <span>baru</span>
               @endif    
           </div>
           <div class="col-md-6">
               @if(isset($edit))
                   <img src="{{ asset('/public/images/sertifikat').'/'.$edit->sertifikat_pelatih }}" alt="" class="img-preview">
                   <span>lama</span>
               @endif    
           </div>
       </div>
   </div>
   <hr>
</div>

                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-sertifikat').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-foto").change(function() {
        readURL(this);
    });

    $("#input-sertifikat").change(function() {
        readURL1(this);
    });
</script>
@endsection