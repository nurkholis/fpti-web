@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
@endsection

@section('css')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection

@php
use Config\Kholis as Helper;
@endphp

@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        {{-- tahun seleksi --}}
        <div class="card">
          <div class="card-header card-header-success">
          <h4 class="card-title">Data Tahun Seleksi</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                @if( Request::get('search') )
                <div class="alert alert-info alert-with-icon" data-notify="container">
                  <i class="material-icons" data-notify="icon">info</i>
                  <a href="{{ route('admin.tahunSeleksi.index') }}" class="close" aria-label="Close">
                    <i class="material-icons">close</i>
                  </a>
                  <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search') }}"</strong></span>
                </div>
                @endif
              </div>
              <div class="col-md-6 float-right">
              <form class="navbar-form">
                <span class="bmd-form-group"><div class="input-group no-border">
                  <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Cari..." autofocus>
                  <button type="submit" class="btn btn-success btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                  </button>
                  <a href="{{ route('admin.tahunSeleksi.create') }}" class="btn btn-success btn-round btn-just-icon">
                      <i class="material-icons">add</i>
                    </a>
                </div>
                </span>
              </form>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-success">
                  <th>Tahun Seleksi</th>
                  <th>Tanggal Buka</th>
                  <th>Tanggal Tutup</th>
                  <th>Aksi</th>
                </thead>
                <tbody>
                  @foreach($tahunSeleksi as $item)
                    <tr>
                      <td>{{ $item->tahun_seleksi }}</td>
                      <td>{{ Helper::tanggal( $item->tanggal_buka )  }}</td>
                      <td>{{ Helper::tanggal( $item->tanggal_tutup )  }}</td>
                      <td class="td-actions">
                        <a href="{{ route('admin.tahunSeleksi.edit', $item->id_tahun_seleksi) }}" rel="tooltip" title="" class="btn btn-success btn-link btn-sm" data-original-title="Sunting">
                          <i class="material-icons">edit</i>
                        <div class="ripple-container"></div>
                      </a>
                        <a href="{{ route('admin.tahunSeleksi.delete', $item->id_tahun_seleksi) }}" onclick="confirmDelete()" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Hapus">
                          <i class="material-icons">close</i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="float-right">
                  {{ $tahunSeleksi->links() }}
              </div>
            </div>
          </div>
        </div>
        {{-- data seleksi --}}
        <div class="card">
          <div class="card-header card-header-success">
          <h4 class="card-title">Data Seleksi</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
              </div>
              <div class="col-md-6 float-right">
              <form class="navbar-form">
                <span class="bmd-form-group">
                <div class="input-group">
                      <select class="form-control" name="tahun_seleksi">
                          @if( Request::get('tahun_seleksi') ) 
                              @foreach ($data_tahun_seleksi as $item)
                                  <option value="{{ $item->tahun_seleksi }}" {{ $item->tahun_seleksi == Request::get('tahun_seleksi') ? 'selected' : '' }} >{{ $item->tahun_seleksi }}</option>
                              @endforeach
                          @else
                              @foreach ($data_tahun_seleksi as $item)
                                  <option value="{{ $item->tahun_seleksi }}" {{ $item->tahun_seleksi == $tahun_seleksi->tahun_seleksi ? 'selected' : '' }}> {{ $item->tahun_seleksi }}</option>
                              @endforeach
                          @endif
                      </select>
                  <button type="submit" class="btn btn-success btn-round">
                    Buka
                    <div class="ripple-container"></div>
                  </button>
                </div>
                </span>
              </form>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table" id="">
                <thead class=" text-success">
                  <th>Tahun Seleksi</th>
                  <th>Tanggal Buka</th>
                  <th>Tanggal Tutup</th>
                  <th>Jumlah Peserta</th>
                  <th>Tidak Lolos</th>
                  <th>Lolos</th>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $tahun_seleksi->tahun_seleksi }}</td>
                    <td>{{ Helper::tanggal( $tahun_seleksi->tanggal_buka )  }}</td>
                    <td>{{ Helper::tanggal( $tahun_seleksi->tanggal_tutup )  }}</td>
                    <td>{{ $tahun_seleksi->jumlah_peseta }}</td>
                    <td>{{ $tahun_seleksi->jumlah_peseta - $tahun_seleksi->lulus }}</td>
                    <td>{{ $tahun_seleksi->lulus }}</td>
                  </tr>
                </tbody>
              </table>
            </div>

          </div>
        </div>
        {{-- hasil seleksi --}}
        <div class="card">
          <div class="card-header card-header-primary">
          <h4 class="card-title">Hasil Seleksi Atlet</h4>
          </div>
          <div class="card-body">

            <div class="row">

              <div class="col-md-12">
                @if( Request::get('search_seleksi') )
                <div class="alert alert-info alert-with-icon" data-notify="container">
                  <i class="material-icons" data-notify="icon">info</i>
                  <a href="{{ route('admin.seleksi.index') }}" class="close" aria-label="Close">
                    <i class="material-icons">close</i>
                  </a>
                  <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search_seleksi') }}"</strong></span>
                </div>
                @endif
              </div>

              <div class="col-md-8">
              </div>
              
              <div class="col-md-4">
                <form class="navbar-form">
                  <span class="bmd-form-group"><div class="input-group no-border">
                    <input type="text" name="search_seleksi" value="{{ Request::get('search_seleksi') }}" class="form-control" placeholder="Cari..." autofocus>
                    <button type="submit" class="btn btn-primary btn-round btn-just-icon">
                      <i class="material-icons">search</i>
                      <div class="ripple-container"></div>
                    </button>
                  </div>
                  </span>
                </form>
              </div>

            </div>
            
            {{-- data seleksi --}}
            <div class="row">
              <div class="table-responsive">
                <table class="table table-bordered" id="tabel-seleksi" >
                  <thead class=" text-primary">
                    <tr>
                      <th>Nomor</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Gender</th>
                      <th>Kelas</th>
                      <th>Tetala</th>
                      <th>Telepon </th>
                      <th>Alamat</th>
                      <th>Aksi </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($seleksi as $item)
                      <tr>
                        <td>{{ $loop->iteration  }}</td>
                        <td>{{ $item->atlet_nama }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->atlet_jenis_kelamin }}</td>
                        <td>{{ Helper::kelasUmur( $item->atlet_tanggal_lahir ) }}</td>
                        <td>{{ Helper::tanggal( $item->atlet_tanggal_lahir ) }}, {{ $item->atlet_tempat_lahir }}</td>
                        <td>{{ $item->atlet_telepon }}</td>
                        <td>{{ $item->atlet_alamat }}</td>
                        <td class="td-actions" >  
                            <a id="btn-info" data_id="{{ $item->id_atlet }}" class="btn btn-success btn-link btn-sm">
                              <i class="material-icons">info</i>
                            </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="float-right">
                    {{ $nilai->links() }}
                </div>
              </div>
            </div>

            <div class="row">

                <div class="col-md-12">
                  @if( Request::get('search_nilai') )
                  <div class="alert alert-info alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">info</i>
                    <a href="{{ route('admin.seleksi.index') }}" class="close" aria-label="Close">
                      <i class="material-icons">close</i>
                    </a>
                    <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search_nilai') }}"</strong></span>
                  </div>
                  @endif
                </div>
  
                <div class="col-md-8">
                  Hasil Seleksi Atlet
                </div>
                
                <div class="col-md-4">
                  <form class="navbar-form">
                    <span class="bmd-form-group"><div class="input-group no-border">
                      <input type="text" name="search_nilai" value="{{ Request::get('search_nilai') }}" class="form-control" placeholder="Cari..." autofocus>
                      <button type="submit" class="btn btn-primary btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                      </button>
                    </div>
                    </span>
                  </form>
                </div>
  
              </div>

            <!-- data nilai -->
            <div class="row">
              <div class="table-responsive">
                <table class="table table-bordered" id="tabel-nilai" >
                  <thead class=" text-primary">
                    <tr>
                      <th>Nomor</th>
                      <th>Nama</th>
                      <th>Gender</th>
                      <th>Kelas</th>
                      <th>Club</th>
                      <th>P.Lead </th>
                      <th>P.Speed </th>
                      <th>T.Fisik </th>
                      <th>Verifikasi </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($nilai as $item)
                      <tr>
                        <td>{{ $loop->iteration  }}</td>
                        <td>{{ $item->atlet_nama }}</td>
                        <td>{{ $item->atlet_jenis_kelamin }}</td>
                        <td>{{ Helper::kelasUmur( $item->atlet_tanggal_lahir ) }}</td>
                        <td>{{ $item->nama_club }}</td>
                        <td>{{ $item->panjat_lead }}</td>
                        <td>{{ $item->panjat_speed }}</td>
                        <td>{{ ( $item->lari + $item->push_up + $item->sit_up + $item->pull_up) / 4 }}</td>
                        <td>
                          @if ( !$item->status )
                            <a href="{{ route('admin.seleksi.verifikasi' , [$item->id_atlet, '1']) }}" rel="tooltip"class="btn btn-primary btn-link btn-sm">
                              verifikasi
                            </a>
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="float-right">
                    {{ $nilai->links() }}
                </div>
              </div>
            </div>

          </div>
        </div>

        <div id="modal">
            
            <!-- Modal -->
            <div class="modal fade" id="modal-atlet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Info Seleksi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                      <div class="d-flex justify-content-center" >
                          <div class="loader d-flex justify-content-center" id="loader"></div>
                      </div>
                    <div class="table-responsive">
                        <table class="table" id="tabel-atlet">
                          <tbody>
                            <tr>
                              <td class="text-success">Golongan Darah</td>
                              <td id="info-atlet_golongan_darah"></td>
                            </tr>
                            <tr>
                              <td class="text-success">Sekolah</td>
                              <td id="info-atlet_sekolah"></td>
                            </tr>
                            <tr>
                              <td class="text-success">Nama Ortu</td>
                              <td id="info-atlet_nama_ortu"></td>
                            </tr>
                            <tr>
                              <td class="text-success">Foto</td>
                              <td id="info-atlet_foto"><a href="" target="_blank">
                                <img src="" >
                              </td>
                            </tr>
                            <tr>
                              <td class="text-success">Akte</td>
                              <td id="info-atlet_akte"><a href="" target="_blank">
                                <img src="">
                              </td>
                            </tr>
                            <tr>
                              <td class="text-success">Surat Pernyataan</td>
                              <td id="info-atlet_sp"><a href="" target="_blank">
                                <img src="">
                              </td>
                            </tr>
                            <tr>
                              <td class="text-success">KK</td>
                              <td  id="info-atlet_kk"><a href="" target="_blank">
                                <img src="">
                              </td>
                            </tr>
                          </tbody>
                        </table>
                  </div>
  
                  </div>
                </div>
              </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script>
      function confirmDelete(){
        if (!confirm("hapus data?")) {
            event.preventDefault()
        }
      }
      $("#tabel-seleksi").on("click", "#btn-info", function() {
          var data_id =  $(this).attr('data_id');
            
            $('#modal-atlet').modal('show');
            //alert(data_id);
            getSeleksi(data_id);
      });
      function getSeleksi(id){
        $.ajax({
            type: 'GET',
            url: '{{ asset("api/atlet?id_atlet=") }}' + id,
            success: function(data){
                $('#loader').removeClass('show');
                $('#loader').addClass('hide');
                var data = data.data[0]
                console.log(data.atlet_golongan_darah);
                $('#info-atlet_golongan_darah').html(data.atlet_golongan_darah);
                $('#info-atlet_sekolah').html(data.atlet_sekolah);
                $('#info-atlet_nama_ortu').html(data.atlet_nama_ortu);

                $('#info-atlet_foto img').attr( "src" , "{{ asset('/public/images/atlet/foto').'/' }}" + data.atlet_foto );
                $('#info-atlet_foto a').attr( "href" , "{{ asset('/public/images/atlet/foto').'/' }}" + data.atlet_foto );

                $('#info-atlet_akte img').attr( "src" , "{{ asset('/public/images/atlet/akte').'/' }}" + data.atlet_akte );
                $('#info-atlet_akte a').attr( "href" , "{{ asset('/public/images/atlet/akte').'/' }}" + data.atlet_akte );

                $('#info-atlet_kk img').attr( "src" , "{{ asset('/public/images/atlet/kk').'/' }}" + data.atlet_kk );
                $('#info-atlet_kk a').attr( "href" , "{{ asset('/public/images/atlet/kk').'/' }}" + data.atlet_kk );

                $('#info-atlet_sp img').attr( "src" , "{{ asset('/public/images/atlet/sp').'/' }}" + data.atlet_sp );
                $('#info-atlet_sp a').attr( "href" , "{{ asset('/public/images/atlet/sp').'/' }}" + data.atlet_sp );
                $('#loader').hide();
            }
        });
      }
      
      $('#tabel-nilai').DataTable({searching: false, paging: false, info: false});
  </script>
@endsection