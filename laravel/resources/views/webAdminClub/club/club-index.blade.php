@extends('layouts.adminClub')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
@endsection

@php

use Config\Kholis as Helper; 

@endphp

@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
          <h4 class="card-title">Data Club</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <tbody>
                    <tr>
                      <td class="text-success" >Nama Club</td>
                      <td>{{ $club->nama_club }}</td>
                    </tr>
                    <tr>
                      <td class="text-success" >Logo</td>
                      <td>
                          <a href="{{ asset('/public/images/club').'/'.$club->logo_club }}" target="_blank">
                              <img src="{{ asset('/public/images/club').'/'.$club->logo_club }}">
                          </a>
                      </td>
                    </tr>
                  <tr>
                    <td class="text-success" >Nomor Anggota FPT</td>
                    <td>{{ $club->nomor_club }}</td>
                  </tr>
                  <tr>
                    <td class="text-success" >Tanggal Keanggotaan</td>
                    <td>{{ Helper::tanggal( $club->tanggal_keanggotaan ) }}</td>
                  </tr>
                  <tr>
                    <td class="text-success" >Alamat</td>
                    <td>{{ $club->alamat_club }}</td>
                  </tr>
                  <tr>
                    <td class="text-success" >Nomor Telepon</td>
                    <td>{{ $club->telepon_club }}</td>
                  </tr>
                  <tr>
                    <td class="text-success" >Tahun Berdiri</td>
                    <td>{{ $club->tahun_berdiri }}</td>
                  </tr>
                  <tr>
                    <td class="text-success" >Ketua</td>
                    <td>{{ $club->ketua_club }}</td>
                  {{-- <tr>
                    <td class="text-success" >Pelatih</td>
                    <td></td>
                  </tr> --}}
                  <tr>
                    <td class="text-success" >Pembina</td>
                    <td>{{ $club->pembina_club }}</td>
                  </tr>
                </tbody>
              </table>
              <div class="float-right">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
    <script>
        function confirmDelete(){
            if (!confirm("hapus data?")) {
                event.preventDefault()
            }
        }
    </script>
@endsection