@extends('layouts.adminClub')
@section('content')
    <div class="contet">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 mt-40">
                    <div class="card">
                        <div class="card-body">
                            <h3>Selamat datang {{ Auth::guard('adminClub')->user()->nama_admin_club }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection