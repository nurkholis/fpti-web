@extends('layouts.adminClub')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('adminClub.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('adminClub.seleksi.index') }}">Seleksi</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Seleksi</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('adminClub.seleksi.put', $edit->id_seleksi) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('adminClub.seleksi.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="bmd-label-floating text-capitalize">nama</label>
            <input type="text" class="form-control" name="seleksi_nama" 
            @if( isset($edit) ) value="{{ $edit->seleksi_nama }}" @else value="{{ old('seleksi_nama') }}" @endif required>
        </div>

        <div class="form-group">
            <label class="bmd-label-floating text-capitalize">email</label>
            <input type="email" class="form-control" name="seleksi_email" 
            @if( isset($edit) ) value="{{ $edit->seleksi_email }}" @else value="{{ old('seleksi_email') }}" @endif required>
        </div>

        <div class="form-group">
            <label class="bmd-label-floating text-capitalize">tempat lahir</label>
            <input type="text" class="form-control" name="seleksi_tempat_lahir" 
            @if( isset($edit) ) value="{{ $edit->seleksi_tempat_lahir }}" @else value="{{ old('seleksi_tempat_lahir') }}" @endif required>
        </div>

        <div class="form-group">
            <label class="bmd-label-floating text-capitalize">tanggal lahir</label>
            <input type="date" class="form-control" name="seleksi_tanggal_lahir" 
            @if( isset($edit) ) value="{{ $edit->seleksi_tanggal_lahir }}" @else value="{{ old('seleksi_tanggal_lahir') }}" @endif required>
        </div>

        <div class="form-group">
            <label class="bmd-label-floating text-capitalize">jenis kelamin</label>
            <select class="form-control" name="seleksi_jenis_kelamin">
                <option value="L" @if( isset($edit) ) {{ $edit->seleksi_jenis_kelamin == 'L' ? 'selected' : '' }} @endif >Laki-laki</option>
                <option value="P" @if( isset($edit) ) {{ $edit->seleksi_jenis_kelamin == 'P' ? 'selected' : '' }} @endif >Perempuan</option>
            </select>
        </div>

        <div class="form-group">
            <label class="bmd-label-floating text-capitalize">golongan darah</label>
            <select class="form-control" name="seleksi_golongan_darah">
                <option value="L" @if( isset($edit) ) {{ $edit->seleksi_golongan_darah == 'A' ? 'selected' : '' }} @endif >A</option>
                <option value="B" @if( isset($edit) ) {{ $edit->seleksi_golongan_darah == 'B' ? 'selected' : '' }} @endif >B</option>
                <option value="AB" @if( isset($edit) ) {{ $edit->seleksi_golongan_darah == 'AB' ? 'selected' : '' }} @endif >AB</option>
                <option value="O" @if( isset($edit) ) {{ $edit->seleksi_golongan_darah == 'O' ? 'selected' : '' }} @endif >O</option>
            </select>
        </div>

        <div class="form-group">
            <label class="bmd-label-floating text-capitalize">tinggi</label>
            <input type="number" class="form-control" name="seleksi_tinggi" 
            @if( isset($edit) ) value="{{ $edit->seleksi_tinggi }}" @else value="{{ old('seleksi_tinggi') }}" @endif required>
        </div>

        <div class="form-group">
            <label class="bmd-label-floating text-capitalize">berat</label>
            <input type="number" class="form-control" name="seleksi_berat" 
            @if( isset($edit) ) value="{{ $edit->seleksi_berat }}" @else value="{{ old('seleksi_berat') }}" @endif required>
        </div>

        <div class="form-group">
            <label class="bmd-label-floating text-capitalize">alamat</label>
            <input type="text" class="form-control" name="seleksi_alamat" 
            @if( isset($edit) ) value="{{ $edit->seleksi_alamat }}" @else value="{{ old('seleksi_alamat') }}" @endif required>
        </div>

    <div class="form-group">
        <label class="bmd-label-floating text-capitalize">sekolah</label>
        <input type="text" class="form-control" name="seleksi_sekolah" 
        @if( isset($edit) ) value="{{ $edit->seleksi_sekolah }}" @else value="{{ old('seleksi_sekolah') }}" @endif required>
    </div>

    <div class="form-group">
        <label class="bmd-label-floating text-capitalize">telepon</label>
        <input type="number" class="form-control" name="seleksi_telepon" 
        @if( isset($edit) ) value="{{ $edit->seleksi_telepon }}" @else value="{{ old('seleksi_telepon') }}" @endif required>
    </div>

    <div class="form-group">
        <label class="bmd-label-floating text-capitalize">nama ortu</label>
        <input type="text" class="form-control" name="seleksi_nama_ortu" 
        @if( isset($edit) ) value="{{ $edit->seleksi_nama_ortu }}" @else value="{{ old('seleksi_nama_ortu') }}" @endif required>
    </div>

    </div>
    <div class="col-md-6">

        <div class="input-file-box row">
            <div class="col-md-4 mt-40">
                <button type="submit" class="btn btn-success">Foto</button>
                <input class="form-control input-file" type="file" id="input-foto" name="seleksi_foto" accept=".jpg,.png">
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                            <img src="" id="prev-foto" class="img-preview"> 
                        @if(isset($edit))
                            <span>baru</span>
                        @endif    
                    </div>
                    <div class="col-md-6">
                        @if(isset($edit))
                            <img src="{{ asset('/public/images/seleksi/foto').'/'.$edit->seleksi_foto }}" alt="" class="img-preview">
                            <span>lama</span>
                        @endif    
                    </div>
                </div>
            </div>
            <hr>
        </div>

        <div class="input-file-box row">
            <div class="col-md-4 mt-40">
                <button type="submit" class="btn btn-success">KK</button>
                <input class="form-control input-file" type="file" id="input-kk" name="seleksi_kk" accept=".jpg,.png">
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                            <img src="" id="prev-kk" class="img-preview"> 
                        @if(isset($edit))
                            <span>baru</span>
                        @endif    
                    </div>
                    <div class="col-md-6">
                        @if(isset($edit))
                            <img src="{{ asset('/public/images/seleksi/kk').'/'.$edit->seleksi_kk }}" alt="" class="img-preview">
                            <span>lama</span>
                        @endif    
                    </div>
                </div>
            </div>
            <hr>
        </div>

        <div class="input-file-box row">
            <div class="col-md-4 mt-40">
                <button type="submit" class="btn btn-success">Akte</button>
                <input class="form-control input-file" type="file" id="input-akte" name="seleksi_akte" accept=".jpg,.png">
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                            <img src="" id="prev-akte" class="img-preview"> 
                        @if(isset($edit))
                            <span>baru</span>
                        @endif    
                    </div>
                    <div class="col-md-6">
                        @if(isset($edit))
                            <img src="{{ asset('/public/images/seleksi/akte').'/'.$edit->seleksi_akte }}" alt="" class="img-preview">
                            <span>lama</span>
                        @endif    
                    </div>
                </div>
            </div>
            <hr>
        </div>

        <div class="input-file-box row">
            <div class="col-md-4 mt-40">
                <button type="submit" class="btn btn-success">Pernyataan</button>
                <input class="form-control input-file" type="file" id="input-sp" name="seleksi_sp" accept=".jpg,.png">
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                            <img src="" id="prev-sp" class="img-preview"> 
                        @if(isset($edit))
                            <span>baru</span>
                        @endif    
                    </div>
                    <div class="col-md-6">
                        @if(isset($edit))
                            <img src="{{ asset('/public/images/seleksi/kk').'/'.$edit->seleksi_sp }}" alt="" class="img-preview">
                            <span>lama</span>
                        @endif    
                    </div>
                </div>
            </div>
            <hr>
        </div>

    </div>
</div>


                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(target).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-kk").change(function() {
        readURL(this, '#prev-kk');
    });

    $("#input-foto").change(function() {
        readURL(this, '#prev-foto');
    });

    $("#input-akte").change(function() {
        readURL(this, '#prev-akte');
    });

    $("#input-sp").change(function() {
        readURL(this, '#prev-sp');
    });

</script>
@endsection