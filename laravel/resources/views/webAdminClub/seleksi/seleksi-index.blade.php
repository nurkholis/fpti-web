@extends('layouts.adminClub')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('adminClub.home') }}">Beranda</a>/
@endsection

@php
use Config\Kholis as Helper;
@endphp

@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      @if ( isset($baru_buka) )
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <h5>
                  Pendaftaran Tahun {{ $baru_buka->tahun_seleksi }} <br>
              </h5>
               dibuka {{ Helper::tanggal( $baru_buka->tanggal_buka ) }} <br> 
               ditutup {{ Helper::tanggal( $baru_buka->tanggal_tutup ) }}
            </div>
          </div>
        </div>
      @else
        <div class="col-md-12">
          
          <!-- atlet belum terdaftar -->
          <div class="card">
            <div class="card-header card-header-success">
            <h4 class="card-title">Data Atlet</h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  @if( Request::get('search0') )
                  <div class="alert alert-info alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">info</i>
                    <a href="{{ route('adminClub.seleksi.index') }}" class="close" aria-label="Close">
                      <i class="material-icons">close</i>
                    </a>
                    <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search0') }}"</strong></span>
                  </div>
                  @endif
                </div>
                <div class="col-md-6 float-right">
                <form class="navbar-form">
                  <span class="bmd-form-group"><div class="input-group no-border">
                    <input type="text" name="search0" value="{{ Request::get('search0') }}" class="form-control" placeholder="Cari..." autofocus>
                    <button type="submit" class="btn btn-success btn-round btn-just-icon">
                      <i class="material-icons">search</i>
                      <div class="ripple-container"></div>
                    </button>
                  </div>
                  </span>
                </form>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table" id="tabel-atlet">
                  <thead class=" text-success">
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Kelas</th>
                    <th>Gender</th>
                    <th>Tinggi</th>
                    <th>Berat</th>
                    <th>Tetala</th>
                    <th>Telepon</th>
                    <th>Alamat</th>
                    <th>Daftarkan</th>
                  </thead>
                  <tbody>
                    @foreach($atlet0 as $item)
                      <tr>
                        <td>{{ $item->atlet_nama }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ Helper::kelasUmur( $item->atlet_tanggal_lahir ) }}</td>
                        <td>{{ $item->atlet_jenis_kelamin }}</td>
                        <td>{{ $item->atlet_tinggi }} cm</td>
                        <td>{{ $item->atlet_berat }} kg</td>
                        <td>{{ $item->atlet_tempat_lahir }}, {{ Helper::tanggal($item->atlet_tanggal_lahir) }}</td>
                        <td>{{ $item->atlet_telepon }}</td>
                        <td>{{ $item->atlet_alamat }}</td>
                        <td class="td-actions">
                          <a href="{{ route('adminClub.seleksi.daftar', [1,$item->id_atlet] ) }}" rel="tooltip" title="" class="btn btn-success btn-link btn-sm" data-original-title="Daftarkan">
                            <i class="material-icons">check_circle</i>
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="float-right">
                    {{ $atlet0->links() }}
                </div>
              </div>
            </div>
          </div>

          <!-- atlet sudah terdaftar -->
          <div class="card">
            <div class="card-header card-header-success">
            <h4 class="card-title">Data Atlet</h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  @if( Request::get('search1') )
                  <div class="alert alert-info alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">info</i>
                    <a href="{{ route('adminClub.seleksi.index') }}" class="close" aria-label="Close">
                      <i class="material-icons">close</i>
                    </a>
                    <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search1') }}"</strong></span>
                  </div>
                  @endif
                </div>
                <div class="col-md-6 float-right">
                <form class="navbar-form">
                  <span class="bmd-form-group"><div class="input-group no-border">
                    <input type="text" name="search1" value="{{ Request::get('search1') }}" class="form-control" placeholder="Cari..." autofocus>
                    <button type="submit" class="btn btn-success btn-round btn-just-icon">
                      <i class="material-icons">search</i>
                      <div class="ripple-container"></div>
                    </button>
                  </div>
                  </span>
                </form>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table" id="tabel-atlet">
                  <thead class=" text-success">
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Kelas</th>
                    <th>Gender</th>
                    <th>Tinggi</th>
                    <th>Berat</th>
                    <th>Tetala</th>
                    <th>Telepon</th>
                    <th>Alamat</th>
                    <th>Batalkan</th>
                  </thead>
                  <tbody>
                    @foreach($atlet1 as $item)
                      <tr>
                        <td>{{ $item->atlet_nama }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ Helper::kelasUmur( $item->atlet_tanggal_lahir ) }}</td>
                        <td>{{ $item->atlet_jenis_kelamin }}</td>
                        <td>{{ $item->atlet_tinggi }} cm</td>
                        <td>{{ $item->atlet_berat }} kg</td>
                        <td>{{ $item->atlet_tempat_lahir }}, {{ Helper::tanggal($item->atlet_tanggal_lahir) }}</td>
                        <td>{{ $item->atlet_telepon }}</td>
                        <td>{{ $item->atlet_alamat }}</td>
                        <td class="td-actions">
                        </a>
                          <a href="{{ route('adminClub.seleksi.daftar', [0,$item->id_atlet] ) }}" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Batalkan">
                            <i class="material-icons">cancel</i>
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="float-right">
                    {{ $atlet0->links() }}
                </div>
              </div>
            </div>
          </div>

          <div id="modal">

            <!-- Modal -->
            <div class="modal fade" id="modal-atlet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Info atlet</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">

                    <div class="table-responsive">
                        <table class="table" id="tabel-atlet">
                          <tbody>
                            <tr>
                              <td class="text-success">Golongan Darah</td>
                              <td id="info-atlet_golongan_darah"></td>
                            </tr>
                            <tr>
                              <td class="text-success">Sekolah</td>
                              <td id="info-atlet_sekolah"></td>
                            </tr>
                            <tr>
                              <td class="text-success">Nama Ortu</td>
                              <td id="info-atlet_nama_ortu"></td>
                            </tr>
                            <tr>
                              <td class="text-success">Foto</td>
                              <td id="info-atlet_foto"><a href="" target="_blank">
                                <img src="" >
                              </td>
                            </tr>
                            <tr>
                              <td class="text-success">Akte</td>
                              <td id="info-atlet_akte"><a href="" target="_blank">
                                <img src="">
                              </td>
                            </tr>
                            <tr>
                              <td class="text-success">Surat Pernyataan</td>
                              <td id="info-atlet_sp"><a href="" target="_blank">
                                <img src="">
                              </td>
                            </tr>
                            <tr>
                              <td class="text-success">KK</td>
                              <td  id="info-atlet_kk"><a href="" target="_blank">
                                <img src="">
                              </td>
                            </tr>
                          </tbody>
                        </table>
                  </div>

                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      @endif
    </div>
  </div>
</div>
@endsection

@section('js')
    <script>
        function confirmDelete(){
          if (!confirm("hapus data?")) {
              event.preventDefault()
          }
        }
        $("#tabel-atlet").on("click", "#btn-info", function() {
            var data_id =  $(this).attr('data_id');
            
            $('#modal-atlet').modal('show');
            //alert(data_id);
            getatlet(data_id);
        });
        function getatlet(id){
            $.ajax({
                type: 'GET',
                url: '{{ asset("api/atlet?id_atlet=") }}' + id,
                success: function(data){
                    var data = data.data[0]
                    console.log(data.atlet_golongan_darah);
                    $('#info-atlet_golongan_darah').html(data.atlet_golongan_darah);
                    $('#info-atlet_sekolah').html(data.atlet_sekolah);
                    $('#info-atlet_nama_ortu').html(data.atlet_nama_ortu);

                    $('#info-atlet_foto img').attr( "src" , "{{ asset('/public/images/atlet/foto').'/' }}" + data.atlet_foto );
                    $('#info-atlet_foto a').attr( "href" , "{{ asset('/public/images/atlet/foto').'/' }}" + data.atlet_foto );

                    $('#info-atlet_akte img').attr( "src" , "{{ asset('/public/images/atlet/akte').'/' }}" + data.atlet_akte );
                    $('#info-atlet_akte a').attr( "href" , "{{ asset('/public/images/atlet/akte').'/' }}" + data.atlet_akte );

                    $('#info-atlet_kk img').attr( "src" , "{{ asset('/public/images/atlet/kk').'/' }}" + data.atlet_kk );
                    $('#info-atlet_kk a').attr( "href" , "{{ asset('/public/images/atlet/kk').'/' }}" + data.atlet_kk );

                    $('#info-atlet_sp img').attr( "src" , "{{ asset('/public/images/atlet/sp').'/' }}" + data.atlet_sp );
                    $('#info-atlet_sp a').attr( "href" , "{{ asset('/public/images/atlet/sp').'/' }}" + data.atlet_sp );
                }
            });
        }
    </script>
@endsection