@extends('layouts.atlet')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('atlet.home') }}">Beranda</a>/
@endsection

@section('content')
<div class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-md-12">
                @if ($errors->any())
                <div class="alert alert-danger alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">info</i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                    </button>
                    <span data-notify="message">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </span>
                </div>
                @endif
            </div>

            <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Profil</h4>
                  <p class="card-category">Biodata</p>
                </div>
                <div class="card-body">

                  <form method="POST" action="{{ route('atlet.atlet.put') }}" enctype="multipart/form-data">
                  <input type="hidden" name="_method" value="PUT">
                  {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">email</label>
    <input type="text" class="form-control" name="email"
     @if( isset($edit) ) value="{{ $edit->email }}" @else value="{{ old('email') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">nama </label>
    <input type="text" class="form-control" name="atlet_nama"
     @if( isset($edit) ) value="{{ $edit->atlet_nama }}" @else value="{{ old('atlet_nama') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">tempat lahir </label>
    <input type="text" class="form-control" name="atlet_temnpat_lahir"
     @if( isset($edit) ) value="{{ $edit->atlet_tempat_lahir }}" @else value="{{ old('atlet_tempat_lahir') }}" @endif required>
</div>

<div class="form-group">
        <label class="bmd-label-floating text-capitalize">tanggal lahir</label>
        <input type="date" class="form-control" name="atlet_tanggal_lahir" 
        @if( isset($edit) ) value="{{ $edit->atlet_tanggal_lahir }}" @else value="{{ old('atlet_tanggal_lahir') }}" @endif required>
    </div>

    <div class="form-group">
        <label class="bmd-label-floating text-capitalize">jenis kelamin</label>
        <select class="form-control" name="atlet_jenis_kelamin">
            <option value="L" @if( isset($edit) ) {{ $edit->atlet_jenis_kelamin == 'L' ? 'selected' : '' }} @endif >Laki-laki</option>
            <option value="P" @if( isset($edit) ) {{ $edit->atlet_jenis_kelamin == 'P' ? 'selected' : '' }} @endif >Perempuan</option>
        </select>
    </div>

    <div class="form-group">
        <label class="bmd-label-floating text-capitalize">golongan darah</label>
        <select class="form-control" name="atlet_golongan_darah">
            <option value="A" @if( isset($edit) ) {{ $edit->atlet_golongan_darah == 'A' ? 'selected' : '' }} @endif >A</option>
            <option value="B" @if( isset($edit) ) {{ $edit->atlet_golongan_darah == 'B' ? 'selected' : '' }} @endif >B</option>
            <option value="AB" @if( isset($edit) ) {{ $edit->atlet_golongan_darah == 'AB' ? 'selected' : '' }} @endif >AB</option>
            <option value="O" @if( isset($edit) ) {{ $edit->atlet_golongan_darah == 'O' ? 'selected' : '' }} @endif >O</option>
        </select>
    </div>

    <div class="form-group">
        <label class="bmd-label-floating text-capitalize">tinggi</label>
        <input type="number" class="form-control" name="atlet_tinggi" 
        @if( isset($edit) ) value="{{ $edit->atlet_tinggi }}" @else value="{{ old('atlet_tinggi') }}" @endif required>
    </div>

    <div class="form-group">
        <label class="bmd-label-floating text-capitalize">berat</label>
        <input type="number" class="form-control" name="atlet_berat" 
        @if( isset($edit) ) value="{{ $edit->atlet_berat }}" @else value="{{ old('atlet_berat') }}" @endif required>
    </div>

    <div class="form-group">
        <label class="bmd-label-floating text-capitalize">alamat</label>
        <input type="text" class="form-control" name="atlet_alamat" 
        @if( isset($edit) ) value="{{ $edit->atlet_alamat }}" @else value="{{ old('atlet_alamat') }}" @endif required>
    </div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">sekolah</label>
    <input type="text" class="form-control" name="atlet_sekolah" 
    @if( isset($edit) ) value="{{ $edit->atlet_sekolah }}" @else value="{{ old('atlet_sekolah') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">telepon</label>
    <input type="number" class="form-control" name="atlet_telepon" 
    @if( isset($edit) ) value="{{ $edit->atlet_telepon }}" @else value="{{ old('atlet_telepon') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">nama ortu</label>
    <input type="text" class="form-control" name="atlet_nama_ortu" 
    @if( isset($edit) ) value="{{ $edit->atlet_nama_ortu }}" @else value="{{ old('atlet_nama_ortu') }}" @endif required>
</div>

<div class="input-file-box row">
   <div class="col-md-4 mt-40">
       <button type="submit" class="btn btn-primary">Foto</button>
       <input class="form-control input-file" type="file" id="input-foto" name="atlet_foto" accept=".jpg,.png">
   </div>
   <div class="col-md-8">
       <div class="row">
           <div class="col-md-12">
                   <img src="" id="prev-foto" class="img-preview">    
           </div>
       </div>
   </div>
   <hr>
</div>

                      <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  </form>

                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card card-profile">
                <div class="card-body">
                  <div class="card-avatar">
                  <img src="{{ asset('/public/images/atlet/foto').'/'.$edit->atlet_foto }}" alt="" class="img-preview">
                  </div>
                  <h6 class="card-category text-gray">{{ $edit->nama_admin }}</h6>
                  <h4 class="card-title">{{ $edit->email }}</h4>
                  <p class="card-description">
                    
                  </p>
                </div>
              </div>
            </div>
            
            <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Profil</h4>
                  <p class="card-category">Password</p>
                </div>
                <div class="card-body">

                  <form method="POST" action="{{ route('atlet.atlet.putPw') }}" enctype="multipart/form-data">
                  <input type="hidden" name="_method" value="PUT">
                  {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">password lama</label>
    <input type="text" class="form-control" name="password_lama"
     @if( isset($edit) ) value="{{ $edit->password_lama }}" @else value="{{ old('password_lama') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">password baru</label>
    <input type="text" class="form-control" name="password_baru"
     @if( isset($edit) ) value="{{ $edit->password_baru }}" @else value="{{ old('password_baru') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">password konfirmasi</label>
    <input type="text" class="form-control" name="password_konfirmasi"
     @if( isset($edit) ) value="{{ $edit->password_konfirmasi }}" @else value="{{ old('password_konfirmasi') }}" @endif required>
</div>

                      <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  </form>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
@endsection

@section('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-sertifikat').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-foto").change(function() {
        readURL(this);
    });

    $("#input-sertifikat").change(function() {
        readURL1(this);
    });
</script>
@endsection