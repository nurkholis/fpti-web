@extends('layouts.pelatih')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('pelatih.home') }}">Beranda</a>/
@endsection

@section('content')


<div class="content">
  <a href="{{ route('pelatih.foto.create') }}" class="btn btn-primary btn-round btn-just-icon btn-floating">
    <i class="material-icons">add</i>
    <div class="ripple-container"></div>
  </a>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
          <h4 class="card-title">Data Foto</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                @if( Request::get('search') )
                <div class="alert alert-info alert-with-icon" data-notify="container">
                  <i class="material-icons" data-notify="icon">info</i>
                  <a href="{{ route('pelatih.foto.index') }}" class="close" aria-label="Close">
                    <i class="material-icons">close</i>
                  </a>
                  <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search') }}"</strong></span>
                </div>
                @endif
              </div>
              <div class="col-md-6 float-right">
              <form class="navbar-form">
                <span class="bmd-form-group"><div class="input-group no-border">
                  <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Cari..." autofocus>
                  <button type="submit" class="btn btn-primary btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                    <div class="ripple-container"></div>
                  </button>
                </div>
                </span>
              </form>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>No</th>
                  <th>Judul Foto</th>
                  <th>Tanggal Foto</th>
                  <th>Keterangan Foto</th>
                  <th>Foto</th>
                  <th>Aksi</th>
                </thead>
                <tbody>
                  @foreach($data as $item)
                    <tr>
                      <td>{{ $loop->iteration  }}</td>
                      <td>{{ $item->judul_foto }}</td>
                      <td>{{ $item->tanggal_foto }}</td>
                      <td>{{ $item->keterangan_foto }}</td>
                      <td><img src="{{ asset('/public/images/foto').'/'.$item->foto }}"></td>
                      <td class="td-actions">
                        <a href="{{ route('pelatih.foto.edit', $item->id_foto) }}" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Sunting">
                          <i class="material-icons">edit</i>
                        <div class="ripple-container"></div></a>
                        <a href="{{ route('pelatih.foto.delete', $item->id_foto) }}" onclick="confirmDelete()" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Hapus">
                          <i class="material-icons">close</i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="float-right">
                  {{ $data->links() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
    <script>
        function confirmDelete(){
            if (!confirm("hapus data?")) {
                event.preventDefault()
            }
        }
    </script>
@endsection