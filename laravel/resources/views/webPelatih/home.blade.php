@extends('layouts.pelatih')
@section('content')
    <div class="contet">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 mt-40">
                    <div class="card">
                        <div class="card-body">
                            <h3>Selamat datang {{ Auth::guard('pelatih')->user()->nama_pelatih }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection