@extends('layouts.pelatih')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('pelatih.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('pelatih.kompetisi.index') }}">Kompetisi</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Kompetisi</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('pelatih.kompetisi.put', $edit->id_kompetisi) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('pelatih.kompetisi.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">nama kompetisi</label>
    <input type="text" class="form-control" name="nama_kompetisi" @if( isset($edit) ) value="{{ $edit->nama_kompetisi }}" 
    @else value="{{ old('nama_kompetisi') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">tanggal kompetisi</label>
    <input type="date" class="form-control" name="tanggal_kompetisi" @if( isset($edit) ) value="{{ $edit->tanggal_kompetisi }}" 
    @else value="{{ old('tanggal_kompetisi') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">keterangan kompetisi</label>
    <input type="text" class="form-control" name="keterangan_kompetisi" @if( isset($edit) ) value="{{ $edit->keterangan_kompetisi }}" 
    @else value="{{ old('keterangan_kompetisi') }}" @endif required>
</div>

<div class="input-file-box row">
    <div class="col-md-4 mt-40">
        <button type="submit" class="btn btn-success">Dokumen</button>
        <input class="form-control input-file" type="file" id="input-file" name="dokumen_kompetisi" accept=".pdf,.doc">
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                    <h3 id="nama_file"></h3>
                @if(isset($edit))
                    <span>baru</span>
                @endif    
            </div>
            <div class="col-md-6">
                @if(isset($edit))
                    <h3>{{ $edit->dokumen_kompetisi }}</h3>
                    <span>lama</span>
                @endif    
            </div>
        </div>
    </div>
    <hr>
</div>

<div class="input-file-box row">
    <div class="col-md-4 mt-40">
        <button type="submit" class="btn btn-success">Laporan</button>
        <input class="form-control input-file" type="file" id="input-laporan" name="laporan_kompetisi" accept=".pdf,.doc">
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                    <h3 id="laporan_kompetisi"></h3>
                @if(isset($edit))
                    <span>baru</span>
                @endif    
            </div>
            <div class="col-md-6">
                @if(isset($edit))
                    <h3>{{ $edit->laporan_kompetisi }}</h3>
                    <span>lama</span>
                @endif    
            </div>
        </div>
    </div>
    <hr>
 </div>
 
 <div class="input-file-box row">
    <div class="col-md-4 mt-40">
        <button type="submit" class="btn btn-success">Hasil</button>
        <input class="form-control input-file" type="file" id="input-hasil" name="hasil_kompetisi" accept=".pdf,.doc">
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                    <h3 id="hasil_kompetisi"></h3>
                @if(isset($edit))
                    <span>baru</span>
                @endif    
            </div>
            <div class="col-md-6">
                @if(isset($edit))
                    <h3>{{ $edit->hasil_kompetisi }}</h3>
                    <span>lama</span>
                @endif    
            </div>
        </div>
    </div>
    <hr>
 </div>
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            $('#nama_file').html(input.files[0]['name']);
        }
    }

    $("#input-file").change(function() {
        readURL(this);
    });

    function readURL1(input) {
        if (input.files && input.files[0]) {
            $('#laporan_kompetisi').html(input.files[0]['name']);
        }
    }

    $("#input-laporan").change(function() {
        readURL1(this);
    });

    function readURL2(input) {
        if (input.files && input.files[0]) {
            $('#hasil_kompetisi').html(input.files[0]['name']);
        }
    }

    $("#input-hasil").change(function() {
        readURL2(this);
    });
</script>
@endsection