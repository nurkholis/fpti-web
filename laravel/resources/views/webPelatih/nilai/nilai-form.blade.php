@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('admin.struktur.index') }}">Stuktur</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Stuktur</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.struktur.put', $edit->id_struktur) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.struktur.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">Jabatan</label>
    <select class="form-control" name="id_jabatan">
        @if (isset($edit))
            @foreach ($jabatan as $item)
                <option value="{{ $item->id_jabatan }}" {{ $item->id_jabatan == $edit->id_jabatan ? 'selected' : '' }} >{{ $item->nama_jabatan }}</option>
            @endforeach
        @else
            @foreach ($jabatan as $item)
                <option value="{{ $item->id_jabatan }}"> {{ $item->nama_jabatan }}</option>
            @endforeach
        @endif
    </select>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">Nama</label>
    <input type="text" class="form-control" name="struktur_nama"
     @if( isset($edit) ) value="{{ $edit->struktur_nama }}" @else value="{{ old('struktur_nama') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">Alamat</label>
    <input type="text" class="form-control" name="struktur_alamat"
     @if( isset($edit) ) value="{{ $edit->struktur_alamat }}" @else value="{{ old('struktur_alamat') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">Telepon</label>
    <input type="number" class="form-control" name="struktur_telepon"
     @if( isset($edit) ) value="{{ $edit->struktur_telepon }}" @else value="{{ old('struktur_telepon') }}" @endif required>
</div>

                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-sertifikat').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-foto").change(function() {
        readURL(this);
    });

    $("#input-sertifikat").change(function() {
        readURL1(this);
    });
</script>
@endsection