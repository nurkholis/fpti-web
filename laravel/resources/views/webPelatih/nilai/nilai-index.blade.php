@extends('layouts.pelatih')


@php
use Config\Kholis as Helper;
@endphp
@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
@endsection


@section('css')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">

        <!-- atlet -->
        <div class="card">
          <div class="card-header card-header-success">
          <h4 class="card-title">Penilaian Seleksi Atlet</h4>
          </div>
          <div class="card-body">
            <div class="row">

              <div class="col-md-12">
                @if( Request::get('search_seleksi') )
                <div class="alert alert-info alert-with-icon" data-notify="container">
                  <i class="material-icons" data-notify="icon">info</i>
                  <a href="{{ route('pelatih.nilai.index') }}" class="close" aria-label="Close">
                    <i class="material-icons">close</i>
                  </a>
                  <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search_seleksi') }}"</strong></span>
                </div>
                @endif
              </div>

              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-5">Pilih tahun seleksi</div>
                  <div class="col-md-7">
                    
                    <form class="navbar-form">
                      <span class="bmd-form-group">
                      <div class="input-group">
                            <select class="form-control" name="tahun_seleksi">
                                @if( Request::get('tahun_seleksi') ) 
                                    @foreach ($data_tahun_seleksi as $item)
                                        <option value="{{ $item->tahun_seleksi }}" {{ $item->tahun_seleksi == Request::get('tahun_seleksi') ? 'selected' : '' }} >{{ $item->tahun_seleksi }}</option>
                                    @endforeach
                                @else
                                    @foreach ($data_tahun_seleksi as $item)
                                        <option value="{{ $item->tahun_seleksi }}" {{ $item->tahun_seleksi == $tahun_seleksi->tahun_seleksi ? 'selected' : '' }}> {{ $item->tahun_seleksi }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <button type="submit" class="btn btn-success btn-round">Buka</button>
                            <button type="submit" class="btn btn-success btn-round" id="btn-print">Print</button>
                      </div>
                      </span>
                    </form>

                  </div>
                </div>
              </div>
              
              <div class="col-md-4">
                <form class="navbar-form">
                  <span class="bmd-form-group"><div class="input-group no-border">
                    <input type="text" name="search_seleksi" value="{{ Request::get('search_seleksi') }}" class="form-control" placeholder="Cari..." autofocus>
                    <button type="submit" class="btn btn-success btn-round btn-just-icon">
                      <i class="material-icons">search</i>
                      <div class="ripple-container"></div>
                    </button>
                  </div>
                  </span>
                </form>
              </div>

            </div>
            <div class="table-responsive">
              <table class="table table-bordered" id="tabel-seleksi" >
                <thead class=" text-success">
                  <tr>
                    <th>Nomor</th>
                    <th>Nama</th>
                    <th>Gender</th>
                    <th>Kelas</th>
                    <th>Club</th>
                    <th>Nilai</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($seleksi as $item)
                    <tr>
                      <td>{{ $loop->iteration  }}</td>
                      <td>{{ $item->atlet_nama }}</td>
                      <td>{{ $item->atlet_jenis_kelamin }}</td>
                      <td>{{ Helper::kelasUmur( $item->atlet_tanggal_lahir ) }}</td>
                      <td>{{ $item->nama_club }}</td>
                      <td>
                      <a rel="tooltip" id="btn-nilai" data-nama="{{ $item->atlet_nama }}" data-id="{{ $item->id_seleksi }}" class="btn btn-success btn-link btn-sm" data-original-title="Nilai {{ $item->atlet_nama }}">
                        <i class="material-icons">edit</i>
                      </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="float-right">
                  {{ $seleksi->links() }}
              </div>
            </div>
          </div>
        </div>

        <!-- nilai -->
        <div class="card">
          <div class="card-header card-header-primary">
          <h4 class="card-title">Hasil Seleksi Atlet</h4>
          </div>
          <div class="card-body">
            <div class="row">

              <div class="col-md-12">
                @if( Request::get('search_nilai') )
                <div class="alert alert-info alert-with-icon" data-notify="container">
                  <i class="material-icons" data-notify="icon">info</i>
                  <a href="{{ route('pelatih.nilai.index') }}" class="close" aria-label="Close">
                    <i class="material-icons">close</i>
                  </a>
                  <span data-notify="message">Menampilkan percarian dengan kata kunci <strong>"{{ Request::get('search_nilai') }}"</strong></span>
                </div>
                @endif
              </div>

              <div class="col-md-8">
                {{-- <div class="row">
                  <div class="col-md-5">Pilih tahun seleksi</div>
                  <div class="col-md-7">
                    
                    <form class="navbar-form">
                      <span class="bmd-form-group">
                      <div class="input-group">
                            <select class="form-control" name="tahun_seleksi2">
                                @if( Request::get('tahun_seleksi2') ) 
                                    @foreach ($data_tahun_seleksi as $item)
                                        <option value="{{ $item->tahun_seleksi }}" {{ $item->tahun_seleksi == Request::get('tahun_seleksi2') ? 'selected' : '' }} >{{ $item->tahun_seleksi }}</option>
                                    @endforeach
                                @else
                                    @foreach ($data_tahun_seleksi as $item)
                                        <option value="{{ $item->tahun_seleksi }}" {{ $item->tahun_seleksi == $tahun_seleksi2->tahun_seleksi ? 'selected' : '' }}> {{ $item->tahun_seleksi }}</option>
                                    @endforeach
                                @endif
                            </select>
                        <button type="submit" class="btn btn-primary btn-round">Buka</button> --}}
                        <a href="{{ route('pelatih.nilai.export', $tahun_seleksi->tahun_seleksi) }}" class="btn btn-primary btn-round">Export</a>
                      {{-- </div>
                      </span>
                    </form>

                  </div>
                </div> --}}
              </div>
              
              <div class="col-md-4">
                <form class="navbar-form">
                  <span class="bmd-form-group"><div class="input-group no-border">
                    <input type="text" name="search_nilai" value="{{ Request::get('search_nilai') }}" class="form-control" placeholder="Cari..." autofocus>
                    <button type="submit" class="btn btn-primary btn-round btn-just-icon">
                      <i class="material-icons">search</i>
                      <div class="ripple-container"></div>
                    </button>
                  </div>
                  </span>
                </form>
              </div>

            </div>

            <!-- atlet belum lolos -->
            <div class="table-responsive">
              <table class="table table-bordered" id="tabel-belum-lulus" >
                <thead class=" text-primary">
                  <tr>
                    <th>Nomor</th>
                    <th>Nama</th>
                    <th>Gender</th>
                    <th>Kelas</th>
                    <th>Club</th>
                    <th>P.Lead </th>
                    <th>P.Speed </th>
                    <th>T.Fisik </th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($nilai as $item)
                    <tr>
                      <td>{{ $loop->iteration  }}</td>
                      <td>{{ $item->atlet_nama }}</td>
                      <td>{{ $item->atlet_jenis_kelamin }}</td>
                      <td>{{ Helper::kelasUmur( $item->atlet_tanggal_lahir ) }}</td>
                      <td>{{ $item->nama_club }}</td>
                      <td>{{ $item->panjat_lead }}</td>
                      <td>{{ $item->panjat_speed }}</td>
                      <td>{{ ( $item->lari + $item->push_up + $item->sit_up + $item->pull_up) / 4 }}</td>
                      <td>
                      <a href="{{ route('pelatih.nilai.lulus' , [$item->id_seleksi, '1']) }}" rel="tooltip"class="btn btn-primary btn-link btn-sm">
                        lulus
                      </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="float-right">
                  {{ $nilai->links() }}
              </div>
            </div>
            
            <h4 class="card-title">Data atlet lulus</h4>
            <!-- atlet yang sudah lulus -->
            
            <div class="table-responsive">
              <table class="table table-bordered" id="tabel-lulus" >
                <thead class=" text-primary">
                  <tr>
                    <th >Nomor</th>
                    <th>Nama</th>
                    <th>Gender</th>
                    <th>Kelas</th>
                    <th>Club</th>
                    <th>P.Lead</th>
                    <th>P.Speed</th>
                    <th>T.Fisik</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($nilai_lulus as $item)
                    <tr>
                      <td>{{ $loop->iteration  }}</td>
                      <td>{{ $item->atlet_nama }}</td>
                      <td>{{ $item->atlet_jenis_kelamin }}</td>
                      <td>{{ Helper::kelasUmur( $item->atlet_tanggal_lahir ) }}</td>
                      <td>{{ $item->nama_club }}</td>
                      <td>{{ $item->panjat_lead }}</td>
                      <td>{{ $item->panjat_speed }}</td>
                      <td>{{ ( $item->lari + $item->push_up + $item->sit_up + $item->pull_up) / 4 }}</td>
                      <td>
                        @if (!$item->kirim)
                          <a href="{{ route('pelatih.nilai.lulus' , [$item->id_seleksi, '0']) }}" rel="tooltip"class="btn btn-primary btn-link btn-sm">
                            batal lulus
                          </a>
                          <br>
                          <a href="{{ route('pelatih.nilai.kirim' , [$item->id_seleksi, '1']) }}" rel="tooltip"class="btn btn-primary btn-link btn-sm">
                            kirim
                          </a>
                        @endif
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="float-right">
                  {{ $nilai_lulus->links() }}
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div id="print" >
  <table class="table table-bordered" >
    <thead class=" text-success">
      <tr>
        <th>Nomor</th>
        <th>Nama</th>
        <th>Gender</th>
        <th>Kelas</th>
        <th>Club</th>
        <th>Panjat Lead</th>
        <th>Panjat Speed</th>
        <th>lari</th>
        <th>Push Up</th>
        <th>Pull Up</th>
        <th>Sit Up</th>
      </tr>
    </thead>
    <tbody>
      @foreach($seleksi as $item)
        <tr>
          <td>{{ $loop->iteration  }}</td>
          <td>{{ $item->atlet_nama }}</td>
          <td>{{ $item->atlet_jenis_kelamin }}</td>
          <td>{{ Helper::kelasUmur( $item->atlet_tanggal_lahir ) }}</td>
          <td>{{ $item->nama_club }}</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>

<!-- Modal -->
<div id="modal">
  <div class="modal fade" id="modal-nilai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-nilai-label"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form method="POST" action="{{ route('pelatih.nilai.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group" style="display: none">
                <label class="bmd-label-floating text-capitalize">id atlet</label>
                <input type="text" class="form-control" id="id_seleksi" name="id_seleksi" value="" required>
            </div>

            <div class="form-group">
                <label class="bmd-label-floating text-capitalize">panjat lead</label>
                <input type="text" class="form-control" name="panjat_lead" value="" required>
            </div>

            <div class="form-group">
                <label class="bmd-label-floating text-capitalize">panjat speed</label>
                <input type="text" class="form-control" name="panjat_speed" value="" required>
            </div>

            <div class="form-group">
                <label class="bmd-label-floating text-capitalize">Lari</label>
                <input type="text" class="form-control" name="lari" value="" required>
            </div>

            <div class="form-group">
                <label class="bmd-label-floating text-capitalize">push up</label>
                <input type="text" class="form-control" name="push_up" value="" required>
            </div>

            <div class="form-group">
                <label class="bmd-label-floating text-capitalize">pull up</label>
                <input type="text" class="form-control" name="pull_up" value="" required>
            </div>

            <div class="form-group">
                <label class="bmd-label-floating text-capitalize">sit up</label>
                <input type="text" class="form-control" name="sit_up" value="" required>
            </div>

              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script>
    $("#tabel-seleksi").on("click", "#btn-nilai", function() {
        var id = $(this).data('id');
        var nama = $(this).data('nama');
        $('#modal-nilai').modal('show');
        $('#modal-nilai-label').html('Niali ' + nama);
        $('#id_seleksi').attr('value', id);
        
    });

    function confirmDelete(){
        if (!confirm("hapus data?")) {
            event.preventDefault()
        }
    }

    $('#tabel-belum-lulus').DataTable({searching: false, paging: false, info: false});
    $('#tabel-lulus').DataTable({searching: false, paging: false, info: false});

    $( "#btn-print" ).click(function() {
        printdiv('print');
      });
    function printdiv(printpage)
    {
      var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr+newstr+footstr;
        console.log(newstr);
        
        location.reload();
        window.print();
        document.body.innerHTML = oldstr;
    }
  </script>
@endsection