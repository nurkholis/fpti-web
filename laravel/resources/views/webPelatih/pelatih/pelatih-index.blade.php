@extends('layouts.pelatih')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
@endsection

@section('content')
<div class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-md-12">
                @if ($errors->any())
                <div class="alert alert-danger alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">info</i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                    </button>
                    <span data-notify="message">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </span>
                </div>
                @endif
            </div>

            <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Profil</h4>
                  <p class="card-category">Biodata</p>
                </div>
                <div class="card-body">

                  <form method="POST" action="{{ route('pelatih.pelatih.put') }}" enctype="multipart/form-data">
                  <input type="hidden" name="_method" value="PUT">
                  {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">email</label>
    <input type="text" class="form-control" name="email"
     @if( isset($edit) ) value="{{ $edit->email }}" @else value="{{ old('email') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">nama </label>
    <input type="text" class="form-control" name="nama_pelatih"
     @if( isset($edit) ) value="{{ $edit->nama_pelatih }}" @else value="{{ old('nama_pelatih') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">jabatan pelatih </label>
    <input type="text" class="form-control" name="jabatan_pelatih"
     @if( isset($edit) ) value="{{ $edit->jabatan_pelatih }}" @else value="{{ old('jabatan_pelatih') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">alamat pelatih </label>
    <input type="text" class="form-control" name="alamat_pelatih"
     @if( isset($edit) ) value="{{ $edit->alamat_pelatih }}" @else value="{{ old('alamat_pelatih') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">sertifikasi pelatih </label>
    <input type="text" class="form-control" name="sertifikasi_pelatih"
     @if( isset($edit) ) value="{{ $edit->sertifikasi_pelatih }}" @else value="{{ old('sertifikasi_pelatih') }}" @endif required>
</div>

<div class="input-file-box row">
   <div class="col-md-4 mt-40">
       <button type="submit" class="btn btn-primary">Foto</button>
       <input class="form-control input-file" type="file" id="input-foto" name="foto_pelatih" accept=".jpg,.png">
   </div>
   <div class="col-md-8">
       <div class="row">
           <div class="col-md-12">
                   <img src="" id="prev-foto" class="img-preview">    
           </div>
       </div>
   </div>
   <hr>
</div>

                      <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  </form>

                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card card-profile">
                <div class="card-body">
                  <div class="card-avatar">
                  <img src="{{ asset('/public/images/pelatih').'/'.$edit->foto_pelatih }}" alt="" class="img-preview">
                  </div>
                  <h6 class="card-category text-gray">{{ $edit->nama_admin }}</h6>
                  <h4 class="card-title">{{ $edit->email }}</h4>
                  <p class="card-description">
                    
                  </p>
                </div>
              </div>
            </div>
            
            <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Profil</h4>
                  <p class="card-category">Password</p>
                </div>
                <div class="card-body">

                  <form method="POST" action="{{ route('pelatih.pelatih.putPw') }}" enctype="multipart/form-data">
                  <input type="hidden" name="_method" value="PUT">
                  {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">password lama</label>
    <input type="text" class="form-control" name="password_lama"
     @if( isset($edit) ) value="{{ $edit->password_lama }}" @else value="{{ old('password_lama') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">password baru</label>
    <input type="text" class="form-control" name="password_baru"
     @if( isset($edit) ) value="{{ $edit->password_baru }}" @else value="{{ old('password_baru') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">password konfirmasi</label>
    <input type="text" class="form-control" name="password_konfirmasi"
     @if( isset($edit) ) value="{{ $edit->password_konfirmasi }}" @else value="{{ old('password_konfirmasi') }}" @endif required>
</div>

                      <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  </form>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
@endsection

@section('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-sertifikat').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-foto").change(function() {
        readURL(this);
    });

    $("#input-sertifikat").change(function() {
        readURL1(this);
    });
</script>
@endsection