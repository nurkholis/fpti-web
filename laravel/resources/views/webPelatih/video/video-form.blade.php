@extends('layouts.pelatih')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('pelatih.home') }}">Beranda</a>/
<a class="navbar-brand" href="{{ route('pelatih.video.index') }}">Video</a>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Video</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">info</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('pelatih.video.put', $edit->id_video) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('pelatih.video.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">judul video</label>
    <input type="text" class="form-control" name="judul_video"
     @if( isset($edit) ) value="{{ $edit->judul_video }}" @else value="{{ old('judul_video') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">tanggal video</label>
    <input type="date" class="form-control" name="tanggal_video"
     @if( isset($edit) ) value="{{ $edit->tanggal_video }}" @else value="{{ old('tanggal_video') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">video </label>
    <input type="text" class="form-control" name="video"
     @if( isset($edit) ) value="{{ $edit->video }}" @else value="{{ old('video') }}" @endif required>
</div>

                                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-sertifikat').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-foto").change(function() {
        readURL(this);
    });

    $("#input-sertifikat").change(function() {
        readURL1(this);
    });
</script>
@endsection