@extends('layouts.visitor')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-primary text-center" style="font-size:8rem; margin-top:20px;">404</h1>
            <br>
            <h3 class="text-primary text-center" >NOT FOUND</h3>
        </div>
    </div>
</div>
@endsection