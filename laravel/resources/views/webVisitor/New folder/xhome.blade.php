@extends('layouts.visitor')

@section('content')
@php
use Config\Kholis as Helper;
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Foto</h4>
                </div>
                <div class="card-body">
                    @foreach( $foto as $item)
                        <div class="box">
                            <div class="box-header">
                                <img src="{{ asset('/public/images/foto').'/'.$item->foto }}" alt="" class="img-preview">
                                <h6 class="card-category text-gray">{{ $item->judul_foto }}</h6>
                            </div>
                        </div>
                    @endforeach
                    <div class="d-flex justify-content-center">
                    <a href="{{ route('visitor.foto.index') }}" class="btn btn-primary btn-round pull-right">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach ($foto as $item)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->iteration - 1 }}" class="@if ( $loop->iteration == 1 ) active  @endif"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach ($foto as $item)
                            <div class="carousel-item @if ( $loop->iteration == 1 ) active  @endif">
                                <img class="d-block w-100" src="{{ asset('/public/images/foto').'/'.$item->foto }}">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>{{ $item->judul_foto }}</h5>
                                    <p>{{ $item->keterangan_foto }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Informasi</h4>
                </div>
                <div class="card-body">
                    @foreach( $info as $item)
                        <div class="box">
                            <div class="box-header">
                                <h4 class="">{{ $item->judul_info }}</h4>
                                <div class="box-category">
                                    <i class="material-icons">person</i> {{ $item->nama_admin }} |
                                    <i class="material-icons">alarm</i>{{ Helper::tanggal( $item->tanggal_info ) }}
                                </div>
                            </div>
                            <div class="box-content">
                                <p class="card-description">{{ $item->info  }}</p>
                            </div>
                        </div>
                    @endforeach
                    <div class="d-flex justify-content-center">
                    <a href="{{ route('visitor.info.index') }}" class="btn btn-primary btn-round pull-right">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Video</h4>
                </div>
                <div class="card-body">
                    @foreach( $video as $item )
                        <div class="box">
                            <div class="box-header">
                                <iframe src="https://www.youtube.com/embed/{{ $item->video }}">
                                </iframe>
                                <h6 class="card-category text-gray">{{  $item->judul_video}}</h6>
                            </div>
                        </div>
                    @endforeach
                    <div class="d-flex justify-content-center">
                    <a href="{{ route('visitor.video.index') }}" class="btn btn-primary btn-round pull-right">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection