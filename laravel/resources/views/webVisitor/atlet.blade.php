@extends('layouts.visitor')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Atlet FPTI</h4>
                </div>
                <div class="card-body">
                <div class="table-responsive" >
                    <table id="table" class="table table-bordered" style="width:100%">
                        <thead class="text-primary">
                            <tr>
                                <th>Nama</th>
                                <th>Gender</th>
                                <th>Tinggi / Cm</th>
                                <th>Berat / Kg</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Telepon</th>
								<th>Club</th>
                                <th>Alamat</th>
                                <th>Foto</th>
                            </tr>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('js')
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('visitor.atlet.data') }}",
            columns: [
                {data: 'atlet_nama'},
                {data: 'atlet_jenis_kelamin'},
                {data: 'atlet_tinggi'},
                {data: 'atlet_berat'},
                {data: 'atlet_tempat_lahir'},
                {data: 'atlet_tanggal_lahir'},
                {data: 'atlet_telepon'},
				{data: 'nama_club'},
                {data: 'atlet_alamat'},
                {data: 'foto'}
            ]
        });
    </script>
@endsection