@extends('layouts.visitor')

@php
    use Config\Kholis as Helper;
@endphp

@section('css')
    <style>
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Profil {{ $atlet->atlet_nama }}</h4>
                </div>
                <div class="card-body">
                    <img style="width: 100%" src="{{ asset('/public/images/atlet/foto').'/'.$atlet->atlet_foto }}" >
                    <table class="table">
                        <tbody>
                                <tr>
                                    <th scope="row">Club</th>
                                    <td>{{ $atlet->nama_club }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Tempat Lahir</th>
                                    <td>{{ $atlet->atlet_tempat_lahir }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Tanggal Lahir</th>
                                    <td>{{ Helper::tanggal( $atlet->atlet_tanggal_lahir ) }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Gender</th>
                                    <td>{{ $atlet->atlet_jenis_kelamin }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Alamat</th>
                                    <td>{{ $atlet->atlet_alamat }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Tinggi</th>
                                    <td>{{ $atlet->atlet_tinggi }} cm</td>
                                </tr>
                                <tr>
                                    <th scope="row">Berat</th>
                                    <td>{{ $atlet->atlet_berat }} kg</td>
                                </tr>
                                <tr>
                                    <th scope="row">Golongan Darah</th>
                                    <td>{{ $atlet->atlet_golongan_darah }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Telepon</th>
                                    <td>{{ $atlet->atlet_telepon }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Sekolah</th>
                                    <td>{{ $atlet->atlet_sekolah }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Nama Orang Tua</th>
                                    <td>{{ $atlet->atlet_nama_ortu }}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-nd-3"></div>
    </div>
</div>
@endsection

@section('js')
@endsection