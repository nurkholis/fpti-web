@extends('layouts.visitor')

@section('content')
@php
use Config\Kholis as Helper;
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            
                <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                            <h4 class="card-title"><i class="material-icons">photo_library</i> FPTI BERITA </h4>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-3"></div>

                                    <div class="col-md-6">
                                        <div class="box box-bordered">
                                            <div class="box-header">
                                                
                                                    <div>

                                                        <div class="zoomeffect">
                                                        <img class src="http://static.fpti.or.id/img/2018/11/siap-berlaga-di-asian-championship-2018-japan-520x292.jpeg" data-gallery="manual" alt="" class="img-preview">
                                                        <br>
                                                        <h6 class="card-category text-gray">
                                                            11 Atlet Panjat Tebing Siap Berlaga di Asian Championship 2018 di Jepang
                                                        </h6>
                                                        <div class="box-category">	
                                                            <p>TOTTORI–Federasi Panjat Tebing Indonesia (FPTI) mengirimkan 11 atlet andalan untuk mengikuti kejuaraan panjat tebing Asian Championships 2018 di Kurayoshi, Tottori, Jepang, 7-11 November. Kesebelas atlet tersebut terdiri dari enam atlet putra dan lima atlet putri.</p>
                                                            <p>Aries Susanti Rahayu yang memenangi dua seri kejuaraan panjat tebing dunia pada Oktober lalu akan menjadi andalan bersama Puji Lestari, Agustina Sari, Nurul Iqamah, dan Rajiah Salsabillah.</p>
                                                            <p>Di tim putra Aspar Jaelolo yang menduduki peringkat enam dunia akan turun bersama Sabri, Muhammad Hinayah, Pangeran Septo, Veddriq Leonardo serta Alfian M Fajri.</p>
                                                            <p>Ketua II Pengurus Pusat Federasi Panjat Tebing Indonesia (PP FPTI) Pristiawan Buntoro menyebut keikutsertaan kali ini merupakan upaya FPTI mempertahankan tradisi. Sejak menjadi tuan rumah pada tahun 1996, Indonesia tidak pernah absen mengikuti kejuaaraan panjat tebing tertinggi di benua Asia ini.</p>
                                                            <p>“Tahun (2017) lalu kita berhasil menjadi runner up. Semoga tradisi naik podium pada tahun ini juga bisa kita pertahankan,” tambah Pristiawan.</p>
                                                            
                                                        </div>
                                                        <img class src="http://static.fpti.or.id/img/2018/11/siap-berlaga-di-asian-championship-2018-japan-02-1024x576.jpeg" data-gallery="manual" alt="" class="img-preview">
                                                        <div class="box-category">	
                                                            <p>Berbeda dengan Asian Championship tahun lalu, setiap atlet panjat tebing Indonesia kali ini akan bermain di semua nomor, baik itu speed, lead maupun boulder. “Ini sangat berat, tapi ini kesempatan terbaik untuk atlet menunjukkan kemampuan,” ungkap Veddriq Leonardo, atlet Indonesia yang biasa bertanding di nomor speed.</p>
                                                            <p>Pelatih Timnas Panjat Tebing Indonesia Hendra Basir menyebut, ajang Asian Championships merupakan kesempatan bagi atlet untuk unjuk kemampuan di semua nomor. “Olimpiade itu mempertandingkan nomor combined. Bukan nomor speed saja. Jadi, ini semacam tes awal bagi atlet untuk menghadapi kualifikasi olimpiade,” imbuh Hendra.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3"></div>
                                    
                                </div>
                                    <div class="box-category">	
                                    {{-- <h6 class="card-category text-blue"><a href="{{ route('visitor.foto.index') }}" class="pull-right">Next Foto<i class="material-icons" class="pull-right" >arrow_forward_ios</i></a></h6>	 --}}
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
</div>
@endsection