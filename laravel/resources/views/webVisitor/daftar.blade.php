@extends('layouts.visitor')

@section('css')
    <style>
.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Pendaftaran anggota FPTI</h4>
                </div>
                <div class="card-body">
                        @if (Session::has('success'))
                            <div class="alert alert-info alert-with-icon" data-notify="container">
                                <i class="material-icons" data-notify="icon">info</i>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="material-icons">close</i>
                                </button>
                                <span data-notify="message">
                                    <ul>
                                        Berhasil mendaftar
                                    </ul>
                                </span>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger alert-with-icon" data-notify="container">
                                <i class="material-icons" data-notify="icon">info</i>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="material-icons">close</i>
                                </button>
                                <span data-notify="message">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </span>
                            </div>
                        @endif
                        <form method="POST" class="container-fluid" action="{{ route('visitor.daftar.store') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-md-3">Email</label>
                            <input type="email" name="email" value="{{ old('email') }}" class="form-control col-md-9" >
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">NIL</label>
                            <input type="number" name="anggota_nik" value="{{ old('anggota_nik') }}" class="form-control col-md-9" >
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">Nama</label>
                            <input type="text" name="anggota_nama" value="{{ old('anggota_nama') }}" class="form-control col-md-9" >
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">Tempat Lahir</label>
                            <input type="text" name="anggota_tempat_lahir" value="{{ old('anggota_tempat_lahir') }}" class="form-control col-md-9" >
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">Tanggal Lahir</label>
                            <input type="date" name="anggota_tanggal_lahir" value="{{ old('anggota_tanggal_lahir') }}" class="form-control col-md-9" >
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">Jenis Kelamin</label>
                            <select class="form-control col-md-9" name="anggota_jenis_kelamin">
                                <option value="L" {{ old('anggota_tanggal_lahir') == 'L' ? 'selected' : '' }}>Laki-laki</option>
                                <option value="P" {{ old('anggota_tanggal_lahir') == 'P' ? 'selected' : '' }}>Perempuan</option>
                            </select>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">Tinggi / cm</label>
                            <input type="number" name="anggota_tinggi" value="{{ old('anggota_tinggi') }}" class="form-control col-md-9" >
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">Berat / kg</label>
                            <input type="number" name="anggota_berat" value="{{ old('anggota_berat') }}" class="form-control col-md-9" >
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">Alamat</label>
                            <input type="text" name="anggota_alamat" value="{{ old('anggota_alamat') }}" class="form-control col-md-9" >
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">Telepon / No Hp</label>
                            <input type="number" name="anggota_telepon" value="{{ old('anggota_telepon') }}" class="form-control col-md-9" >
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">Pekerjaan</label>
                            <input type="text" name="anggota_pekerjaan" value="{{ old('anggota_pekerjaan') }}" class="form-control col-md-9" >
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3">Foto</label>
                             <!-- image-preview-filename input [CUT FROM HERE]-->
                            <div class="input-group image-preview  col-md-9">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input name="anggota_foto" type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                                    </div>
                                </span>
                            </div><!-- /input-group image-preview [TO HERE]--> 
                        </div>

                        <button type="submit" class="btn btn-primary pull-right">Daftar</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-nd-2"></div>
    </div>
</div>
@endsection

@section('js')
    <script>
$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});
    </script>
@endsection