@extends('layouts.visitor')

@section('content')
@php
use Config\Kholis as Helper;
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Dokumen</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach( $dokumen as $item)

                            <div class="col-md-4">
                                <div class="box box-bordered">
                                    <div class="box-header">
                                        <div class="row">
                                            <div class="col-md-6 d-none d-sm-block">
                                                <img src="{{ asset('/public/images/pdf.png') }}">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="box-category">
                                                    <h5>{{ $item->nama_dokumen }}</h5>
                                                    <i class="material-icons">alarm</i> {{ Helper::tanggal( $item->tanggal_dokumen ) }} <br>
                                                    <i class="material-icons">info</i> {{ $item->keterangan_dokumen }} <br>
                                                    <a href="{{ asset('/public/files/dokumen') . '/' . $item->dokumen }}" class="btn btn-primary">Download</a>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection