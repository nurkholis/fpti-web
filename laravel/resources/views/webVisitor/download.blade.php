@extends('layouts.visitor')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <br>
            <br>
            <br>
            <br>
            <h3 class="text-primary text-center" style="">Download disini !</h3>
            <br>
            <p class=" text-center"> 
            	<a href="{{ $url }}" class="btn btn-primary" download>Download</a>
            <br>
            <br>
            <br>
            <br>
            <br>
            </p>

        </div>
    </div>
</div>
@endsection