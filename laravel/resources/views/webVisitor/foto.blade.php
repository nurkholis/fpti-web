@extends('layouts.visitor')

@section('css')

<link href="{{ asset('public/assets/css/photoviewer.css') }}" rel="stylesheet">
<style>
    img {
        cursor: zoom-in;
    }
</style>
@endsection

@section('content')
<div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
        </div>
      </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Foto</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach( $foto as $item )
                            <div class="col-md-3">
                                <div class="box box-bordered">
                                    <div class="box-header">
                                        <div data-caption="{{ $item->judul_foto }}" data-gallery="manual" href="{{ asset('/public/images/foto').'/'.$item->foto }}">
                                            <img src="{{ asset('/public/images/foto').'/'.$item->foto }}" data-gallery="manual" alt="" class="img-preview">
                                            <h6 class="card-category text-gray">{{ $item->judul_foto }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="pull-righ">
                        {{ $foto->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script src="{{ asset('public/assets/js/photoviewer.js') }}"></script>
<script>
    $('[data-gallery=manual]').click(function (e) {

        e.preventDefault();

        var items = [],
        // get index of element clicked
        options = {
            index: $(this).index()
        };

        // looping to create images array
        $('[data-gallery=manual]').each(function () {
        let src = $(this).attr('href');
        items.push({
            src: src
        });
        });

        new PhotoViewer(items, options);

    });
    
</script>
@endsection