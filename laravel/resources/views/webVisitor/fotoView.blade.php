@extends('layouts.visitor')

@section('content')
@php
use Config\Kholis as Helper;
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            
                <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                    <h4 class="card-title"><i class="material-icons">photo_library</i> FPTI FOTO </h4>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-3"></div>

                                    <div class="col-md-6">
                                        <div class="box box-bordered">
                                            <div class="box-header">
                                                
                                                <div>
                                                    <div class="zoomeffect">
                                                        <img class src="{{ asset('/public/images/foto').'/'.$data->foto }}" data-gallery="manual" alt="" class="img-preview">
                                                        <br>
                                                    </div>
                                                    <i style="font-size: 10px" class="material-icons">date_range</i>   {{ Helper::tanggal( $data->tanggal_foto ) }}
                                                    <h6 class="card-category text-gray">
                                                        {{ $data->judul_foto }}
                                                    </h6>
                                                    <p>
                                                        {{ $data->keterangan_foto }}
                                                    </p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3"></div>
                                    
                                </div>
                                    <div class="box-category">	
                                    {{-- <h6 class="card-category text-blue"><a href="{{ route('visitor.foto.index') }}" class="pull-right">Next Foto<i class="material-icons" class="pull-right" >arrow_forward_ios</i></a></h6>	 --}}
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
</div>
@endsection