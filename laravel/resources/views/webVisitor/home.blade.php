@extends('layouts.visitor')

@section('content')
@php
use Config\Kholis as Helper;
@endphp

{{-- berita --}}

{{-- <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title"><i class="material-icons">photo_library</i> FPTI BERITA </h4>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="box box-bordered">
                                <div class="box-header">
                                    
                                        <div>
                                            <div class="zoomeffect">
                                            <img class src="http://static.fpti.or.id/img/2018/11/siap-berlaga-di-asian-championship-2018-japan-520x292.jpeg" data-gallery="manual" alt="" class="img-preview">
                                            <br>
                                            <a href="{{ route('visitor.berita.index') }}">
                                                <h6 class="card-category text-gray">
                                                    11 Atlet Panjat Tebing Siap Berlaga di Asian Championship 2018 di Jepang
                                                </h6>
                                            </a>
                                            <div class="box-category">	
                                                    TOTTORI–Federasi Panjat Tebing Indonesia (FPTI) mengirimkan 11 atlet andalan untuk mengikuti kejuaraan panjat tebing Asian Championships 2018 di Kurayoshi, Tottori, Jepang, 7-11 November. Kesebelas atlet tersebut terdiri dari enam atlet putra dan lima atlet putri.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
                    </div>
						<div class="box-category">	

					</div>
                </div>
            </div>
        </div>
    </div>
</div> --}}


<!--------------INFO------->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title"><i class="material-icons">library_books</i> INFO FPTI</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach( $info as $item)
                            <div class="col-md-6">
                                <div class="box box-bordered">
                                    <div class="box-header">
                                        <h4 class="">{{ $item->judul_info }}</h4>
                                        <div class="box-category">
                                            
                                            <i class="material-icons">date_range</i>   {{ Helper::tanggal( $item->  tanggal_info ) }}
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <p class="card-description">{{ $item->info }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
					<div class="box-category">	
						<h6 class="card-category text-blue"><a href="{{ route('visitor.info.index') }}" class="pull-right">Next Info<i class="material-icons" class="pull-right" >navigate_next</i></a></h6>
					</div>					
                </div>
            </div>
        </div>
    </div>
</div>


<!--------------video------->



<div class="container">
    <div class="row">
        <div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-primary">
					<h4 class="card-title"><i class="material-icons">video_library</i> FPTI VIDEO</h4>
                </div>
					<div class="card-body">
					<div class="row">
                    @foreach( $video as $item )
						<div class="col-md-3">
							  <div class="box box-bordered">
								<div class="box-header">
									<iframe src="https://www.youtube.com/embed/{{ $item->video }}">
									</iframe>
										<h6 class="card-category text-gray">{{  $item->judul_video}}</h6>
										<div class="box-category">
										<i class="material-icons">date_range</i>   {{ Helper::tanggal( $item->  tanggal_video ) }}
										</div>
								</div>	
							</div>
						</div>
                    @endforeach

					</div>
					<div class="box-category">	
						<h6 class="card-category text-blue"><a href="{{ route('visitor.video.index') }}" class="pull-right">Next Video<i class="material-icons" class="pull-right" >navigate_next</i></a></h6>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

<!--------------foto------->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title"><i class="material-icons">photo_library</i> FPTI FOTO </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach( $foto as $item )
                            <div class="col-md-3">
                                <div class="box box-bordered">
                                    <div class="box-header">
										
											<div data-caption="{{ $item->judul_foto }}" data-gallery="manual" href="{{ asset('/public/images/foto').'/'.$item->foto }}">
												<div class="zoomeffect">
												<img class src="{{ asset('/public/images/foto').'/'.$item->foto }}" data-gallery="manual" alt="" class="img-preview">
                                                
                                                <br>
                                                <a href="{{ route('visitor.foto.view', $item->id_foto) }}"><h6 class="card-category text-gray">{{ $item->judul_foto }}</h6></a>
												<div class="box-category">	
                                                    <i class="material-icons">date_range</i>   {{ Helper::tanggal( $item->tanggal_foto ) }}
                                                    <br>
                                                    <p>
                                                        {{ implode(' ', array_slice(explode(' ', $item->keterangan_foto), 0, 10))  }}...
                                                    </p>
												</div>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
						
                </div>
						<div class="box-category">	
						<h6 class="card-category text-blue"><a href="{{ route('visitor.foto.index') }}" class="pull-right">Next Foto<i class="material-icons" class="pull-right" >arrow_forward_ios</i></a></h6>	
					</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            {{-- <ol class="carousel-indicators">
                                @foreach ($foto as $item)
                                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->iteration - 1 }}" class="@if ( $loop->iteration == 1 ) active  @endif"></li>
                                @endforeach
                            </ol> --}}
                            <div class="carousel-inner">
                                @for ($i = 0; $i < count($club); $i++)
                                    @if ( $i%4 == 0 )
                                        
                                        <div class="carousel-item @if ( $i == 0 ) active  @endif">
                                                <img class="carousel-img-circel" src="{{ asset('/public/images/club').'/'.$club[$i]->logo_club }}">
                                                @if ($i + 1 < count($club) )
                                                    <img class="carousel-img-circel" src="{{ asset('/public/images/club').'/'.$club[$i + 1]->logo_club }}">
                                                @endif
                                                @if ($i + 2 < count($club) )
                                                    <img class="carousel-img-circel" src="{{ asset('/public/images/club').'/'.$club[$i + 2]->logo_club }}">
                                                @endif
                                                @if ($i + 3 < count($club) )
                                                    <img class="carousel-img-circel" src="{{ asset('/public/images/club').'/'.$club[$i + 3]->logo_club }}">
                                                @endif
                                        </div>
                                    @endif
                                @endfor
                                    
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>
		                   
									
                  
                  


<!-------


<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Foto</h4>
                </div>
                <div class="card-body">
                    @foreach( $foto as $item)
                        <div class="box">
                            <div class="box-header">
                                <img src="{{ asset('/public/images/foto').'/'.$item->foto }}" alt="" class="img-preview">
                                <h6 class="card-category text-gray">{{ $item->judul_foto }}</h6>
                            </div>
                        </div>
                    @endforeach
                    <div class="d-flex justify-content-center">
                    <a href="#pablo" class="btn btn-primary btn-round pull-right">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach ($foto as $item)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->iteration - 1 }}" class="@if ( $loop->iteration == 1 ) active  @endif"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach ($foto as $item)
                            <div class="carousel-item @if ( $loop->iteration == 1 ) active  @endif">
                                <img class="d-block w-100" src="{{ asset('/public/images/foto').'/'.$item->foto }}">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>{{ $item->judul_foto }}</h5>
                                    <p>{{ $item->keterangan_foto }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Informasi</h4>
                </div>
                <div class="card-body">
                    @foreach( $info as $item)
                        <div class="box">
                            <div class="box-header">
                                <h4 class="">{{ $item->judul_info }}</h4>
                                <div class="box-category">
                                    <i class="material-icons">person</i> {{ $item->nama_admin }} |
                                    <i class="material-icons">alarm</i>{{ Helper::tanggal( $item->tanggal_info ) }}
                                </div>
                            </div>
                            <div class="box-content">
                                <p class="card-description">{{ $item->info  }}</p>
                            </div>
                        </div>
                    @endforeach
                    <div class="d-flex justify-content-center">
                    <a href="#pablo" class="btn btn-primary btn-round pull-right">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Video</h4>
                </div>
                <div class="card-body">
                    @foreach( $video as $item )
                        <div class="box">
                            <div class="box-header">
                                <iframe src="https://www.youtube.com/embed/{{ $item->video }}">
                                </iframe>
                                <h6 class="card-category text-gray">{{  $item->judul_video}}</h6>
                            </div>
                        </div>
                    @endforeach
                    <div class="d-flex justify-content-center">
                    <a href="#pablo" class="btn btn-primary btn-round pull-right">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

--------->

@endsection