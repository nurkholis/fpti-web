@extends('layouts.visitor')

@section('content')
@php
use Config\Kholis as Helper;
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Info</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach( $info as $item)
                            <div class="col-md-6">
                                <div class="box box-bordered">
                                    <div class="box-header">
                                        <h4 class="">{{ $item->judul_info }}</h4>
                                        <div class="box-category">
                                            <i class="material-icons">person</i> {{ $item->nama_admin }} |
                                            <i class="material-icons">alarm</i>{{ Helper::tanggal( $item->tanggal_info ) }}
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <p class="card-description">{{ $item->info }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="pull-right ">
                        {{ $info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection