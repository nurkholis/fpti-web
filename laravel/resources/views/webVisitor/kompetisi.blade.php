@extends('layouts.visitor')

@section('content')
@php
use Config\Kholis as Helper;
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Kompetisi</h4>
                </div>
                <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <tr>
                                <th>Nama Kompetisi</th>
                                <th>Tanggal</th>
                                <th>Keterangan</th>
                                <th>Dokumen</th>
                                <th>Laporan</th>
                                <th>Hasil</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $kompetisi as $item )
                                <tr>
                                    <td>{{ $item->nama_kompetisi }}</td>
                                    <td>{{ Helper::tanggal( $item->tanggal_kompetisi ) }}</td>
                                    <td>{{ $item->keterangan_kompetisi }}</td>
                                    <td>
                                        
                                        <a href="{{ asset('/public/files/dokumen_kompetisi') . '/' . $item->dokumen_kompetisi }}" class="text-primary"><i class="material-icons">cloud_download</i></a>
                                    </td>
                                    <td>
                                        <a href="{{ asset('/public/files/laporan_kompetisi') . '/' . $item->laporan_kompetisi }}" class="text-primary"><i class="material-icons">cloud_download</i></a>
                                    </td>
                                    <td>
                                        <a href="{{ asset('/public/files/hasil_kompetisi') . '/' . $item->hasil_kompetisi }}" class="text-primary"><i class="material-icons">cloud_download</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{ $kompetisi->links() }}
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection