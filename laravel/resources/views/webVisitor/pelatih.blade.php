@extends('layouts.visitor')

@section('content')
@php
use Config\Kholis as Helper;
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Pelatih</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach( $pelatih as $item)

                            <div class="col-md-4">
                                <div class="box box-bordered">
                                    <div class="box-header">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img src="{{ asset('/public/images/pelatih').'/'.$item->foto_pelatih }}">
                                            </div>
                                            <div class="col-md-6">
                                                <h5 class="">{{ $item->nama_pelatih }}</h5>
                                                <div class="box-category">
                                                    <i class="material-icons">email</i> {{ $item->email }} <br>
                                                    <i class="material-icons">place</i> {{ $item->alamat_pelatih }} <br>
                                                    <i class="material-icons">assignment_turned_in</i> {{ $item->sertifikasi_pelatih }} <br>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection