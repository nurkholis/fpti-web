@extends('layouts.visitor')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Prestasi</h4>
                </div>
                <div class="card-body">
                <div class="table-responsive" >
                    <table id="table" class="table table-bordered" style="width:100%">
                        <thead class="text-primary">
                            <tr>
                                <th>Nama Atlet</th>
                                <th>Club</th>
                                <th>Kategori</th>
                                <th>Peringkat</th>
                                <th>Tingkat</th>
                                <th>Tahun</th>
                                <th>Tempat</th>
                            </tr>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('visitor.prestasi.data') }}",
            columns: [
                {data: 'atlet_nama', name: 'atlet_nama'},
                {data: 'nama_club', name: 'nama_club'},
                {data: 'kategori', name: 'kategori'},
                {data: 'tingkat', name: 'tingkat'},
                {data: 'peringkat', name: 'peringkat'},
                {data: 'tahun', name: 'tahun'},
                {data: 'tempat', name: 'tempat'},
            ]
        });
    </script>
@endsection