@extends('layouts.visitor')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Sarana dan Prasaranan</h4>
                </div>
                <div class="card-body">
                <div class="table-responsive" >
                    <table id="table" class="table table-bordered" style="width:100%">
                        <thead class="text-primary">
                            <tr>
                                <th>Item</th>
                                <th>Volume</th>
                                <th>Satuan</th>
                                <th>Tahun</th>
                                <th>Keterangan</th>
                                <th>Kode Barang</th>
                                <th>Pemilik</th>
                            </tr>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('visitor.sarpras.data') }}",
            columns: [
                {data: 'items', name: 'items'},
                {data: 'volume', name: 'volume'},
                {data: 'satuan', name: 'satuan'},
                {data: 'tahun_perolehan', name: 'tahun_perolehan'},
                {data: 'keterangan_sarpras', name: 'keterangan_sarpras'},
                {data: 'kode_barang', name: 'kode_barang'},
                {data: 'pemilik', name: 'pemilik'},
            ]
        });
    </script>
@endsection