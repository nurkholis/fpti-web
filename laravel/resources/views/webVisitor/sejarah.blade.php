@extends('layouts.visitor')

@section('content')
@php
use Config\Kholis as Helper;
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Sejarah</h4>
                </div>
                <div class="card-body">
                    @foreach( $sejarah as $item)
                        <div class="box">
                            <div class="box-header">
                                <h4 class="">{!! $item->judul_sejarah !!}</h4>
                            </div>
                            <div class="box-content">
                                <p class="card-description">{!! $item->isi_sejarah !!}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection