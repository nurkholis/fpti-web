@extends('layouts.visitor')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Struktur</h4>
                </div>
                <div class="card-body">
                <div class="table-responsive" >
                    <table id="table" class="table table-bordered" style="width:100%">
                        <thead class="text-primary">
                            <tr>
                                <th>Jabatan</th>
                                <th>Nama</th>
                                <th>Telepon</th>
                                <th>Alamat</th>
                            </tr>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('visitor.struktur.data') }}",
            columns: [
                {data: 'nama_jabatan', name: 'nama_jabatan'},
                {data: 'struktur_nama', name: 'struktur_nama'},
                {data: 'struktur_telepon', name: 'struktur_telepon'},
                {data: 'struktur_alamat', name: 'struktur_alamat'},
            ]
        });
    </script>
@endsection