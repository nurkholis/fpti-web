@extends('layouts.visitor')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                <h4 class="card-title">Video</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach( $video as $item )
							<div class="col-md-3">
                                <div class="box box-bordered">
                                    <div class="box-header">
                                       
                                <iframe src="https://www.youtube.com/embed/{{ $item->video }}">
                                </iframe>
                                        <h6 class="card-category text-gray">{{  $item->judul_video}}</h6>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="pull-righ">
                        {{ $video->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection