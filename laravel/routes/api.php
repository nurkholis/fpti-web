<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('pelatih')->group( function() {
Route::get('/', 'Api\pelatihController@get');
});
Route::prefix('foto')->group( function() {
    Route::get('/', 'Api\FotoController@get');
});
Route::prefix('video')->group( function() {
    Route::get('/', 'Api\VideoController@get');
});
Route::prefix('info')->group( function() {
    Route::get('/', 'Api\InfoController@get');
});
Route::prefix('seleksi')->group( function() {
    Route::get('/', 'Api\SeleksiController@get');
});
Route::prefix('atlet')->group( function() {
    Route::get('/', 'Api\AtletController@get');
});
Route::prefix('anggota')->group( function() {
    Route::post('/login', 'Api\AnggotaController@login');
    Route::post('/', 'Api\AnggotaController@post');
    Route::post('/pw/reset', 'Api\AnggotaController@pwReset');
    Route::post('/edit', 'Api\AnggotaController@edit')->middleware('auth:api-anggota');
    Route::get('/profil', 'Api\AnggotaController@profil')->middleware('auth:api-anggota');
    Route::get('/', 'Api\AnggotaController@get');
});
Route::prefix('sejarah')->group( function() {
    Route::get('/', 'Api\SejarahController@get');
});
Route::prefix('struktur')->group( function() {
    Route::get('/', 'Api\StrukturController@get');
});
Route::prefix('sarpras')->group( function() {
    Route::get('/', 'Api\SarprasController@get');
});
Route::prefix('dokumen')->group( function() {
    Route::get('/', 'Api\DokumenController@get');
});
Route::prefix('kompetisi')->group( function() {
    Route::get('/', 'Api\KompetisiController@get');
});
Route::prefix('atlet')->group( function() {
    Route::get('/', 'Api\AtletController@get');
});
Route::prefix('prestasi')->group( function() {
    Route::get('/', 'Api\PrestasiController@get');
});

