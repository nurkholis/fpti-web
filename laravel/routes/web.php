<?php

Route::prefix('/')->group( function() {
    Route::get('/download/{url}', 'WebVisitor\DownloadController@index')->name('visitor.download.index');
    Route::get('/', 'WebVisitor\HomeController@index')->name('visitor.visitor.index');
    Route::get('/sejarah', 'WebVisitor\HomeController@sejarah')->name('visitor.sejarah.index');
    Route::get('/info', 'WebVisitor\HomeController@info')->name('visitor.info.index');
    Route::get('/foto', 'WebVisitor\HomeController@foto')->name('visitor.foto.index');
    Route::get('/foto/{id_foto}', 'WebVisitor\HomeController@fotoView')->name('visitor.foto.view');
    Route::get('/video', 'WebVisitor\HomeController@video')->name('visitor.video.index');
    Route::get('/daftar', function () { return view('webVisitor.daftar'); })->name('visitor.daftar.index');
    Route::post('/daftar/form', 'WebVisitor\AnggotaController@store')->name('visitor.daftar.store');
    Route::get('/404', function () { return view('webVisitor.404'); })->name('visitor.404.index');
    Route::get('/visitor/pelatih', 'WebVisitor\HomeController@pelatih')->name('visitor.pelatih.index');
    Route::get('/dokumen', 'WebVisitor\HomeController@dokumen')->name('visitor.dokumen.index');
    Route::get('/prestasi', function () { return view('webVisitor.prestasi'); })->name('visitor.prestasi.index');
    Route::get('/prestasi/data', 'WebVisitor\HomeController@prestasiData')->name('visitor.prestasi.data');
    Route::get('/struktur', function () { return view('webVisitor.struktur'); })->name('visitor.struktur.index');
    Route::get('/struktur/data', 'WebVisitor\HomeController@strukturData')->name('visitor.struktur.data');
    Route::get('/sarpras', function () { return view('webVisitor.sarpras'); })->name('visitor.sarpras.index');
    Route::get('/sarpras/data', 'WebVisitor\HomeController@sarprasData')->name('visitor.sarpras.data');
    Route::get('/dokumen', 'WebVisitor\HomeController@dokumen')->name('visitor.dokumen.index');
    Route::get('/kompetisi', 'WebVisitor\HomeController@kompetisi')->name('visitor.kompetisi.index');
    Route::get('/kompetisi/data', 'WebVisitor\HomeController@kompetisiData')->name('visitor.kompetisi.data');
    Route::get('/visitor/atlet', function () { return view('webVisitor.atlet'); })->name('visitor.atlet.index');
    Route::get('/visitor/atlet/data', 'WebVisitor\HomeController@atletData')->name('visitor.atlet.data');
    Route::get('/visitor/atlet/{id_atlet}', 'WebVisitor\HomeController@atletProfil')->name('visitor.atlet.profil');
});

Route::prefix('/admin')->group( function() {

    Route::get('', 'WebAdmin\HomeController@index')->name('admin.home');
    Route::get('login', 'AuthAdmin\LoginController@showloginForm')->name('admin.login');
    Route::post('login', 'AuthAdmin\LoginController@login')->name('admin.login.submit');
    Route::get('logout', function(){
        Auth::logout();
        Session::flush();
        return redirect(route('admin.login'));
    })->name('admin.logout'); 

    Route::prefix('profil')->group( function() {
        Route::get('/', 'WebAdmin\AdminController@index')->name('admin.admin.index');
        Route::put('/', 'WebAdmin\AdminController@update')->name('admin.admin.put');
        Route::put('/pw', 'WebAdmin\AdminController@updatePw')->name('admin.admin.putPw');
    });
    
    Route::prefix('pelatih')->group( function() {
        Route::get('/', 'WebAdmin\PelatihController@index')->name('admin.pelatih.index');
        Route::get('/form', 'WebAdmin\pelatihController@create')->name('admin.pelatih.create');
        Route::get('/reset/{id}', 'WebAdmin\PelatihController@reset')->name('admin.pelatih.reset');
        Route::get('/form/{id}', 'WebAdmin\PelatihController@edit')->name('admin.pelatih.edit');
        Route::post('/', 'WebAdmin\PelatihController@store')->name('admin.pelatih.store');
        Route::get('/delete/{id}', 'WebAdmin\PelatihController@destroy')->name('admin.pelatih.delete');
        Route::put('/{id}', 'WebAdmin\PelatihController@update')->name('admin.pelatih.put');
        Route::get('/export', 'WebAdmin\PelatihController@export')->name('admin.pelatih.export');
    });

    Route::prefix('sejarah')->group( function() {
        Route::get('/', 'WebAdmin\SejarahController@index')->name('admin.sejarah.index');
        Route::get('/form', 'WebAdmin\SejarahController@create')->name('admin.sejarah.create');
        Route::get('/form/{id}', 'WebAdmin\SejarahController@edit')->name('admin.sejarah.edit');
        Route::post('/', 'WebAdmin\SejarahController@store')->name('admin.sejarah.store');
        Route::get('/delete/{id}', 'WebAdmin\SejarahController@destroy')->name('admin.sejarah.delete');
        Route::put('/{id}', 'WebAdmin\SejarahController@update')->name('admin.sejarah.put');
    });

    Route::prefix('jabatan')->group( function() {
        Route::get('/', 'WebAdmin\JabatanController@index')->name('admin.jabatan.index');
        Route::get('/form', 'WebAdmin\JabatanController@create')->name('admin.jabatan.create');
        Route::get('/form/{id}', 'WebAdmin\JabatanController@edit')->name('admin.jabatan.edit');
        Route::post('/', 'WebAdmin\JabatanController@store')->name('admin.jabatan.store');
        Route::get('/delete/{id}', 'WebAdmin\JabatanController@destroy')->name('admin.jabatan.delete');
        Route::put('/{id}', 'WebAdmin\JabatanController@update')->name('admin.jabatan.put');
    });

    Route::prefix('struktur')->group( function() {
        Route::get('/', 'WebAdmin\StrukturController@index')->name('admin.struktur.index');
        Route::get('/form', 'WebAdmin\StrukturController@create')->name('admin.struktur.create');
        Route::get('/form/{id}', 'WebAdmin\StrukturController@edit')->name('admin.struktur.edit');
        Route::post('/', 'WebAdmin\StrukturController@store')->name('admin.struktur.store');
        Route::get('/delete/{id}', 'WebAdmin\StrukturController@destroy')->name('admin.struktur.delete');
        Route::put('/{id}', 'WebAdmin\StrukturController@update')->name('admin.struktur.put');
    });

    Route::prefix('sarpras')->group( function() {
        Route::get('/', 'WebAdmin\SarprasController@index')->name('admin.sarpras.index');
        Route::get('/form', 'WebAdmin\SarprasController@create')->name('admin.sarpras.create');
        Route::get('/form/{id}', 'WebAdmin\SarprasController@edit')->name('admin.sarpras.edit');
        Route::post('/', 'WebAdmin\SarprasController@store')->name('admin.sarpras.store');
        Route::get('/delete/{id}', 'WebAdmin\SarprasController@destroy')->name('admin.sarpras.delete');
        Route::put('/{id}', 'WebAdmin\SarprasController@update')->name('admin.sarpras.put');
    });

    Route::prefix('dokumen')->group( function() {
        Route::get('/', 'WebAdmin\DokumenController@index')->name('admin.dokumen.index');
        Route::get('/form', 'WebAdmin\DokumenController@create')->name('admin.dokumen.create');
        Route::get('/form/{id}', 'WebAdmin\DokumenController@edit')->name('admin.dokumen.edit');
        Route::post('/', 'WebAdmin\DokumenController@store')->name('admin.dokumen.store');
        Route::get('/delete/{id}', 'WebAdmin\DokumenController@destroy')->name('admin.dokumen.delete');
        Route::put('/{id}', 'WebAdmin\DokumenController@update')->name('admin.dokumen.put');
    });

    Route::prefix('info')->group( function() {
        Route::get('/', 'WebAdmin\InfoController@index')->name('admin.info.index');
        Route::get('/form', 'WebAdmin\InfoController@create')->name('admin.info.create');
        Route::get('/form/{id}', 'WebAdmin\InfoController@edit')->name('admin.info.edit');
        Route::post('/', 'WebAdmin\InfoController@store')->name('admin.info.store');
        Route::get('/delete/{id}', 'WebAdmin\InfoController@destroy')->name('admin.info.delete');
        Route::put('/{id}', 'WebAdmin\InfoController@update')->name('admin.info.put');
    });

    Route::prefix('foto')->group( function() {
        Route::get('/', 'WebAdmin\FotoController@index')->name('admin.foto.index');
        Route::get('/form', 'WebAdmin\FotoController@create')->name('admin.foto.create');
        Route::get('/form/{id}', 'WebAdmin\FotoController@edit')->name('admin.foto.edit');
        Route::post('/', 'WebAdmin\FotoController@store')->name('admin.foto.store');
        Route::get('/delete/{id}', 'WebAdmin\FotoController@destroy')->name('admin.foto.delete');
        Route::put('/{id}', 'WebAdmin\FotoController@update')->name('admin.foto.put');
    });

    Route::prefix('video')->group( function() {
        Route::get('/', 'WebAdmin\VideoController@index')->name('admin.video.index');
        Route::get('/form', 'WebAdmin\VideoController@create')->name('admin.video.create');
        Route::get('/form/{id}', 'WebAdmin\VideoController@edit')->name('admin.video.edit');
        Route::post('/', 'WebAdmin\VideoController@store')->name('admin.video.store');
        Route::get('/delete/{id}', 'WebAdmin\VideoController@destroy')->name('admin.video.delete');
        Route::put('/{id}', 'WebAdmin\VideoController@update')->name('admin.video.put');
    });

    Route::prefix('seleksi')->group( function() {
        Route::get('/', 'WebAdmin\SeleksiController@index')->name('admin.seleksi.index');
        Route::get('/form', 'WebAdmin\SeleksiController@create')->name('admin.seleksi.create');
        Route::get('/export/{tahu_seleksi}', 'WebAdmin\SeleksiController@export')->name('admin.seleksi.export');
        Route::get('/form/{id}', 'WebAdmin\SeleksiController@edit')->name('admin.seleksi.edit');
        Route::get('/verifikasi/{id_atlet}/{status}', 'WebAdmin\SeleksiController@verifikasi')->name('admin.seleksi.verifikasi');
        Route::post('/', 'WebAdmin\SeleksiController@store')->name('admin.seleksi.store');
        Route::get('/delete/{id}', 'WebAdmin\SeleksiController@destroy')->name('admin.seleksi.delete');
        Route::put('/{id}', 'WebAdmin\SeleksiController@update')->name('admin.seleksi.put');
    });

    Route::prefix('tahunSeleksi')->group( function() {
        Route::get('/', 'WebAdmin\TahunSeleksiController@index')->name('admin.tahunSeleksi.index');
        Route::get('/form', 'WebAdmin\TahunSeleksiController@create')->name('admin.tahunSeleksi.create');
        Route::get('/form/{id}', 'WebAdmin\TahunSeleksiController@edit')->name('admin.tahunSeleksi.edit');
        Route::post('/', 'WebAdmin\TahunSeleksiController@store')->name('admin.tahunSeleksi.store');
        Route::get('/delete/{id}', 'WebAdmin\TahunSeleksiController@destroy')->name('admin.tahunSeleksi.delete');
        Route::put('/{id}', 'WebAdmin\TahunSeleksiController@update')->name('admin.tahunSeleksi.put');
    });

    Route::prefix('kompetisi')->group( function() {
        Route::get('/', 'WebAdmin\KompetisiController@index')->name('admin.kompetisi.index');
        Route::get('/form', 'WebAdmin\KompetisiController@create')->name('admin.kompetisi.create');
        Route::get('/form/{id}', 'WebAdmin\KompetisiController@edit')->name('admin.kompetisi.edit');
        Route::post('/', 'WebAdmin\KompetisiController@store')->name('admin.kompetisi.store');
        Route::get('/delete/{id}', 'WebAdmin\KompetisiController@destroy')->name('admin.kompetisi.delete');
        Route::put('/{id}', 'WebAdmin\KompetisiController@update')->name('admin.kompetisi.put');
    });

    Route::prefix('admin/club')->group( function() {
        Route::get('/', 'WebAdmin\ClubController@index')->name('admin.club.index');
        Route::get('/form', 'WebAdmin\ClubController@create')->name('admin.club.create');
        Route::get('/form/{id}', 'WebAdmin\ClubController@edit')->name('admin.club.edit');
        Route::post('/', 'WebAdmin\ClubController@store')->name('admin.club.store');
        Route::get('/delete/{id}', 'WebAdmin\ClubController@destroy')->name('admin.club.delete');
        Route::put('/{id}', 'WebAdmin\ClubController@update')->name('admin.club.put');
    });

    Route::prefix('adminclub')->group( function() {
        Route::get('/', 'WebAdmin\AdminClubController@index')->name('admin.adminClub.index');
        Route::get('/form', 'WebAdmin\AdminClubController@create')->name('admin.adminClub.create');
        Route::get('/reset/{id}', 'WebAdmin\AdminClubController@reset')->name('admin.adminClub.reset');
        Route::get('/form/{id}', 'WebAdmin\AdminClubController@edit')->name('admin.adminClub.edit');
        Route::post('/', 'WebAdmin\AdminClubController@store')->name('admin.adminClub.store');
        Route::get('/delete/{id}', 'WebAdmin\AdminClubController@destroy')->name('admin.adminClub.delete');
        Route::put('/{id}', 'WebAdmin\AdminClubController@update')->name('admin.adminClub.put');
    });

    Route::prefix('prestasi')->group( function() {
        Route::get('/', 'WebAdmin\PrestasiController@index')->name('admin.prestasi.index');
        Route::get('/form', 'WebAdmin\PrestasiController@create')->name('admin.prestasi.create');
        Route::get('/form/{id}', 'WebAdmin\PrestasiController@edit')->name('admin.prestasi.edit');
        Route::post('/', 'WebAdmin\PrestasiController@store')->name('admin.prestasi.store');
        Route::get('/delete/{id}', 'WebAdmin\PrestasiController@destroy')->name('admin.prestasi.delete');
        Route::put('/{id}', 'WebAdmin\PrestasiController@update')->name('admin.prestasi.put');
    });

    Route::prefix('anggota')->group( function() {
        Route::get('/', 'WebAdmin\AnggotaController@index')->name('admin.anggota.index');
        Route::get('/delete/{id}', 'WebAdmin\AnggotaController@destroy')->name('admin.anggota.delete');
    });

    Route::prefix('atlet')->group( function() {
        Route::get('/', 'WebAdmin\AtletController@index')->name('admin.atlet.index');
        Route::get('/form', 'WebAdmin\AtletController@create')->name('admin.atlet.create');
        Route::get('/form/{id}', 'WebAdmin\AtletController@edit')->name('admin.atlet.edit');
        Route::post('/', 'WebAdmin\AtletController@store')->name('admin.atlet.store');
        Route::get('/delete/{id}', 'WebAdmin\AtletController@destroy')->name('admin.atlet.delete');
        Route::put('/{id}', 'WebAdmin\AtletController@update')->name('admin.atlet.put');
        Route::get('/export', 'WebAdmin\AtletController@export')->name('admin.atlet.export');
    });

});

Route::prefix('/pelatih')->group( function() {

    Route::get('', 'WebPelatih\HomeController@index')->name('pelatih.home');
    Route::get('login', 'AuthPelatih\LoginController@showloginForm')->name('pelatih.login');
    Route::post('login', 'AuthPelatih\LoginController@login')->name('pelatih.login.submit');
    Route::get('logout', function(){
        Auth::logout();
        Session::flush();
        return redirect(route('pelatih.login'));
    })->name('pelatih.logout'); 

    Route::prefix('profil')->group( function() {
        Route::get('/', 'WebPelatih\PelatihController@index')->name('pelatih.pelatih.index');
        Route::put('/', 'WebPelatih\PelatihController@update')->name('pelatih.pelatih.put');
        Route::put('/pw', 'WebPelatih\PelatihController@updatePw')->name('pelatih.pelatih.putPw');
    });

    Route::prefix('info')->group( function() {
        Route::get('/', 'WebPelatih\InfoController@index')->name('pelatih.info.index');
        Route::get('/form', 'WebPelatih\InfoController@create')->name('pelatih.info.create');
        Route::get('/form/{id}', 'WebPelatih\InfoController@edit')->name('pelatih.info.edit');
        Route::post('/', 'WebPelatih\InfoController@store')->name('pelatih.info.store');
        Route::get('/delete/{id}', 'WebPelatih\InfoController@destroy')->name('pelatih.info.delete');
        Route::put('/{id}', 'WebPelatih\InfoController@update')->name('pelatih.info.put');
    });

    Route::prefix('foto')->group( function() {
        Route::get('/', 'WebPelatih\FotoController@index')->name('pelatih.foto.index');
        Route::get('/form', 'WebPelatih\FotoController@create')->name('pelatih.foto.create');
        Route::get('/form/{id}', 'WebPelatih\FotoController@edit')->name('pelatih.foto.edit');
        Route::post('/', 'WebPelatih\FotoController@store')->name('pelatih.foto.store');
        Route::get('/delete/{id}', 'WebPelatih\FotoController@destroy')->name('pelatih.foto.delete');
        Route::put('/{id}', 'WebPelatih\FotoController@update')->name('pelatih.foto.put');
    });

    Route::prefix('video')->group( function() {
        Route::get('/', 'WebPelatih\VideoController@index')->name('pelatih.video.index');
        Route::get('/form', 'WebPelatih\VideoController@create')->name('pelatih.video.create');
        Route::get('/form/{id}', 'WebPelatih\VideoController@edit')->name('pelatih.video.edit');
        Route::post('/', 'WebPelatih\VideoController@store')->name('pelatih.video.store');
        Route::get('/delete/{id}', 'WebPelatih\VideoController@destroy')->name('pelatih.video.delete');
        Route::put('/{id}', 'WebPelatih\VideoController@update')->name('pelatih.video.put');
    });

    Route::prefix('kompetisi')->group( function() {
        Route::get('/', 'WebPelatih\KompetisiController@index')->name('pelatih.kompetisi.index');
        Route::get('/form', 'WebPelatih\KompetisiController@create')->name('pelatih.kompetisi.create');
        Route::get('/form/{id}', 'WebPelatih\KompetisiController@edit')->name('pelatih.kompetisi.edit');
        Route::post('/', 'WebPelatih\KompetisiController@store')->name('pelatih.kompetisi.store');
        Route::get('/delete/{id}', 'WebPelatih\KompetisiController@destroy')->name('pelatih.kompetisi.delete');
        Route::put('/{id}', 'WebPelatih\KompetisiController@update')->name('pelatih.kompetisi.put');
    });

    Route::prefix('nilai')->group( function() {
        Route::get('/', 'WebPelatih\NilaiController@index')->name('pelatih.nilai.index');
        Route::get('/lulus{id_seleksi}/{lulus}', 'WebPelatih\NilaiController@lulus')->name('pelatih.nilai.lulus');
        Route::get('/kirim{id_seleksi}/{kirim}', 'WebPelatih\NilaiController@kirim')->name('pelatih.nilai.kirim');
        Route::get('/seleksi/export/{tahun}', 'WebPelatih\NilaiController@exportSeleksi')->name('pelatih.nilai.seleksiExport');
        Route::get('/export/{tahun}', 'WebPelatih\NilaiController@export')->name('pelatih.nilai.export');
        Route::get('/data', 'WebPelatih\NilaiController@data')->name('pelatih.nilai.data');
        Route::post('/', 'WebPelatih\NilaiController@store')->name('pelatih.nilai.store');
    });

    Route::prefix('prestasi')->group( function() {
        Route::get('/', 'WebPelatih\PrestasiController@index')->name('pelatih.prestasi.index');
    });
    Route::prefix('atlet')->group( function() {
        Route::get('/', 'WebPelatih\AtletController@index')->name('pelatih.atlet.index');
        Route::get('/export', 'WebPelatih\AtletController@export')->name('pelatih.atlet.export');
    });

});

Route::prefix('/atlet')->group( function() {

    Route::get('', 'WebAtlet\HomeController@index')->name('atlet.home');
    Route::get('login', 'AuthAtlet\LoginController@showloginForm')->name('atlet.login');
    Route::post('login', 'AuthAtlet\LoginController@login')->name('atlet.login.submit');
    Route::get('logout', function(){
        Auth::logout();
        Session::flush();
        return redirect(route('atlet.login'));
    })->name('atlet.logout'); 

    Route::prefix('profil')->group( function() {
        Route::get('/', 'WebAtlet\AtletController@index')->name('atlet.atlet.index');
        Route::put('/', 'WebAtlet\AtletController@update')->name('atlet.atlet.put');
        Route::put('/pw', 'WebAtlet\AtletController@updatePw')->name('atlet.atlet.putPw');
    });

});

Route::prefix('/admin/club')->group( function() {

    Route::get('', 'WebAdminClub\HomeController@index')->name('adminClub.home');
    Route::get('login', 'AuthAdminClub\LoginController@showloginForm')->name('adminClub.login');
    Route::post('login', 'AuthAdminClub\LoginController@login')->name('adminClub.login.submit');
    Route::get('logout', function(){
        Auth::logout();
        Session::flush();
        return redirect(route('adminClub.login'));
    })->name('adminClub.logout'); 

    Route::prefix('profil')->group( function() {
        Route::get('/', 'WebAdminClub\AdminClubController@index')->name('adminClub.adminClub.index');
        Route::put('/profil', 'WebAdminClub\AdminClubController@update')->name('adminClub.adminClub.put');
        Route::put('/profil/pw', 'WebAdminClub\AdminClubController@updatePw')->name('adminClub.adminClub.putPw');
    });
    
    Route::prefix('club')->group( function() {
        Route::get('/', 'WebAdminClub\ClubController@index')->name('adminClub.club.index');
    });
    
    Route::prefix('sarpras')->group( function() {
        Route::get('/', 'WebAdminClub\SarprasController@index')->name('adminClub.sarpras.index');
        Route::get('/form', 'WebAdminClub\SarprasController@create')->name('adminClub.sarpras.create');
        Route::get('/form/{id}', 'WebAdminClub\SarprasController@edit')->name('adminClub.sarpras.edit');
        Route::post('/', 'WebAdminClub\SarprasController@store')->name('adminClub.sarpras.store');
        Route::get('/delete/{id}', 'WebAdminClub\SarprasController@destroy')->name('adminClub.sarpras.delete');
        Route::put('/{id}', 'WebAdminClub\SarprasController@update')->name('adminClub.sarpras.put');
        Route::get('/export', 'WebAdminClub\SarprasController@export')->name('adminClub.sarpras.export');
    });

    Route::prefix('atlet')->group( function() {
        Route::get('/', 'WebAdminClub\AtletController@index')->name('adminClub.atlet.index');
        Route::get('/form', 'WebAdminClub\AtletController@create')->name('adminClub.atlet.create');
        Route::get('/form/{id}', 'WebAdminClub\AtletController@edit')->name('adminClub.atlet.edit');
        Route::post('/', 'WebAdminClub\AtletController@store')->name('adminClub.atlet.store');
        Route::get('/delete/{id}', 'WebAdminClub\AtletController@destroy')->name('adminClub.atlet.delete');
        Route::put('/{id}', 'WebAdminClub\AtletController@update')->name('adminClub.atlet.put');
        Route::get('/export', 'WebAdminClub\AtletController@export')->name('adminClub.atlet.export');
    });
    
    Route::prefix('seleksi')->group( function() {
        Route::get('/', 'WebAdminClub\SeleksiController@index')->name('adminClub.seleksi.index');
        Route::get('/daftar/{status}/{id}', 'WebAdminClub\SeleksiController@daftar')->name('adminClub.seleksi.daftar');
    });

});
